#!/bin/bash
docker-compose --profile dev build
docker-compose --profile dev up -d
echo "Wainting for swagger file"
while ! curl --output /dev/null --silent --head --fail $CF_ROUTE/doc; do
 sleep 1 && echo -n .;
done;
echo "Downloading swagger file"
curl --silent $CF_ROUTE/doc -o swagger.json
echo "generating .ts from swagger"/doc
npx swagger-typescript-api -p ./swagger.json -o ./client-ui -n index.ts -t ./serverless/swagger/templates/default --union-enums <<-EOF
y
EOF