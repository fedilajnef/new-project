import { ref } from "vue";
import { useAuthStore } from "@/store/useAuth";
const { currentUser } = useAuthStore();

export default function useDocMenuConfig() {
  const DocMenuConfig = ref([
    {
      pages: [
        {
          heading: "Home",
          route: "/",
          svgIcon: "svg/icons/art002.svg",
          fontIcon: "bi-app-indicator",
        },
      ],
    },
    {
      heading: "appconfigs",
      route: `/${currentUser?.roles[0]}/appconfigs`,
      pages: [
        {
          heading: "All appconfigs",
          route: `/${currentUser?.roles[0]}/appconfigs`,
          svgIcon: "/svg/icons/abs015.svg",
          fontIcon: "bi-calendar3-event",
        },
        {
          heading: "Create appconfig",
          route: `/${currentUser?.roles[0]}/appconfigs/create`,
          svgIcon: "/svg/icons/lay009.svg",
          fontIcon: "bi-calendar3-event",
        },
        {
          heading: "Import data",
          route: `/${currentUser?.roles[0]}/appconfigs/import`,
          svgIcon: "/svg/files/upload.svg",
          fontIcon: "bi-calendar3-event",
        },
      ],
    },
    {
      heading: "users",
      route: `/${currentUser?.roles[0]}/users`,
      pages: [
        {
          heading: "All users",
          route: `/${currentUser?.roles[0]}/users`,
          svgIcon: "/svg/icons/abs015.svg",
          fontIcon: "bi-calendar3-event",
        },
        {
          heading: "Create user",
          route: `/${currentUser?.roles[0]}/users/create`,
          svgIcon: "/svg/icons/lay009.svg",
          fontIcon: "bi-calendar3-event",
        },
        {
          heading: "Import data",
          route: `/${currentUser?.roles[0]}/users/import`,
          svgIcon: "/svg/files/upload.svg",
          fontIcon: "bi-calendar3-event",
        },
      ],
    },
    {
      heading: "condidates",
      route: `/${currentUser?.roles[0]}/condidates`,
      pages: [
        {
          heading: "All condidates",
          route: `/${currentUser?.roles[0]}/condidates`,
          svgIcon: "/svg/icons/abs015.svg",
          fontIcon: "bi-calendar3-event",
        },
        {
          heading: "Create condidate",
          route: `/${currentUser?.roles[0]}/condidates/create`,
          svgIcon: "/svg/icons/lay009.svg",
          fontIcon: "bi-calendar3-event",
        },
        {
          heading: "Import data",
          route: `/${currentUser?.roles[0]}/condidates/import`,
          svgIcon: "/svg/files/upload.svg",
          fontIcon: "bi-calendar3-event",
        },
      ],
    },
    {
      heading: "condidateinjoboffers",
      route: `/${currentUser?.roles[0]}/condidateinjoboffers`,
      pages: [
        {
          heading: "All condidateinjoboffers",
          route: `/${currentUser?.roles[0]}/condidateinjoboffers`,
          svgIcon: "/svg/icons/abs015.svg",
          fontIcon: "bi-calendar3-event",
        },
        {
          heading: "Create condidateinjoboffer",
          route: `/${currentUser?.roles[0]}/condidateinjoboffers/create`,
          svgIcon: "/svg/icons/lay009.svg",
          fontIcon: "bi-calendar3-event",
        },
        {
          heading: "Import data",
          route: `/${currentUser?.roles[0]}/condidateinjoboffers/import`,
          svgIcon: "/svg/files/upload.svg",
          fontIcon: "bi-calendar3-event",
        },
      ],
    },
    {
      heading: "joboffers",
      route: `/${currentUser?.roles[0]}/joboffers`,
      pages: [
        {
          heading: "All joboffers",
          route: `/${currentUser?.roles[0]}/joboffers`,
          svgIcon: "/svg/icons/abs015.svg",
          fontIcon: "bi-calendar3-event",
        },
        {
          heading: "Create joboffer",
          route: `/${currentUser?.roles[0]}/joboffers/create`,
          svgIcon: "/svg/icons/lay009.svg",
          fontIcon: "bi-calendar3-event",
        },
        {
          heading: "Import data",
          route: `/${currentUser?.roles[0]}/joboffers/import`,
          svgIcon: "/svg/files/upload.svg",
          fontIcon: "bi-calendar3-event",
        },
      ],
    },

    {
      sectionTitle: "authentication",
      svgIcon: "svg/icons//teh004.svg",
      fontIcon: "bi-sticky",
      sub: [
        {
          sectionTitle: "basicFlow",
          sub: [
            {
              heading: "signIn",
              route: "/auth/sign-in",
            },
            {
              heading: "signUp",
              route: "/auth/sign-up",
            },
            {
              heading: "passwordReset",
              route: "/auth/password-reset",
            },
            {
              heading: "emailResetPassword",
              route: "/auth/email-reset-password",
            },
            {
              heading: "msgResetPassword",
              route: "/auth/msg-reset-password",
            },
          ],
        },
        {
          heading: "error404",
          route: "/404",
        },
      ],
    },
    {
      pages: [],
    },
  ]);
  return DocMenuConfig;
}
