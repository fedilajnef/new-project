import { Components } from "@tekab-dev-team/storybook-devfactory";
import router from "@/router/index";
import service from '@/service';


export class AuthService {
  async getCurrent() {
    try {
      let currentSession: any = sessionStorage.getItem("session")
      let access_token: any = localStorage.getItem('access_token')
      console.log(JSON.parse(currentSession), "currentSession")
      const data = await service.getUserByToken.getUserByTokenCreate({ token: access_token })
      console.log(data, "data")
      let isLoggedIn: boolean = currentSession ? true : false;
      console.log("isLoggedIn service", isLoggedIn, JSON.parse(currentSession));
      if (JSON.parse(currentSession) == null) {
        currentSession = null;
        isLoggedIn = false;
        return { session: null, isLoggedIn: false };
      } else {
        currentSession = JSON.parse(currentSession);
        isLoggedIn = true;
        await service.setBaseApiParams({
          headers: {
            Authorization: "Bearer " + access_token,
          },
        });
      }
      return { session: currentSession, isLoggedIn: isLoggedIn };
    } catch (error: any) {
      console.error(error.message)
    }
  }

  async signInWithEmail(email: string, password: string, rememberMe: boolean) {
    try {
      const credentials = { email: email, password: password };
      const result = await service.login.loginCreate(credentials);
      localStorage.setItem("rememberMe", rememberMe.toString());
      localStorage.setItem('access_token', result.data.tokens.access.token)
      sessionStorage.setItem('session', JSON.stringify(result.data))
      const token = localStorage.getItem("access_token");

      if (result.data.user) {
        router.push({ name: "home" });
      } else if (result.status) {
        Components.ElMessage.error(result.status);
      }
      return result;
    } catch (error: any) {
      console.error(error.message)
      Components.ElMessage.error(error.message);
    }
  }

  async signUp(email: string, password: string, firstName: string, lastName: string, role: string, rememberMe: boolean) {
    try {

      let result = await service.signUp.signUpCreate({ username: email, password: password, firstName: firstName, lastName, role: role, rememberMe: rememberMe });
      console.log(result.data)

      if (result.data.accessToken) {
        try {
          const result = await this.signInWithEmail(email, password, rememberMe);
          return { data: result, error: null };
        } catch (error) {
          Components.ElMessage.error(error);
          return { data: null, error };
        }
      } else {
        console.log(
          "code error: " + result.data.status + ", msg: " + result.data.statusText
        );
        Components.ElMessage.error(result.data.statusText);
        return { data: null, error: result.data.statusText };
      }

    } catch (error: any) {
      console.error(error.message)
      Components.ElMessage.error(error.message);
    }
  }

  async resetByEmail(email: string) {
    try {
      let result = await service.resetPasswordWithEmail.resetPasswordWithEmailCreate({ username: email });
      console.log(result?.data);
      if (result?.data?.status !== 200) {
        Components.ElMessage.error(result?.data?.status);
      } else {
        router.push({ name: "msg-reset-password" });
      }
    } catch (error: any) {
      console.log("error", error);
      Components.ElMessage.error(error.message);
    }
  }

  async reset(password: string, token: string) {
    try {
      const result = await service.resetPassword.resetPasswordCreate({ newPassword: password, token: token });

      if (result) {
        router.push({ name: "sign-in" });
      }
    } catch (error: any) {
      console.log("error", error);
      Components.ElMessage.error(error.message);
    }
  }
  tryParseJson = (jsonString: any) => {
    try {
      let parsed = JSON.parse(jsonString);
      return parsed;
    } catch (error) {
      return undefined;
    }
  };
  getParameterByName(name: string, url?: string) {
    try {
      if (typeof window === "undefined") return "";

      if (!url) url = window?.location?.href || "";
      // eslint-disable-next-line no-useless-escape
      name = name.replace(/[\[\]]/g, "\\$&");
      const regex = new RegExp("[?&#]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
      if (!results) return null;
      if (!results[2]) return "";
      return decodeURIComponent(results[2].replace(/\+/g, " "));
    } catch (error: any) {
      console.log("error", error);
      Components.ElMessage.error(error.message);
    }
  }
  getAccessToken() {
    try {
      if (typeof window === "undefined") return "";
      const tokenData = window?.localStorage["access_token"];
      if (!tokenData) {
        // try to get from url fragment
        const access_token = this.getParameterByName("access_token");
        if (access_token) return access_token;
        else return undefined;
      }
      const tokenObj = this.tryParseJson(tokenData);
      if (tokenObj === false) {
        return "";
      }
      return tokenObj.currentSession.access_token;
      // ignore if server-side
    } catch (error: any) {
      console.log("error", error);
      Components.ElMessage.error(error.message);
    }
  }
}

export const authService = new AuthService();
