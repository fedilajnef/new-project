export const user_router = {
  path: "/user",
  name: "user",
  meta: {
    requiresAuth: true,
  },
  component: () =>
    import(
      /* webpackChunkName: "layout" */ "@/components/layouts/mainLayout/userLayout.vue"
    ),
  children: [
    {
      path: "appconfigs",
      meta: {
        requiresAuth: true,
      },
      component: () =>
        import(
          /* webpackChunkName: "layout" */ "@/components/layouts/mainLayout/Layout.vue"
        ),
      children: [
        {
          path: "",
          name: "user-list-appconfig",
          component: () =>
            import(
              /* webpackChunkName: "list-appConfig" */ "@/views/main/appConfig/AppConfigList.vue"
            ),
        },
        {
          path: "create",
          name: "user-create-appconfig",
          component: () =>
            import(
              /* webpackChunkName: "create-appConfig" */ "@/views/main/appConfig/AppConfigCreate.vue"
            ),
        },
        {
          path: "edit/:id",
          name: "user-edit-appconfig",
          component: () =>
            import(
              /* webpackChunkName: "edit-appConfig" */ "@/views/main/appConfig/AppConfigEdit.vue"
            ),
        },
        {
          path: ":id",
          name: "user-detail-appconfig",
          component: () =>
            import(
              /* webpackChunkName: "detail-appConfig" */ "@/views/main/appConfig/AppConfigDetail.vue"
            ),
        },
        {
          path: "import",
          name: "user-import-data-appconfig",
          component: () =>
            import(
              /* webpackChunkName: "import-data-appConfig" */ "@/views/main/appConfig/AppConfigImportData.vue"
            ),
        },
      ],
    },
    {
      path: "users",
      meta: {
        requiresAuth: true,
      },
      component: () =>
        import(
          /* webpackChunkName: "layout" */ "@/components/layouts/mainLayout/Layout.vue"
        ),
      children: [
        {
          path: "",
          name: "user-list-user",
          component: () =>
            import(
              /* webpackChunkName: "list-user" */ "@/views/main/user/UserList.vue"
            ),
        },
        {
          path: "create",
          name: "user-create-user",
          component: () =>
            import(
              /* webpackChunkName: "create-user" */ "@/views/main/user/UserCreate.vue"
            ),
        },
        {
          path: "edit/:id",
          name: "user-edit-user",
          component: () =>
            import(
              /* webpackChunkName: "edit-user" */ "@/views/main/user/UserEdit.vue"
            ),
        },
        {
          path: ":id",
          name: "user-detail-user",
          component: () =>
            import(
              /* webpackChunkName: "detail-user" */ "@/views/main/user/UserDetail.vue"
            ),
        },
        {
          path: "import",
          name: "user-import-data-user",
          component: () =>
            import(
              /* webpackChunkName: "import-data-user" */ "@/views/main/user/UserImportData.vue"
            ),
        },
      ],
    },
    {
      path: "condidates",
      meta: {
        requiresAuth: true,
      },
      component: () =>
        import(
          /* webpackChunkName: "layout" */ "@/components/layouts/mainLayout/Layout.vue"
        ),
      children: [
        {
          path: "",
          name: "user-list-condidate",
          component: () =>
            import(
              /* webpackChunkName: "list-condidate" */ "@/views/main/condidate/CondidateList.vue"
            ),
        },
        {
          path: "create",
          name: "user-create-condidate",
          component: () =>
            import(
              /* webpackChunkName: "create-condidate" */ "@/views/main/condidate/CondidateCreate.vue"
            ),
        },
        {
          path: "edit/:id",
          name: "user-edit-condidate",
          component: () =>
            import(
              /* webpackChunkName: "edit-condidate" */ "@/views/main/condidate/CondidateEdit.vue"
            ),
        },
        {
          path: ":id",
          name: "user-detail-condidate",
          component: () =>
            import(
              /* webpackChunkName: "detail-condidate" */ "@/views/main/condidate/CondidateDetail.vue"
            ),
        },
        {
          path: "import",
          name: "user-import-data-condidate",
          component: () =>
            import(
              /* webpackChunkName: "import-data-condidate" */ "@/views/main/condidate/CondidateImportData.vue"
            ),
        },
      ],
    },
    {
      path: "condidateinjoboffers",
      meta: {
        requiresAuth: true,
      },
      component: () =>
        import(
          /* webpackChunkName: "layout" */ "@/components/layouts/mainLayout/Layout.vue"
        ),
      children: [
        {
          path: "",
          name: "user-list-condidateinjoboffer",
          component: () =>
            import(
              /* webpackChunkName: "list-condidateInJoboffer" */ "@/views/main/condidateInJoboffer/CondidateInJobofferList.vue"
            ),
        },
        {
          path: "create",
          name: "user-create-condidateinjoboffer",
          component: () =>
            import(
              /* webpackChunkName: "create-condidateInJoboffer" */ "@/views/main/condidateInJoboffer/CondidateInJobofferCreate.vue"
            ),
        },
        {
          path: "edit/:id",
          name: "user-edit-condidateinjoboffer",
          component: () =>
            import(
              /* webpackChunkName: "edit-condidateInJoboffer" */ "@/views/main/condidateInJoboffer/CondidateInJobofferEdit.vue"
            ),
        },
        {
          path: ":id",
          name: "user-detail-condidateinjoboffer",
          component: () =>
            import(
              /* webpackChunkName: "detail-condidateInJoboffer" */ "@/views/main/condidateInJoboffer/CondidateInJobofferDetail.vue"
            ),
        },
        {
          path: "import",
          name: "user-import-data-condidateinjoboffer",
          component: () =>
            import(
              /* webpackChunkName: "import-data-condidateInJoboffer" */ "@/views/main/condidateInJoboffer/CondidateInJobofferImportData.vue"
            ),
        },
      ],
    },
    {
      path: "joboffers",
      meta: {
        requiresAuth: true,
      },
      component: () =>
        import(
          /* webpackChunkName: "layout" */ "@/components/layouts/mainLayout/Layout.vue"
        ),
      children: [
        {
          path: "",
          name: "user-list-joboffer",
          component: () =>
            import(
              /* webpackChunkName: "list-joboffer" */ "@/views/main/joboffer/JobofferList.vue"
            ),
        },
        {
          path: "create",
          name: "user-create-joboffer",
          component: () =>
            import(
              /* webpackChunkName: "create-joboffer" */ "@/views/main/joboffer/JobofferCreate.vue"
            ),
        },
        {
          path: "edit/:id",
          name: "user-edit-joboffer",
          component: () =>
            import(
              /* webpackChunkName: "edit-joboffer" */ "@/views/main/joboffer/JobofferEdit.vue"
            ),
        },
        {
          path: ":id",
          name: "user-detail-joboffer",
          component: () =>
            import(
              /* webpackChunkName: "detail-joboffer" */ "@/views/main/joboffer/JobofferDetail.vue"
            ),
        },
        {
          path: "import",
          name: "user-import-data-joboffer",
          component: () =>
            import(
              /* webpackChunkName: "import-data-joboffer" */ "@/views/main/joboffer/JobofferImportData.vue"
            ),
        },
      ],
    },
  ],
};
