import {
  JobofferUpdateInput,
  JobofferCreateInput,
  Joboffer,
} from "./../../index";
import { defineStore } from "pinia";
import service from "@/service";
import { storeToRefs } from "pinia";
import { useBodyStore } from "@/store/useBodyModule";
import _ from "lodash";
import axios from "axios";

let accessToken = localStorage.getItem("access_token");

interface IPagination {
  take?: number;
  skip?: number;
}
const { isLoading } = storeToRefs(useBodyStore());
const initialState: Joboffer | JobofferCreateInput | JobofferUpdateInput = {
  image: "",
  dateFin: "2024-03-07T09:41:08.662Z",
  condidateInJoboffers: { id: "" },
};
export const useJobofferStore = defineStore("joboffer-store", {
  state: () => {
    return {
      jobofferList: [] as Array<Joboffer>,
      error: null as Object | any,
      isLoading: useBodyStore().isLoading,
      joboffer: _.cloneDeep(initialState) as
        | Joboffer
        | JobofferCreateInput
        | JobofferUpdateInput,
      jobofferExcelFile: "" as string,
      jobofferPagination: {
        skip: 0,
        take: Number(localStorage.getItem("take")) || 5,
        total: 0,
      },
    };
  },

  getters: {},

  actions: {
    async fetchJoboffers(payload?: IPagination) {
      try {
        const { data } = await service.joboffer.jobofferList({
          skip: payload?.skip || 0,

          take: payload?.take || data.totalCount,
        });
        this.jobofferList = data.paginatedResult;

        this.jobofferList.forEach((element) => {
          for (const [key, value] of Object.entries(element)) {
            if (typeof value == "object" && value) {
              element[key] = Object.values(value);
            }
          }
        });
        this.jobofferPagination = {
          total: data.totalCount,
          skip: payload?.skip ?? 0,
          take: payload?.take ?? data.totalCount,
        };
        localStorage.setItem(
          "take",
          payload?.take?.toString() ?? data.totalCount.toString()
        );
        this.error = null;
      } catch (err: any) {
        this.jobofferList = [];
        console.error("Error loading  ITEMS", err.message);
        this.error = err.error;
      } finally {
      }
    },
    async fetchDataExcelJoboffers() {
      try {
        const { data } = await service.joboffer.fileExcelList();
        this.jobofferExcelFile = data.file;
        this.error = null;
      } catch (err: any) {
        console.error("Error loading  ITEMS", err.message);
        this.error = err.error;
      } finally {
        this.isLoading = false;
      }
    },
    async softDeleteJoboffer(payload: string) {
      this.isLoading = true;
      try {
        const { data } = await service.joboffer.softDeletePartialUpdate(
          payload,
          {
            deletedAt: new Date().toISOString(),
          }
        );
        this.error = null;
        this.fetchJoboffers({
          take: this.jobofferPagination.take,
          skip: this.jobofferPagination.skip,
        });
      } catch (err: any) {
        console.error("Error loading  ITEMS", err.message);
        this.error = err.error;
        this.isLoading = false;
      } finally {
        this.isLoading = false;
      }
    },
    async deleteJoboffer(payload: string) {
      this.isLoading = true;
      try {
        const { data } = await service.joboffer.jobofferDelete(payload);
        this.jobofferList = this.jobofferList.filter(
          (joboffer) => joboffer.id !== data.id
        );
        this.jobofferPagination.total--;
        this.isLoading = false;
        this.error = null;
      } catch (err: any) {
        console.error("Error loading  ITEMS", err);
        this.error = err.error;
        this.isLoading = false;
      } finally {
        this.isLoading = false;
      }
    },
    async editJoboffer(payload: { id: string; data?: JobofferUpdateInput }) {
      this.isLoading = true;
      try {
        const editJobofferData: JobofferUpdateInput = payload.data ??
          this.joboffer ?? [this.joboffer.roles];

        const { data } = await service.joboffer.jobofferPartialUpdate(
          payload.id,
          editJobofferData
        );
        this.jobofferList = this.jobofferList.map((item) =>
          item.id === payload.id ? { ...item, ...data } : item
        );
        this.error = null;
      } catch (err: any) {
        console.error("Error Update  ITEMS", err.message);
        this.error = err.error;
      } finally {
        this.isLoading = false;
      }
    } /*
      async editManyJoboffer(payload: { data: JobofferUpdateInput; where: any }) {
        this.isLoading  = true;
        try {
          const { data } = await service.api.jobofferControllerUpdateMany(
            payload.data,
            payload.where
           
          );
          this.jobofferList = this.jobofferList.map((item) =>
            item.id === payload.id ? { ...item, ...payload.data } : item
          );
          this.error = null;
        } catch (err:any) {
          console.error("Error Update  ITEMS", err.message);
          this.error = err.error;
        } finally {
          this.isLoading = false;
        }
      },*/,

    async getJobofferById(payload: string) {
      this.isLoading = true;
      try {
        const { data } = await service.joboffer.jobofferDetail(payload);
        this.joboffer = {
          ...data,
        };
        this.error = null;
      } catch (err: any) {
        this.resetJoboffer();
        console.error("Error Update  ITEMS", err.message);
        this.error = err.error;
      } finally {
        this.isLoading = false;
      }
    },
    async createJoboffer(payload?: { data: JobofferCreateInput }) {
      this.isLoading = true;
      try {
        const createJobofferData: JobofferCreateInput =
          payload?.data ?? (this.joboffer as JobofferCreateInput);

        const { data } = await service.joboffer.jobofferCreate(
          createJobofferData
        );
        this.jobofferList = [...this.jobofferList, data];
        this.error = null;
      } catch (err: any) {
        this.error = err.error;
      } finally {
        this.isLoading = false;
      }
    },
    async createManyJoboffer(payload: any) {
      this.isLoading = true;
      try {
        const { data } = await service.joboffer.jobofferCreate(payload);
        this.error = null;
      } catch (err: any) {
        this.error = err.error;
      } finally {
        this.isLoading = false;
      }
    },

    resetJoboffer() {
      this.$reset();
    },
    disposeJoboffer() {
      this.$dispose();
    },
  },
});
