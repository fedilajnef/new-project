import { defineStore } from "pinia";
import { authService } from "@/core/services/AuthService";
import service from "@/service";
import router from "@/router/index";
export const useAuthStore = defineStore("authStore", {
  state: () => {
    return {
      currentUser: null as any,
      accessToken: "" as string | undefined,
      refreshToken: "" as string | undefined,
      isLoggedIn: null as null | boolean,
    };
  },
  actions: {
    async login(email: string, password: string, rememberMe: boolean) {
      try {
        const { data } = await authService.signInWithEmail(email, password, rememberMe);
        this.isLoggedIn = true;
        this.currentUser = data?.user;
        this.accessToken = data?.tokens.access.token;
        this.refreshToken = data?.tokens.refresh.token!;
        service.abortRequest({
          headers: {
            Authorization: "Bearer " + this.accessToken,
          },
        });
        await this.getCurrent();
        router.push({ name: "home" });
      } catch (error: any) {
        console.log(error?.message, "error login");
        throw new error("error login")
      }
    },

    async signUp(email: string, password: string, firstName: string, lastName: string, role: string, rememberMe: boolean) {
      try {
        const result = await authService.signUp(email, password, firstName, lastName, role, rememberMe);
        if (result.data) {
          this.isLoggedIn = true;
          this.currentUser = {
            ...result?.data, user_metadata: {
              username: email,
              firstName: firstName,
              lastName: lastName
            },
            roles: [role]
          };

          this.accessToken = result?.data?.accessToken;
          await this.getCurrent();
        }
      } catch (error) {
        console.log(error, "error signup");
      }
    },
    async resetByEmail(email: string) {
      await authService.resetByEmail(email);
    },
    async reset(password: string, token: string) {
      await authService.reset(password, token);
    },
    async logout() {
      localStorage.removeItem('access_token');
      sessionStorage.removeItem('session');
      sessionStorage.removeItem('rememberMe');
      this.isLoggedIn = false;
      window.location.reload();
    },
    async getCurrent() {
      const result = await authService.getCurrent();
      this.isLoggedIn = result.isLoggedIn;
      if (result.session) {
        this.currentUser = result.session.user;
        this.accessToken = result.session.access_token;
        this.refreshToken = result.session.refresh_token!;
      } else {
        this.currentUser = null;
        this.accessToken = "";
        this.refreshToken = "";
      }
      console.log("isLoggedIn ", this.isLoggedIn);
      return result;
    },

    // async changeEmail(payload: { email: string, password: string, userId: string }) {
    //   try {
    //     await service.api.authControllerChangeEmail({ email: payload.email, password: payload.password, userId: payload.userId })
    //     const newSession = await supabase.auth.api.refreshAccessToken(this.refreshToken as string)
    //     if (newSession.data) {
    //       localStorage.setItem("supabase.auth.token", JSON.stringify({ "currentSession": newSession.data, "expiresAt": newSession.data.expires_at }));
    //       this.accessToken = newSession.data.access_token
    //       this.refreshToken = newSession.data.refresh_token
    //       this.currentUser = newSession.data.user
    //       service.setBaseApiParams({
    //         headers: {
    //           Authorization: "Bearer " + this.accessToken,
    //         },
    //       });
    //       return true
    //     } else {
    //       // any problem with getting newSession data will logout and ask to sign in again
    //       this.logout();
    //       return true
    //     }

    //   } catch (error: any) {
    //     console.log(error?.message, "error login");
    //     return false;
    //   }
    // },

    async signInWithLinkedin() {
      try {
        const { data } = await service.auth.linkedinList();
        return data;
      } catch (error: any) {
        console.error(error.message)
      }
    },
  },
});
