import {
  CondidateInJobofferUpdateInput,
  CondidateInJobofferCreateInput,
  CondidateInJoboffer,
} from "./../../index";
import { defineStore } from "pinia";
import service from "@/service";
import { storeToRefs } from "pinia";
import { useBodyStore } from "@/store/useBodyModule";
import _ from "lodash";
import axios from "axios";

let accessToken = localStorage.getItem("access_token");

interface IPagination {
  take?: number;
  skip?: number;
}
const { isLoading } = storeToRefs(useBodyStore());
const initialState:
  | CondidateInJoboffer
  | CondidateInJobofferCreateInput
  | CondidateInJobofferUpdateInput = { jobIdId: "", condidateIdId: "" };
export const useCondidateInJobofferStore = defineStore(
  "condidateinjoboffer-store",
  {
    state: () => {
      return {
        condidateinjobofferList: [] as Array<CondidateInJoboffer>,
        error: null as Object | any,
        isLoading: useBodyStore().isLoading,
        condidateinjoboffer: _.cloneDeep(initialState) as
          | CondidateInJoboffer
          | CondidateInJobofferCreateInput
          | CondidateInJobofferUpdateInput,
        condidateinjobofferExcelFile: "" as string,
        condidateinjobofferPagination: {
          skip: 0,
          take: Number(localStorage.getItem("take")) || 5,
          total: 0,
        },
      };
    },

    getters: {},

    actions: {
      async fetchCondidateInJoboffers(payload?: IPagination) {
        try {
          const {
            data,
          } = await service.condidateinjoboffer.condidateinjobofferList({
            skip: payload?.skip || 0,

            take: payload?.take || data.totalCount,
          });
          this.condidateinjobofferList = data.paginatedResult;

          this.condidateinjobofferList.forEach((element) => {
            for (const [key, value] of Object.entries(element)) {
              if (typeof value == "object" && value) {
                element[key] = Object.values(value);
              }
            }
          });
          this.condidateinjobofferPagination = {
            total: data.totalCount,
            skip: payload?.skip ?? 0,
            take: payload?.take ?? data.totalCount,
          };
          localStorage.setItem(
            "take",
            payload?.take?.toString() ?? data.totalCount.toString()
          );
          this.error = null;
        } catch (err: any) {
          this.condidateinjobofferList = [];
          console.error("Error loading  ITEMS", err.message);
          this.error = err.error;
        } finally {
        }
      },
      async fetchDataExcelCondidateInJoboffers() {
        try {
          const { data } = await service.condidateinjoboffer.fileExcelList();
          this.condidateinjobofferExcelFile = data.file;
          this.error = null;
        } catch (err: any) {
          console.error("Error loading  ITEMS", err.message);
          this.error = err.error;
        } finally {
          this.isLoading = false;
        }
      },
      async softDeleteCondidateInJoboffer(payload: string) {
        this.isLoading = true;
        try {
          const {
            data,
          } = await service.condidateinjoboffer.softDeletePartialUpdate(
            payload,
            {
              deletedAt: new Date().toISOString(),
            }
          );
          this.error = null;
          this.fetchCondidateInJoboffers({
            take: this.condidateinjobofferPagination.take,
            skip: this.condidateinjobofferPagination.skip,
          });
        } catch (err: any) {
          console.error("Error loading  ITEMS", err.message);
          this.error = err.error;
          this.isLoading = false;
        } finally {
          this.isLoading = false;
        }
      },
      async deleteCondidateInJoboffer(payload: string) {
        this.isLoading = true;
        try {
          const {
            data,
          } = await service.condidateinjoboffer.condidateinjobofferDelete(
            payload
          );
          this.condidateinjobofferList = this.condidateinjobofferList.filter(
            (condidateinjoboffer) => condidateinjoboffer.id !== data.id
          );
          this.condidateinjobofferPagination.total--;
          this.isLoading = false;
          this.error = null;
        } catch (err: any) {
          console.error("Error loading  ITEMS", err);
          this.error = err.error;
          this.isLoading = false;
        } finally {
          this.isLoading = false;
        }
      },
      async editCondidateInJoboffer(payload: {
        id: string;
        data?: CondidateInJobofferUpdateInput;
      }) {
        this.isLoading = true;
        try {
          const editCondidateInJobofferData: CondidateInJobofferUpdateInput = payload.data ??
            this.condidateinjoboffer ?? [this.condidateinjoboffer.roles];

          const {
            data,
          } = await service.condidateinjoboffer.condidateinjobofferPartialUpdate(
            payload.id,
            editCondidateInJobofferData
          );
          this.condidateinjobofferList = this.condidateinjobofferList.map(
            (item) => (item.id === payload.id ? { ...item, ...data } : item)
          );
          this.error = null;
        } catch (err: any) {
          console.error("Error Update  ITEMS", err.message);
          this.error = err.error;
        } finally {
          this.isLoading = false;
        }
      } /*
      async editManyCondidateInJoboffer(payload: { data: CondidateInJobofferUpdateInput; where: any }) {
        this.isLoading  = true;
        try {
          const { data } = await service.api.condidateInJobofferControllerUpdateMany(
            payload.data,
            payload.where
           
          );
          this.condidateinjobofferList = this.condidateinjobofferList.map((item) =>
            item.id === payload.id ? { ...item, ...payload.data } : item
          );
          this.error = null;
        } catch (err:any) {
          console.error("Error Update  ITEMS", err.message);
          this.error = err.error;
        } finally {
          this.isLoading = false;
        }
      },*/,

      async getCondidateInJobofferById(payload: string) {
        this.isLoading = true;
        try {
          const {
            data,
          } = await service.condidateinjoboffer.condidateinjobofferDetail(
            payload
          );
          this.condidateinjoboffer = {
            ...data,
          };
          this.error = null;
        } catch (err: any) {
          this.resetCondidateInJoboffer();
          console.error("Error Update  ITEMS", err.message);
          this.error = err.error;
        } finally {
          this.isLoading = false;
        }
      },
      async createCondidateInJoboffer(payload?: {
        data: CondidateInJobofferCreateInput;
      }) {
        this.isLoading = true;
        try {
          const createCondidateInJobofferData: CondidateInJobofferCreateInput =
            payload?.data ??
            (this.condidateinjoboffer as CondidateInJobofferCreateInput);

          const {
            data,
          } = await service.condidateinjoboffer.condidateinjobofferCreate(
            createCondidateInJobofferData
          );
          this.condidateinjobofferList = [
            ...this.condidateinjobofferList,
            data,
          ];
          this.error = null;
        } catch (err: any) {
          this.error = err.error;
        } finally {
          this.isLoading = false;
        }
      },
      async createManyCondidateInJoboffer(payload: any) {
        this.isLoading = true;
        try {
          const {
            data,
          } = await service.condidateinjoboffer.condidateinjobofferCreate(
            payload
          );
          this.error = null;
        } catch (err: any) {
          this.error = err.error;
        } finally {
          this.isLoading = false;
        }
      },

      resetCondidateInJoboffer() {
        this.$reset();
      },
      disposeCondidateInJoboffer() {
        this.$dispose();
      },
    },
  }
);
