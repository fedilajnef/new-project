import {
  CondidateUpdateInput,
  CondidateCreateInput,
  Condidate,
} from "./../../index";
import { defineStore } from "pinia";
import service from "@/service";
import { storeToRefs } from "pinia";
import { useBodyStore } from "@/store/useBodyModule";
import _ from "lodash";
import axios from "axios";

let accessToken = localStorage.getItem("access_token");

interface IPagination {
  take?: number;
  skip?: number;
}
const { isLoading } = storeToRefs(useBodyStore());
const initialState: Condidate | CondidateCreateInput | CondidateUpdateInput = {
  name: "",
  familyName: "",
  resume: "",
};
export const useCondidateStore = defineStore("condidate-store", {
  state: () => {
    return {
      condidateList: [] as Array<Condidate>,
      error: null as Object | any,
      isLoading: useBodyStore().isLoading,
      condidate: _.cloneDeep(initialState) as
        | Condidate
        | CondidateCreateInput
        | CondidateUpdateInput,
      condidateExcelFile: "" as string,
      condidatePagination: {
        skip: 0,
        take: Number(localStorage.getItem("take")) || 5,
        total: 0,
      },
    };
  },

  getters: {},

  actions: {
    async fetchCondidates(payload?: IPagination) {
      try {
        const { data } = await service.condidate.condidateList({
          skip: payload?.skip || 0,

          take: payload?.take || data.totalCount,
        });
        this.condidateList = data.paginatedResult;

        this.condidateList.forEach((element) => {
          for (const [key, value] of Object.entries(element)) {
            if (typeof value == "object" && value) {
              element[key] = Object.values(value);
            }
          }
        });
        this.condidatePagination = {
          total: data.totalCount,
          skip: payload?.skip ?? 0,
          take: payload?.take ?? data.totalCount,
        };
        localStorage.setItem(
          "take",
          payload?.take?.toString() ?? data.totalCount.toString()
        );
        this.error = null;
      } catch (err: any) {
        this.condidateList = [];
        console.error("Error loading  ITEMS", err.message);
        this.error = err.error;
      } finally {
      }
    },
    async fetchDataExcelCondidates() {
      try {
        const { data } = await service.condidate.fileExcelList();
        this.condidateExcelFile = data.file;
        this.error = null;
      } catch (err: any) {
        console.error("Error loading  ITEMS", err.message);
        this.error = err.error;
      } finally {
        this.isLoading = false;
      }
    },
    async softDeleteCondidate(payload: string) {
      this.isLoading = true;
      try {
        const { data } = await service.condidate.softDeletePartialUpdate(
          payload,
          {
            deletedAt: new Date().toISOString(),
          }
        );
        this.error = null;
        this.fetchCondidates({
          take: this.condidatePagination.take,
          skip: this.condidatePagination.skip,
        });
      } catch (err: any) {
        console.error("Error loading  ITEMS", err.message);
        this.error = err.error;
        this.isLoading = false;
      } finally {
        this.isLoading = false;
      }
    },
    async deleteCondidate(payload: string) {
      this.isLoading = true;
      try {
        const { data } = await service.condidate.condidateDelete(payload);
        this.condidateList = this.condidateList.filter(
          (condidate) => condidate.id !== data.id
        );
        this.condidatePagination.total--;
        this.isLoading = false;
        this.error = null;
      } catch (err: any) {
        console.error("Error loading  ITEMS", err);
        this.error = err.error;
        this.isLoading = false;
      } finally {
        this.isLoading = false;
      }
    },
    async editCondidate(payload: { id: string; data?: CondidateUpdateInput }) {
      this.isLoading = true;
      try {
        const editCondidateData: CondidateUpdateInput = payload.data ??
          this.condidate ?? [this.condidate.roles];

        const { data } = await service.condidate.condidatePartialUpdate(
          payload.id,
          editCondidateData
        );
        this.condidateList = this.condidateList.map((item) =>
          item.id === payload.id ? { ...item, ...data } : item
        );
        this.error = null;
      } catch (err: any) {
        console.error("Error Update  ITEMS", err.message);
        this.error = err.error;
      } finally {
        this.isLoading = false;
      }
    } /*
      async editManyCondidate(payload: { data: CondidateUpdateInput; where: any }) {
        this.isLoading  = true;
        try {
          const { data } = await service.api.condidateControllerUpdateMany(
            payload.data,
            payload.where
           
          );
          this.condidateList = this.condidateList.map((item) =>
            item.id === payload.id ? { ...item, ...payload.data } : item
          );
          this.error = null;
        } catch (err:any) {
          console.error("Error Update  ITEMS", err.message);
          this.error = err.error;
        } finally {
          this.isLoading = false;
        }
      },*/,

    async getCondidateById(payload: string) {
      this.isLoading = true;
      try {
        const { data } = await service.condidate.condidateDetail(payload);
        this.condidate = {
          ...data,
        };
        this.error = null;
      } catch (err: any) {
        this.resetCondidate();
        console.error("Error Update  ITEMS", err.message);
        this.error = err.error;
      } finally {
        this.isLoading = false;
      }
    },
    async createCondidate(payload?: { data: CondidateCreateInput }) {
      this.isLoading = true;
      try {
        const createCondidateData: CondidateCreateInput =
          payload?.data ?? (this.condidate as CondidateCreateInput);

        const { data } = await service.condidate.condidateCreate(
          createCondidateData
        );
        this.condidateList = [...this.condidateList, data];
        this.error = null;
      } catch (err: any) {
        this.error = err.error;
      } finally {
        this.isLoading = false;
      }
    },
    async createManyCondidate(payload: any) {
      this.isLoading = true;
      try {
        const { data } = await service.condidate.condidateCreate(payload);
        this.error = null;
      } catch (err: any) {
        this.error = err.error;
      } finally {
        this.isLoading = false;
      }
    },

    resetCondidate() {
      this.$reset();
    },
    disposeCondidate() {
      this.$dispose();
    },
  },
});
