import { Api } from "../../index";

const handleError = () => {
   localStorage.removeItem('access_token');
   window.location.pathname = "/auth/sign-in"
}
let accessToken = localStorage.getItem("access_token");
// Authorization headers for requests
const authHeaders = {
   Authorization: "Bearer " + accessToken,
};

const service = new Api({
   baseUrl: `${import.meta.env.VITE_API_URL}`,
   handleError: handleError,
   baseApiParams: { headers: authHeaders }
});

export default service;
