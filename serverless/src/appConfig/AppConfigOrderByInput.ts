import { z } from '@hono/zod-openapi'

const sortEnum = z.enum(['asc', 'desc'])

const appConfigOrderByInputSchema = z.object({
    id: sortEnum.optional(), 
    createdAt: sortEnum.optional(), 
    updatedAt: sortEnum.optional(), 
    deletedAt: sortEnum.optional(), 
    value: sortEnum.optional(), 
    key: sortEnum.optional()}).strict();

const AppConfigOrderByInput = appConfigOrderByInputSchema.openapi({
  example: {
    id: 'asc', 
    createdAt: 'asc', 
    updatedAt: 'asc', 
    deletedAt: 'asc', 
    value: 'asc', 
    key: 'asc'  
}
})

export { AppConfigOrderByInput }
