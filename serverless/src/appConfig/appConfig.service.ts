import type { Prisma,AppConfig } from '@prisma/client/edge'
import type { PaginatedInterface } from '../util/PaginatedInterface'
import { prisma } from '../config/database'


export class AppConfigService {

findMany = async(
  args: Prisma.AppConfigFindManyArgs
): Promise<PaginatedInterface<AppConfig>> => {
  try {
    const [data, totalCount] = await Promise.all([
      prisma.appConfig.findMany(args),
      prisma.appConfig.count({ where: { deletedAt: null } })
    ])
    return { paginatedResult: data, totalCount }
  } catch (error: any) {
    throw new Error(`Error updating AppConfig: ${ error.message }`)
  }
}

// find a single AppConfig
findOne = async (
  id: string | undefined
): Promise<AppConfig | null > => {
  try {
    const response = await prisma.appConfig.findUnique({
      where: { id: id }
    })
    return response
  } catch (error: any) {
    throw new Error(`Error updating AppConfig: ${ error.message }`)
  }
}

// update a AppConfig by ID
update = async (
  args: Prisma.AppConfigUpdateArgs
): Promise<AppConfig> => {
  try {
    const updated = await prisma.appConfig.update(args)
    return updated
  } catch (error: any) {
    throw new Error(`Error updating AppConfig: ${ error.message }`)
  }
}

// create a new AppConfig
create = async (
  args: Prisma.AppConfigCreateArgs
): Promise<AppConfig> => {
  try {
    const newRecord = await prisma.appConfig.create(args)
    return newRecord
  } catch (error: any) {
      throw new Error(`Error creating AppConfig: ${ error.message }`)
  }
}

// delete existing AppConfig
delete = async (
  args: Prisma.AppConfigDeleteArgs
): Promise<AppConfig> => {
  try {
    const deleted = await prisma.appConfig.delete(args)
    return deleted
  } catch (error: any) {
    throw new Error(`Error deleting AppConfig: ${ error.message }`)
  }
}

// create many AppConfigs
createMany = async (
  args: Prisma.AppConfigCreateManyArgs
): Promise<Prisma.BatchPayload> => {
  try {
    return await prisma.appConfig.createMany(args)
  }catch(error:any) {
    throw new Error(`Error updating AppConfig: ${ error.message }`)
  }
}

// count AppConfigs
count = async (
  args: Prisma.SelectSubset<null, Prisma.AppConfigFindManyArgs>
): Promise<number> => {
  try {
    return await prisma.appConfig.count(args)
  } catch (error: any){
    throw new Error(`Error updating AppConfig: ${ error.message }`)
  }
}

// update many AppConfigs
updateMany = async (
  args: Prisma.AppConfigUpdateManyArgs
): Promise<Prisma.BatchPayload> => {
  try {
    return await prisma.appConfig.updateMany(args)
  } catch (error: any) {
    throw new Error(`Error updating AppConfig: ${ error.message }`)
  }
}

// soft delete a AppConfig
  softDelete = async (
    args: Prisma.AppConfigUpdateArgs
  ): Promise<any> => {
    try {
      const deleted = await prisma.appConfig.update(args)
      return deleted
    } catch (error: any) {
      throw new Error(`Error soft deleting AppConfig: ${ error.message }`)
    }
  }
}

