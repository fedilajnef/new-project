import { z } from '@hono/zod-openapi'
import { AppConfigOrderByInput } from './AppConfigOrderByInput'
import { AppConfigWhereInput } from './AppConfigWhereInput'
// Export the generated OpenAPI example as appConfig
const appConfigUpdateManyArgsSchema = z.object({
  where: z
  .object({
    where: z.object({
      AND: z.union([AppConfigWhereInput, z.array(AppConfigWhereInput)]).optional(),
      OR: z.array(AppConfigWhereInput).optional(),
      NOT: z.union([AppConfigWhereInput, z.array(AppConfigWhereInput)]).optional()
    }).merge(AppConfigWhereInput)
  })
  .optional(),
})

export type UpdateAppConfig = z.infer<typeof appConfigUpdateManyArgsSchema>
export { appConfigUpdateManyArgsSchema as AppConfigUpdateManyArgs }
