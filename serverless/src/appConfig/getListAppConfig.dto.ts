 import { z } from '@hono/zod-openapi'
import { appConfigDto } from './AppConfig'

const getListAppConfigDtoSchema = z.object({
    paginatedResult: z.array(appConfigDto), // An array of appConfig objects
    totalCount: z.number() // Total count of  appConfig 
}).strict() 

// Generate an OpenAPI example for appConfig input
export const GetListAppConfigDto = getListAppConfigDtoSchema.openapi({ 
  example: {

     paginatedResult: [ 
{
    id: '550e8400-e29b-41d4-a716-446655440000', 
    createdAt: new Date('2023-08-24T10:00:00Z'), 
    updatedAt: new Date('2023-08-24T10:00:00Z'), 
    deletedAt: new Date('2023-08-24T10:00:00Z'), 
    value: 'value', 
    key: 'key' 
}
 ], 
 totalCount: 1
 } 
}) 

