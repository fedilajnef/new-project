import { z } from '@hono/zod-openapi'
import { AppConfigOrderByInput } from './AppConfigOrderByInput'
import { AppConfigWhereInput } from './AppConfigWhereInput'
// Export the generated OpenAPI example as appConfig
const appConfigFindManyArgsSchema = z.object({
  where: z
  .object({
    where: z.object({
      AND: z.union([AppConfigWhereInput, z.array(AppConfigWhereInput)]).optional(),
      OR: z.array(AppConfigWhereInput).optional(),
      NOT: z.union([AppConfigWhereInput, z.array(AppConfigWhereInput)]).optional()
    }).merge(AppConfigWhereInput)
  })
  .optional(),
  orderBy: z.object({ orderBy: AppConfigOrderByInput.optional() }).optional(),
// eslint-disable-next-line @typescript-eslint/no-unsafe-argument
  skip: z.string().optional().refine((value: any) => value === undefined || !isNaN(value), {
    message: 'Invalid value for skip, must be a number or undefined'
  }).openapi(({ example: '0' })), // Use a number example instead of string

  // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
  take: z.string().optional().refine((value: any) => value === undefined || !isNaN(value), {
    message: 'Invalid value for take, must be a number or undefined'
  }).openapi(({ example: '10' }))
})
export { appConfigFindManyArgsSchema as AppConfigFindManyArgs }
