import { z } from '@hono/zod-openapi'

// Define the schema for appConfigUpdateInputSchema input using Zod
export const appConfigUpdateInputSchema = z.object({ 
  id: z.optional(z.string()),
  createdAt: z.optional(z.union([z.date(), z.string()])), 
  updatedAt: z.optional(z.union([z.date(), z.string(), z.null()])), 
  deletedAt: z.optional(z.union([z.date(), z.string(), z.null()])), 
  value: z.optional(z.union([
  z.string(),
 z.null()
])), 
  key: z.optional(z.union([
  z.string(),
 z.null()
]))}).strict() 

// Generate an OpenAPI example for appConfig input
const openApiExample = appConfigUpdateInputSchema.openapi({ 
  example: {
    id: '550e8400-e29b-41d4-a716-446655440000', 
    createdAt: new Date('2023-08-24T10:00:00Z'), 
    updatedAt: new Date('2023-08-24T10:00:00Z'), 
    deletedAt: new Date('2023-08-24T10:00:00Z'), 
    value: 'value', 
    key: 'key'  
}
}).strict() 

// Define a type for appConfig input using the generated example
export type UpdateAppConfig = z.infer <typeof appConfigUpdateInputSchema>

// Export the generated OpenAPI example as appConfig
export { openApiExample as AppConfigUpdateInput } 
