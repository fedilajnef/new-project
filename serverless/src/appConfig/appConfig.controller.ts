import * as XLSX from 'xlsx'
import { auth } from '../middlewares/auth'
import { checkPermission } from '../util/checkPermission'
import { processArgs } from '../util/ProcessArgs'
import { flattenObject } from '../util/flattenObject'
import { createRoute } from '@hono/zod-openapi'
import type { Prisma } from '@prisma/client/edge'
import type { AppConfigService } from './appConfig.service'
import { AppConfigCreateInput } from './AppConfigCreateInput'
import { AppConfigFindManyArgs } from './AppConfigFindManyArgs'
import { AppConfigWhereUniqueInput } from './AppConfigWhereUniqueInput'
import { AppConfigUpdateInput } from './AppConfigUpdateInput'
import { GetListAppConfigDto } from './getListAppConfig.dto'
import {appConfigDto} from './AppConfig'
import { errorHandler } from '../middlewares/errorHandlingMiddleware'

export class AppConfigControllerBase {
  // eslint-disable-next-line @typescript-eslint/space-before-function-paren
  constructor (
    private readonly appconfigService: AppConfigService
  ) { }
  create: any = async (
    c: any
  ) => {
    try {
      await auth(c)
      const payloadRoles: string[] = c.get('payload').roles
      const permission = checkPermission(payloadRoles, 'create:any')
      const rolesString = payloadRoles.join(', ')
      if (!permission) {
        return c.json(
          {
            code: 403,
            message: `Permission denied. AppConfig creation is not allowed for roles: ${rolesString}.`
          },
          403
        )
      }
      const bodyParse = await c.req.json()
      // If permission check passes, create ENTITY_NAME
      const response = await this.appconfigService.create({ 
        data: {
   value: bodyParse.value,
   key: bodyParse.key}
    })

    return c.json({ response, code: 200 })
  } catch (error: any) {
    return await errorHandler(error,c)
  }
}

routeCreate = createRoute({
  method: 'post',
  path: '/appconfig',
  tags: ['appconfig'],
  request: {
    body: {
      content: {
        'application/json': {
          schema: AppConfigCreateInput
          }
        }
      }
    },
  responses: {
    200: {
      content: {
        'application/json': {
          schema: AppConfigCreateInput
        }
      },
      description: 'Create a new AppConfig'
    }
  }
})

// Find many appconfig entity
findMany: any = async (c: any) => {
  try {
    await auth(c)
    const payloadRoles: string[] = c.get('payload').roles
    const permission = checkPermission(payloadRoles, 'read:any')
    const rolesString = payloadRoles.join(', ')
    if (!permission) {
      return c.json(
        {
          code: 403,
          message: `Permission denied. AppConfig finding is not allowed for roles: ${rolesString}.`
        },
        403
      )
    }

    // Parse and process query parameters
    const queryParse = c.req.query()
    const args = processArgs(queryParse)
    // Fetch ENTITY_NAME with selected fields
    const result = await this.appconfigService.findMany({
      where: args?.where as Prisma.AppConfigWhereInput,

      orderBy: args?.orderBy as Prisma.AppConfigOrderByWithRelationInput,

      skip: args.skip !== undefined ? parseInt(args?.skip) : undefined,

      take: args.take !== undefined ? parseInt(args?.take) : undefined,

      select: {
   id: true,
   value: true,
   key: true}
      })

      // Return paginated result and total count
      return c.json({ paginatedResult: result.paginatedResult, totalCount: result.totalCount, code: 200 })
    } catch (error: any) {
      return await errorHandler(error,c)
  }
}

routeFindMany = createRoute({
  method: 'get',
  path: '/appconfig',
  tags: ['appconfig'],
  request: {
    query: AppConfigFindManyArgs
    },
  responses: {
    200: {
      content: {
        'application/json': {
          schema: GetListAppConfigDto
        }
      },
      description: 'Retrieve a list of AppConfig'
    }
  }
})

// Update appconfig entity
update: any = async (c: any) => {
  try {
    await auth(c)
    const payloadRoles: string[] = c.get('payload').roles
    const permission = checkPermission(payloadRoles, 'update:any')
    const rolesString = payloadRoles.join(', ')
    if (!permission) {
      return c.json(
        {
          code: 403,
          message: `Permission denied. AppConfig updating is not allowed for roles: ${rolesString}.`
        },
        403
      )
    }

    // Parse request parameters and body
    const paramsParse = c.req.param()
    const bodyParse = await c.req.json()
    const params = AppConfigWhereUniqueInput.parse(paramsParse)
    const response = await this.appconfigService.update({
      where: { id: params.id },
      data: {
   value: bodyParse.value,
   key: bodyParse.key}
})
    return c.json(response, 200)
  } catch (error: any) {
     return await errorHandler(error,c)
  }
}

routeUpdate = createRoute({
  method: 'patch',
  path: '/appconfig/{id}',
  tags: ['appconfig'],
  request: {
    params: AppConfigWhereUniqueInput,
    body: {
      content: {
        'application/json': {
          schema: AppConfigUpdateInput
        }
      }
    }
  },
  responses: {
    200: {
      content: {
        'application/json': {
          schema: appConfigDto
        }
      },
      description: 'Update a AppConfig'
    }
  }
})

// Delete appconfig entity
delete: any = async (c: any) => {
  try {
    await auth(c)
    const payloadRoles: string[] = c.get('payload').roles
    const permission = checkPermission(payloadRoles, 'delete:any')
    const rolesString = payloadRoles.join(', ')
    if (!permission) {
      return c.json(
        {
          code: 403,
          message: `Permission denied. AppConfig deleting is not allowed for roles: ${rolesString}.`
         },
         403
      )
    }
    // Parse request parameters
    const paramsParse = c.req.param()
    const params = AppConfigWhereUniqueInput.parse(paramsParse)
    // Delete
    const response = await this.appconfigService.delete({
      where: { id: params.id }
    })

    return c.json(response, 200)
  } catch (error: any) {
    return await errorHandler(error,c)
  }
}

routeDelete = createRoute({
  method: 'delete',
  path: '/appconfig/{id}',
  tags: ['appconfig'],
  request: {
    params: AppConfigWhereUniqueInput
  },
  responses: {
    200: {
      content: {
        'application/json': {
          schema: appConfigDto
        }
      },
      description: 'Delete a AppConfig'
    }
  }
})

// Find appconfig Data For Excel
findDataForExcel: any = async (c: any) => {
  try {
    await auth(c)
    const payloadRoles: string[] = c.get('payload').roles
    const permission = checkPermission(payloadRoles, 'delete:any')
    const rolesString = payloadRoles.join(', ')
    if (!permission) {
      return c.json(
        {
          code: 403,
          message: `Permission denied. AppConfig finding is not allowed for roles: ${rolesString}.`
        },
        403
      )
    }
    // Parse and process query parameters
    const queryParse = c.req.query()
    const args = AppConfigFindManyArgs.strict().parse(processArgs(queryParse))
    // Fetch appconfigdata
    const result = await this.appconfigService.findMany({
      where: args?.where as Prisma.AppConfigWhereInput,

      orderBy: args?.orderBy as Prisma.AppConfigOrderByWithRelationInput,

      skip: args.skip !== undefined ? parseInt(args?.skip) : undefined,

      take: args.take !== undefined ? parseInt(args?.take) : undefined,

      select: { id : true, 
createdAt : true, 
updatedAt : true, 
deletedAt : true, 
value : true, 
key : true}
    })
    // Flatten the result for Excel
    const flatResult = result.paginatedResult.map((obj) => flattenObject(obj))
    // Convert to Excel format
    const excelFile = XLSX.utils.json_to_sheet(flatResult)
    const Workbook = XLSX.utils.book_new()
    await XLSX.utils.book_append_sheet(Workbook, excelFile, 'data')
    // Convert to base64 and return as a file
    const file = await XLSX.write(Workbook, {
      bookType: 'xlsx',
      bookSST: true,
      type: 'base64'
    })
    return c.json({ file }, 200)
  } catch (error: any) {
    return await errorHandler(error,c)
  }
}

routeFindDataExcel = createRoute({
  method: 'get',
  path: '/appconfig/fileExcel',
  tags: ['appconfig'],
  responses: {
    200: {
      content: {
        'application/json': {
          schema: AppConfigFindManyArgs
        }
      },
      description: 'Retrieve AppConfig data in Excel format'
    }
  }
})

// Update appconfig entity
findOne: any = async (c: any) => {
  try {
    await auth(c)
    const payloadRoles: string[] = c.get('payload').roles
    const permission = checkPermission(payloadRoles, 'read:own')
    const rolesString = payloadRoles.join(', ')
    if (!permission) {
      return c.json(
        {
          code: 403,
          message: `Permission denied. AppConfig finding One is not allowed for roles: ${rolesString}.`
        },
        403
      )
    }
    // Parse request parameters and body
    const paramsParse = c.req.param()
    const params = AppConfigWhereUniqueInput.parse(paramsParse)
const AppConfig = await this.appconfigService.findOne(params.id)
    return c.json(AppConfig, 200)
  } catch (error: any) {
    return await errorHandler(error,c)
  }
}

routeFindOne = createRoute({
  method: 'get',
  path: '/appconfig/{id}',
  tags: ['appconfig'],
  request: {
    params: AppConfigWhereUniqueInput
  },
  responses: {
    200: {
      content: {
        'application/json': {
          schema: appConfigDto
        }
      },
      description: 'find one AppConfig'
    }
  }
})
   
// soft deleting appconfig entity
softDelete: any = async (c: any) => {
  try {
      await auth(c)
      const payloadRoles: string[] = c.get('payload').roles
      const permission = checkPermission(payloadRoles, 'update:any')
      const rolesString = payloadRoles.join(', ')
      if (!permission) {
        return c.json(
          {
            code: 403,
            message: `Permission denied. AppConfig  soft deleting  is not allowed for roles: ${rolesString}.`        
          },
          403
        )
      }

      // Parse request parameters and body
      const paramsParse = c.req.param()
      const bodyParse = await c.req.json()
      const params = AppConfigWhereUniqueInput.parse(paramsParse)
      const response = await this.appconfigService.softDelete(
        {
          where: { id: params.id },
          data: { 
            deletedAt: bodyParse.deletedAt
          }
        }
      )
    return c.json(response, 200)
  } catch (error: any) {
    return await errorHandler(error,c)
  }
}

routeSoftDelete = createRoute({
  method: 'patch',
  path: '/appconfig/softDelete/{id}',
  tags: ['appconfig'],
  request: {
    params: AppConfigWhereUniqueInput,
    body: {
      content: {
        'application/json': {
          schema: AppConfigUpdateInput
        }
      }
    }
  },
  responses: {
    200: {
      content: {
        'application/json': {
          schema: appConfigDto
        }
      },
      description: 'soft deleting a AppConfig'
    }
  }
})
}

