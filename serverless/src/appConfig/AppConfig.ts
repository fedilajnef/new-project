import { z } from '@hono/zod-openapi'

// Define the schema for AppConfig input using Zod
export const appConfigSchema = z.object({ 
  id: z.string({
                required_error: 'id is required',
                invalid_type_error: 'id must be a string'
            }),
  createdAt: z.optional(z.union([z.date(), z.string()])), 
  updatedAt: z.optional(z.union([z.date(), z.string(), z.null()])), 
  deletedAt: z.optional(z.union([z.date(), z.string(), z.null()])), 
  value: z.optional(z.union([
  z.string(),
 z.null()
])), 
  key: z.optional(z.union([
  z.string(),
 z.null()
]))}) 

// Generate an OpenAPI example for AppConfig input
const openApiExample = appConfigSchema.openapi({ 
  example: {
    id: '550e8400-e29b-41d4-a716-446655440000', 
    createdAt: new Date('2023-08-24T10:00:00Z'), 
    updatedAt: new Date('2023-08-24T10:00:00Z'), 
    deletedAt: new Date('2023-08-24T10:00:00Z'), 
    value: 'value', 
    key: 'key'  
}
}).strict() 

// Define a type for AppConfig input using the generated example
export type AppConfig = z.infer<typeof openApiExample> 

// Export the generated OpenAPI example as AppConfig
export { openApiExample as appConfigDto }
