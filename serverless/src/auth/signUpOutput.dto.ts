import { z } from '@hono/zod-openapi'

// Define the SignUpOutputDTO schema
export const signUpOutputSchema = z.object({
  id: z.string(),
  firstName: z.string().nullable(),
  lastName: z.string().nullable(),
  username: z.string(),
  roles: z.array(z.string()),
  accessToken: z.string().nullable().optional()
})

// Define an OpenAPI example for SignUpOutputDTO
const signUpOutputExample = signUpOutputSchema.openapi({
  example: {
    id: 'exampleID',
    firstName: 'John',
    lastName: 'Doe',
    username: 'johndoe',
    roles: ['user', 'admin'],
    accessToken: 'exampleAccessToken'
  }
}).strict()

// Export the type and example
export type SignUpOutputDTO = z.infer<typeof signUpOutputSchema>
export { signUpOutputExample as SignUpOutput }