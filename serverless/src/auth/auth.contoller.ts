/* eslint-disable @typescript-eslint/explicit-function-return-type */
import type { StatusCode } from 'hono/utils/http-status'
import type { AuthService } from './auth.service'
import * as authValidation from './auth.validation'
import type { TokenService } from './token.service'
import { createRoute } from '@hono/zod-openapi'
import { SignUpOutput } from './signUpOutput.dto'
import { auth } from '../middlewares/auth'
import { checkPermission } from '../util/checkPermission'
import { UserCreateInput } from '../user/UserCreateInput'
import type { UserService } from '../user/user.service'

// Import necessary DTOs and credentials
import { type SignUpCredentials, SignUpCredentialsInput, EmailResetPasswordCredential, TokenCredentialInput, ResetPasswordCredential } from './Credentials'
import { GetListUserDto } from '../user/getListUser.dto'

export interface Env {
  SITE_URL: string
  BREVO_KEY: string
}

export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly userService: UserService,
    private readonly tokenService: TokenService
  ) { }

  // Define a function for user login
  login = async (c: any) => {
    try {
      // Parse the request body
      const bodyParse = await c.req.json()
      const { email, password } = authValidation.userlogin.parse(bodyParse)

      // Attempt to log in the user
      const user = await this.authService.loginUserWithEmailAndPassword(email, password)

      // Generate authentication tokens
      // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
      const tokens = await this.tokenService.generateAuthTokens(user)

      // Return user and tokens as JSON response
      return c.json({ user, tokens }, 200 as StatusCode)
    } catch (err: any) {
      // Handle errors and return an error response
      return c.json({ code: '400', error: err.message }, 400 as StatusCode)
    }
  }

  // Define a route for user login
  routeLoginUser = createRoute({
    method: 'post',
    path: '/login',
    tags: ['auth'],
    request: {
      body: {
        content: {
          'application/json': {
            schema: authValidation.userlogin
          }
        }
      }
    },
    responses: {
      200: {
        content: {
          'application/json': {
            schema: GetListUserDto
          }
        },
        description: 'Create the user'
      }
    }
  })

  // Define a function for user sign-up
  signUp = async (c: any) => {
    const bodyParse = await c.req.json()
    const body: SignUpCredentials = SignUpCredentialsInput.parse(bodyParse)
    try {
      // Attempt to sign up the user
      const user = await this.authService.signUp(body)

      // Return user as JSON response
      return c.json(user, 200 as StatusCode)
    } catch (error: any) {
      // Handle errors and return an error response
      return c.json({ code: '500', error: error.message }, 500 as StatusCode)
    }
  }

  // Define a route for user sign-up
  routeSignUpUser = createRoute({
    method: 'post',
    path: '/sign_up',
    tags: ['auth'],
    request: {
      body: {
        content: {
          'application/json': {
            schema: SignUpCredentialsInput
          }
        }
      }
    },
    responses: {
      200: {
        content: {
          'application/json': {
            schema: SignUpOutput
          }
        },
        description: 'sign up'
      }
    }
  })

  // Define a function for resetting password with email
  resetPasswordWithEmail = async (c: any) => {
    const bodyParse = await c.req.json()
    const BREVO_KEY = await c.env.BREVO_KEY
    const SITE_URL = await c.env.SITE_URL
    const body = EmailResetPasswordCredential.parse(bodyParse)
    try {
      if (BREVO_KEY !== undefined || BREVO_KEY !== null) {
        await this.authService.resetPasswordWithEmail(SITE_URL, body, BREVO_KEY)
        // Return success response
        return c.json({ message: 'reset password with email with success', status: 200 })
      } else {
        return c.json({ message: 'There is an error the provided API_TOKEN is Undefined', status: 500 })
      }
    } catch (error: any) {
      // Handle errors and return an error response
      return c.json({ code: '500', error: error.message }, 500 as StatusCode)
    }
  }

  // Define a route for resetting password with email
  routeResetPasswordWithEmail = createRoute({
    method: 'post',
    path: '/reset_password_with_email',
    tags: ['auth'],
    request: {
      body: {
        content: {
          'application/json': {
            schema: EmailResetPasswordCredential
          }
        }
      }
    },
    responses: {
      200: {
        description: 'reset password with email with success'
      }
    }
  })

  resetPassword = async (c: any) => {
    const bodyParse = await c.req.json()
    const body = ResetPasswordCredential.parse(bodyParse)

    try {
      await this.authService.resetPassword(body)
      return c.json({ message: 'password reset with success' }, 200 as StatusCode)
    } catch (error: any) {
      return c.json({ code: '500', error: error.message }, 500 as StatusCode)
    }
  }

  routeResetPassword = createRoute({
    method: 'post',
    path: '/reset_password', // This path should be updated to a meaningful one
    tags: ['auth'],
    request: {
      body: {
        content: {
          'application/json': {
            schema: ResetPasswordCredential
          }
        }
      }
    },
    responses: {
      200: {
        description: 'password reset with success'
      }
    }
  })

  // Define a function for getting user by token
  getUserByToken = async (c: any) => {
    const bodyParse = await c.req.json()
    const body = TokenCredentialInput.parse(bodyParse)
    const token = body.token
    try {
      // Attempt to retrieve user by token
      const user = await this.authService.getUserByToken(token)

      // Return user as JSON response
      return c.json(user, 200 as StatusCode)
    } catch (error: any) {
      // Handle errors and return an error response
      return c.json({ code: '500', error: error.message }, 500 as StatusCode)
    }
  }

  // Define a route for getting user by token
  routeGetUserByToken = createRoute({
    method: 'post',
    path: '/get_user_by_token', // This path should be updated to a meaningful one
    tags: ['auth'],
    request: {
      body: {
        content: {
          'application/json': {
            schema: TokenCredentialInput
          }
        }
      }
    },
    responses: {
      200: {
        description: 'Get user by token with success'
      }
    }
  })

  // Define a function for inviting a user
  inviteUser: any = async (c: any) => {
    try {
      // Authenticate the user

      await auth(c)
      const payloadRoles: string[] = c.get('payload').roles
      const permission = checkPermission(payloadRoles, 'create:any')
      const rolesString = payloadRoles.join(', ')

      const SITE_URL = await c.env.SITE_URL
      const BREVO_KEY = await c.env.BREVO_KEY


      if (!permission) {
        return c.json(
          {
            code: 403,
            message: `Permission denied. User creation is not allowed for roles: ${rolesString}.`
          },
          403
        )
      }

      const bodyParse = await c.req.json()
      const body = UserCreateInput.parse(bodyParse)

      // If permission check passes, create the user
      const user = await this.userService.create({ data: body })
      await this.authService.inviteUserByEmail(SITE_URL, user, BREVO_KEY)

      // Return created user as JSON response
      return c.json(user, 200 as StatusCode)
    } catch (err: any) {
      // Handle errors and return an error response
      return c.json({ code: '401', error: err.message }, 401 as StatusCode)
    }
  }

  // Define a route for inviting a user
  routeInviteUser = createRoute({
    method: 'post',
    path: '/invite_user',
    tags: ['auth'],
    request: {
      body: {
        content: {
          'application/json': {
            schema: UserCreateInput
          }
        }
      }
    },
    responses: {
      200: {
        content: {
          'application/json': {
            schema: UserCreateInput
          }
        },
        description: 'Invite and create a new user'
      }
    }
  })

  // Define a function for inviting multiple users
  inviteManyUser: any = async (c: any) => {
    try {
      await auth(c)
      const payloadRoles: string[] = c.get('payload').roles
      const permission = checkPermission(payloadRoles, 'create:any')
      const rolesString = payloadRoles.join(', ')

      if (!permission) {
        return c.json(
          {
            code: 403,
            message: `Permission denied. User creation is not allowed for roles: ${rolesString}.`
          },
          403
        )
      }

      const bodyParse = await c.req.json()
      const SITE_URL = await c.env.SITE_URL
      const BREVO_KEY = await c.env.BREVO_KEY
      const body = UserCreateInput.array().parse(bodyParse)
      const users = []

      for (const element of body) {
        try {
          const result = await this.userService.findOne(element.username)
          if (result !== null || result !== undefined) {
            if (result.isValid === false) {
              const user = await this.userService.update({
                where: { username: element.username },
                data: element
              })
              users.push(user)
            }
          } else {
            const user = await this.userService.create({
              data: element
            })
            users.push(user)

            if (user) await this.authService.inviteUserByEmail(SITE_URL, user, BREVO_KEY)
          }
        } catch (err: any) {
          return c.json(err.message, 500 as StatusCode)
        }
      }

      return c.json(users, 200 as StatusCode)
    } catch (err: any) {
      // Handle errors and return an error response
      return c.json({ code: '401', error: err.message }, 401 as StatusCode)
    }
  }

  // Define a route for inviting multiple users
  routeInviteManyUser = createRoute({
    method: 'post',
    path: '/invite_many_users', // This path should be updated to a meaningful one
    tags: ['auth'],
    request: {
      body: {
        content: {
          'application/json': {
            schema: UserCreateInput.array()
          }
        }
      }
    },
    responses: {
      200: {
        content: {
          'application/json': {
            schema: UserCreateInput.array()
          }
        },
        description: 'Invite and create multiple new users'
      }
    }
  })

}