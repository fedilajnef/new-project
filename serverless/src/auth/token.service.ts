import jwt from '@tsndr/cloudflare-worker-jwt'
import dayjs, { Dayjs } from 'dayjs'
import { Selectable } from 'kysely'
import { Config } from '../config/config'
import { Role } from '../config/roles'
import { TokenType, tokenTypes } from '../config/tokens'

export class TokenService {
  generateToken = async (
    anyId: number,
    type: TokenType,
    roles: Role[],
    expires: Dayjs,
    secret: string,
    isEmailVerified: boolean
  ) => {
    const payload = {
      sub: anyId.toString(),
      exp: expires.unix(),
      iat: dayjs().unix(),
      type,
      roles,
      isEmailVerified
    }
    return jwt.sign(payload, secret)
  }

  generateAuthTokens = async (any: Selectable<any>) => {
    const accessTokenExpires = dayjs().add(15, 'days')
    const accessToken = await this.generateToken(
      any.id,
      tokenTypes.ACCESS,
      any.roles,
      accessTokenExpires,
      'anis',
      any.is_email_verified
    )
    const refreshTokenExpires = dayjs().add(30, 'days')
    const refreshToken = await this.generateToken(
      any.id,
      tokenTypes.REFRESH,
      any.roles,
      refreshTokenExpires,
      'anis',
      any.is_email_verified
    )
    return {
      access: {
        token: accessToken,
        expires: accessTokenExpires.toDate()
      },
      refresh: {
        token: refreshToken,
        expires: refreshTokenExpires.toDate()
      }
    }
  }

  verifyToken = async (token: string, type: TokenType, secret: string) => {
    const isValid = await jwt.verify(token, secret)
    if (!isValid) {
      throw new Error('Token not valid')
    }
    const decoded = jwt.decode(token)
    const payload = decoded.payload
    if (type !== payload.type) {
      throw new Error('Token not valid')
    }
    return payload
  }

  generateVerifyEmailToken = async (
    any: Selectable<any>, jwtConfig: Config['jwt']
  ) => {
    const expires = dayjs().add(jwtConfig.verifyEmailExpirationMinutes, 'minutes')
    const verifyEmailToken = await this.generateToken(
      any.id,
      tokenTypes.VERIFY_EMAIL,
      any.roles,
      expires,
      jwtConfig.secret,
      any.is_email_verified
    )
    return verifyEmailToken
  }

  generateResetPasswordToken = async (
    any: Selectable<any>,
    jwtConfig: Config['jwt']
  ) => {
    const expires = dayjs().add(jwtConfig.resetPasswordExpirationMinutes, 'minutes')
    const resetPasswordToken = await this.generateToken(
      any.id,
      tokenTypes.RESET_PASSWORD,
      any.roles,
      expires,
      jwtConfig.secret,
      any.is_email_verified
    )
    return resetPasswordToken
  }
}