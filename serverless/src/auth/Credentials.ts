import { z } from '@hono/zod-openapi'

// Define the Credentials schema
export const credentialsSchema = z.object({
  username: z.string(),
  password: z.string(),
  rememberMe: z.boolean()
})

// Define an OpenAPI example for Credentials
const credentialsExample = credentialsSchema.openapi({
  example: {
    username: 'exampleUsername',
    password: 'examplePassword',
    rememberMe: true,
  }
}).strict()

// Export the type and example
export type Credentials = z.infer<typeof credentialsSchema>
export { credentialsExample as CredentialsExample }


// Define the SignUpCredentials schema
export const signUpCredentialsSchema = z.object({
  username: z.string(),
  firstName: z.string(),
  lastName: z.string(),
  password: z.string(),
  role: z.string(),
  rememberMe: z.boolean()
})

// Define an OpenAPI example for SignUpCredentials
const signUpCredentialsExample = signUpCredentialsSchema.openapi({
  example: {
    username: 'exampleUsername',
    firstName: 'John',
    lastName: 'Doe',
    password: 'examplePassword',
    role: 'user',
    rememberMe: false
  }
}).strict()

// Export the type and example
export type SignUpCredentials = z.infer<typeof signUpCredentialsSchema>
export { signUpCredentialsExample as SignUpCredentialsInput }


// Define the EmailResetPasswordCredential schema
export const emailResetPasswordCredentialSchema = z.object({
  username: z.string()
})

// Define an OpenAPI example for EmailResetPasswordCredential
const emailResetPasswordCredentialExample = emailResetPasswordCredentialSchema.openapi({
  example: {
    username: 'exampleUsername'
  }
}).strict()

// Export the type and example
export type EmailResetPasswordCredentialDto = z.infer<typeof emailResetPasswordCredentialSchema>
export { emailResetPasswordCredentialExample as EmailResetPasswordCredential }


// Define the ResetPasswordCredential schema
export const resetPasswordCredentialSchema = z.object({
  newPassword: z.string(),
  token: z.string()
});

// Define an OpenAPI example for ResetPasswordCredential
const resetPasswordCredentialExample = resetPasswordCredentialSchema.openapi({
  example: {
    newPassword: 'newPassword123',
    token: 'resetToken123'
  }
}).strict()

// Export the type and example
export type ResetPasswordCredentialType = z.infer<typeof resetPasswordCredentialSchema>
export { resetPasswordCredentialExample as ResetPasswordCredential }


// Define the TokenCredential schema
export const tokenCredentialSchema = z.object({
  token: z.string()
})

// Define an OpenAPI example for TokenCredential
const tokenCredentialExample = tokenCredentialSchema.openapi({
  example: {
    token: 'exampleToken'
  }
}).strict()

// Export the type and example
export type TokenCredential = z.infer<typeof tokenCredentialSchema>
export { tokenCredentialExample as TokenCredentialInput }



// Define the SignUpOutputDTO schema
export const getUserByTokenSchema = z.object({
  id: z.string(),
  firstName: z.string().nullable(),
  lastName: z.string().nullable(),
  username: z.string(),
  roles: z.array(z.string()),
  token: z.string().nullable().optional()
})

// Define an OpenAPI example for SignUpOutputDTO
const getUserByTokenExample = getUserByTokenSchema.openapi({
  example: {
    id: 'exampleID',
    firstName: 'John',
    lastName: 'Doe',
    username: 'johndoe',
    roles: ['user', 'admin'],
    token: 'exampleAccessToken'
  }
}).strict()

// Export the type and example
export type getUserByTokenDTO = z.infer<typeof getUserByTokenSchema>
export { getUserByTokenExample as getUserByTokenInput }
