import { TokenService } from './token.service'
import { PasswordService } from "./password.service"
import dayjs, { Dayjs } from 'dayjs'

import { UserService } from '../user/user.service'
const PasswordServiceClass = new PasswordService(8)
import { createClient } from '@supabase/supabase-js'
import { User } from "../user/User"
import { SignUpCredentials, ResetPasswordCredentialType, EmailResetPasswordCredentialDto } from "./Credentials"
import { SignUpOutputDTO } from "./signUpOutput.dto"
const sendinblueUrl = "https://api.sendinblue.com/v3/smtp/email";

import jwt from '@tsndr/cloudflare-worker-jwt'
const sendinblueApiKey = "example-of-key";

export class AuthService {

  constructor(private readonly userService: UserService, private readonly tokenService: TokenService) { }

  loginUserWithEmailAndPassword = async (
    email: string,
    password: string
  ): Promise<any> => {
    const user = await this.userService.findOne(email)
    // If password is null then the user must login with a social account
    if (!user) {
      throw new Error('email invalid')
    }
    const compareResponse = await PasswordServiceClass.compare(password, user.password)
    console.log(compareResponse, "compareREspancecompareREspance")
    if (!compareResponse) {
      throw new Error('password invalid')
    }
    return user
  }

  createUser = async (
    username: string,
    password: string,
    firstName: string,
    lastName: string,
    role: string,
  ): Promise<SignUpOutputDTO | null> => {
    const userExist = await this.userService.findOne(username)
    if (!userExist) {
      const user = await this.userService.create({
        data: {
          username: username,
          password: password,
          roles: [role],
          firstName: firstName,
          lastName: lastName,
        },
      });
      if (user && user.id)
        return {
          id: user.id,
          firstName: user.firstName,
          lastName: user.lastName,
          username: user.username,
          roles: user.roles,
        };
    }
    return null;
  };


  signUp = async (data: SignUpCredentials): Promise<SignUpOutputDTO> => {
    const { username, password, firstName, lastName, role, rememberMe } = data;

    const user = await this.createUser(username, password, firstName, lastName, role);

    if (!user) {
      throw new Error("Username already exists");
    }

    //@ts-ignore
    const accessToken = await this.tokenService.generateAuthTokens(user
    );

    return { accessToken: accessToken.access.token, ...user };
  };

  sendEmail = async (
    email: string,
    link: string,
    apiKey: string | undefined,
    sender?: string,
    subject?: string,
    textContent?: string,
    name?: string,

  ) => {
    const emailSubject = subject ?? "Réinitialisation du mot de passe";
    const emailHtmlContent = `<div><p>Suivez ce lien pour réinitialiser le mot de passe de votre utilisateur :  </p><p><a href="${link}">Réinitialisation du mot de passe</a></p></div>`;
    const emailTextContent = textContent ?? "Did you forget your password?";

    const sendSMTPemail = {
      to: [
        {
          email: email,
        },
      ],
      sender: {
        email: "anis.ghabi@tekab.dev",
        name: "anis",
      },
      textContent: emailTextContent,
      subject: emailSubject,
      htmlContent: emailHtmlContent,
    };

    try {
      await fetch(sendinblueUrl, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "api-key": `${apiKey}`
        },
        body: JSON.stringify(sendSMTPemail)
      })



    } catch (error) {
      console.error("Email Sender Error: ", JSON.stringify(error));
      throw new Error(`Email Sender Error: ${JSON.stringify(error)}`);
    }

  }
  generateToken = (userId: string) => jwt.sign({
    iss: 'tekab',
    sub: userId,
    exp: dayjs().add(1, 'day').unix(),
  }, "anis");
  generateLink = (url: string, token: string) => `${url}/auth/password-reset?token=${token}`;

  isValidToken = async (userId: string) => {
    try {
      const user = await this.userService.findOneById(userId)
      if (!user) throw new Error("Username doesn't exist");
      else return true;
    } catch (error) {
      return false;
    }
  };

  resetPasswordWithEmail = async (url: any, data: EmailResetPasswordCredentialDto, brevoKey?: string) => {
    try {
      const user = await this.userService.findOne(data.username);
      if (!user) {
        throw new Error(data.username + "Username doesn't exist");
      } else {
        const jwt_token = await this.generateToken(user.id); // generate token
        console.log(jwt_token, "jwt_token")
        const link = this.generateLink(url, jwt_token); // generate link
        console.log(link, "linklink")
        await this.sendEmail(data.username, link, brevoKey); // send email
      }
    } catch (error) {
      console.log("Error:", error);
      throw new Error(`error: ${error}`);
    }
  };


  resetPassword = async (data: ResetPasswordCredentialType) => {
    try {
      var decoded: any = await jwt.verify(data.token, "anis");
      const payload: any = jwt.decode(data.token);
      console.log(payload.payload.sub)
      const validToken = await this.isValidToken(payload.payload.sub);
      if (validToken) {
        // update password
        await this.userService.update({
          where: { id: payload.payload.sub },
          data: {
            password: data.newPassword,
          },
        });
      } else {
        throw new Error(`Expired token`);
      }
    } catch (error) {
      console.log("Error:", error);
      throw new Error(`error: ${error}`);
    }
  };

  getUserByToken = async (token: string) => {
    try {
      var decoded: any = await jwt.verify(token, "anis");
      const payload: any = jwt.decode(token);
      console.log(payload.payload.sub)
      const user = await this.userService.findOneById(payload.payload.sub);

      if (user) {
        const newToken = await this.tokenService.generateAuthTokens(user);

        return { user, token: newToken };
      } else {
        throw new Error("Couldn't find user");
      }
    } catch (e: any) {
      console.log("Error", e);
      throw new Error(e);
    }
  };

  inviteUserByEmail = async (url: any, user: User, brevoKey: string): Promise<void> => {
    try {
      if (!user) {
        throw new Error("User doesn't exist");
      } else {
        const jwt_token = await this.generateToken(user.id); // generate token
        const link = this.generateLink(url, jwt_token); // generate link
        const SUBJECT = "Invitation";
        const EMAIL_CONTENT = "Merci de confirmer cette invitation en cliquant sur le lien et en complétant vos informations.";
        const SENDER = "anis.ghabi@tekab.dev";
        const NAME = "tekabAdmin";
        await this.sendEmail(user.username, brevoKey, link, SENDER, SUBJECT, EMAIL_CONTENT, NAME); // send email
      }
    } catch (error) {
      console.log("Error:", error);
      throw new Error(`error: ${error}`);
    }
  };


}
