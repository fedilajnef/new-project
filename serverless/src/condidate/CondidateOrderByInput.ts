import { z } from '@hono/zod-openapi'

const sortEnum = z.enum(['asc', 'desc'])

const condidateOrderByInputSchema = z.object({
    id: sortEnum.optional(), 
    createdAt: sortEnum.optional(), 
    updatedAt: sortEnum.optional(), 
    deletedAt: sortEnum.optional(), 
    name: sortEnum.optional(), 
    familyName: sortEnum.optional(), 
    resume: sortEnum.optional(), 
    email: sortEnum.optional()}).strict();

const CondidateOrderByInput = condidateOrderByInputSchema.openapi({
  example: {
    id: 'asc', 
    createdAt: 'asc', 
    updatedAt: 'asc', 
    deletedAt: 'asc', 
    name: 'asc', 
    familyName: 'asc', 
    resume: 'asc', 
    email: 'asc'  
}
})

export { CondidateOrderByInput }
