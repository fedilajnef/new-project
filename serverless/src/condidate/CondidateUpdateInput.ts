import { z } from '@hono/zod-openapi'

// Define the schema for condidateUpdateInputSchema input using Zod
export const condidateUpdateInputSchema = z.object({ 
  id: z.optional(z.string()),
  createdAt: z.optional(z.union([z.date(), z.string()])), 
  updatedAt: z.optional(z.union([z.date(), z.string(), z.null()])), 
  deletedAt: z.optional(z.union([z.date(), z.string(), z.null()])), 
  name: z.optional(z.union([
  z.string(),
 z.null()
])), 
  familyName: z.optional(z.union([
  z.string(),
 z.null()
])), 
  resume: z.optional(z.union([
  z.string(),
 z.null()
])), 
  email: z.optional(z.union([
  z.string(),
 z.null()
]))}).strict() 

// Generate an OpenAPI example for condidate input
const openApiExample = condidateUpdateInputSchema.openapi({ 
  example: {
    id: '550e8400-e29b-41d4-a716-446655440000', 
    createdAt: new Date('2023-08-24T10:00:00Z'), 
    updatedAt: new Date('2023-08-24T10:00:00Z'), 
    deletedAt: new Date('2023-08-24T10:00:00Z'), 
    name: 'name', 
    familyName: 'familyName', 
    resume: 'resume', 
    email: 'email'  
}
}).strict() 

// Define a type for condidate input using the generated example
export type UpdateCondidate = z.infer <typeof condidateUpdateInputSchema>

// Export the generated OpenAPI example as condidate
export { openApiExample as CondidateUpdateInput } 
