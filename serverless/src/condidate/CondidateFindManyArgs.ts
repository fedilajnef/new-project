import { z } from '@hono/zod-openapi'
import { CondidateOrderByInput } from './CondidateOrderByInput'
import { CondidateWhereInput } from './CondidateWhereInput'
// Export the generated OpenAPI example as condidate
const condidateFindManyArgsSchema = z.object({
  where: z
  .object({
    where: z.object({
      AND: z.union([CondidateWhereInput, z.array(CondidateWhereInput)]).optional(),
      OR: z.array(CondidateWhereInput).optional(),
      NOT: z.union([CondidateWhereInput, z.array(CondidateWhereInput)]).optional()
    }).merge(CondidateWhereInput)
  })
  .optional(),
  orderBy: z.object({ orderBy: CondidateOrderByInput.optional() }).optional(),
// eslint-disable-next-line @typescript-eslint/no-unsafe-argument
  skip: z.string().optional().refine((value: any) => value === undefined || !isNaN(value), {
    message: 'Invalid value for skip, must be a number or undefined'
  }).openapi(({ example: '0' })), // Use a number example instead of string

  // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
  take: z.string().optional().refine((value: any) => value === undefined || !isNaN(value), {
    message: 'Invalid value for take, must be a number or undefined'
  }).openapi(({ example: '10' }))
})
export { condidateFindManyArgsSchema as CondidateFindManyArgs }
