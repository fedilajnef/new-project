import { z } from '@hono/zod-openapi'

// Define the schema for Condidate input using Zod
export const condidateSchema = z.object({ 
  id: z.string({
                required_error: 'id is required',
                invalid_type_error: 'id must be a string'
            }),
  createdAt: z.optional(z.union([z.date(), z.string()])), 
  updatedAt: z.optional(z.union([z.date(), z.string(), z.null()])), 
  deletedAt: z.optional(z.union([z.date(), z.string(), z.null()])), 
  name: z.optional(z.union([
  z.string(),
 z.null()
])), 
  familyName: z.optional(z.union([
  z.string(),
 z.null()
])), 
  resume: z.optional(z.union([
  z.string(),
 z.null()
])), 
  email: z.optional(z.union([
  z.string(),
 z.null()
]))}) 

// Generate an OpenAPI example for Condidate input
const openApiExample = condidateSchema.openapi({ 
  example: {
    id: '550e8400-e29b-41d4-a716-446655440000', 
    createdAt: new Date('2023-08-24T10:00:00Z'), 
    updatedAt: new Date('2023-08-24T10:00:00Z'), 
    deletedAt: new Date('2023-08-24T10:00:00Z'), 
    name: 'name', 
    familyName: 'familyName', 
    resume: 'resume', 
    email: 'email'  
}
}).strict() 

// Define a type for Condidate input using the generated example
export type Condidate = z.infer<typeof openApiExample> 

// Export the generated OpenAPI example as Condidate
export { openApiExample as condidateDto }
