import type { Prisma,Condidate } from '@prisma/client/edge'
import type { PaginatedInterface } from '../util/PaginatedInterface'
import { prisma } from '../config/database'


export class CondidateService {

findMany = async(
  args: Prisma.CondidateFindManyArgs
): Promise<PaginatedInterface<Condidate>> => {
  try {
    const [data, totalCount] = await Promise.all([
      prisma.condidate.findMany(args),
      prisma.condidate.count({ where: { deletedAt: null } })
    ])
    return { paginatedResult: data, totalCount }
  } catch (error: any) {
    throw new Error(`Error updating Condidate: ${ error.message }`)
  }
}

// find a single Condidate
findOne = async (
  id: string | undefined
): Promise<Condidate | null > => {
  try {
    const response = await prisma.condidate.findUnique({
      where: { id: id }
    })
    return response
  } catch (error: any) {
    throw new Error(`Error updating Condidate: ${ error.message }`)
  }
}

// update a Condidate by ID
update = async (
  args: Prisma.CondidateUpdateArgs
): Promise<Condidate> => {
  try {
    const updated = await prisma.condidate.update(args)
    return updated
  } catch (error: any) {
    throw new Error(`Error updating Condidate: ${ error.message }`)
  }
}

// create a new Condidate
create = async (
  args: Prisma.CondidateCreateArgs
): Promise<Condidate> => {
  try {
    const newRecord = await prisma.condidate.create(args)
    return newRecord
  } catch (error: any) {
      throw new Error(`Error creating Condidate: ${ error.message }`)
  }
}

// delete existing Condidate
delete = async (
  args: Prisma.CondidateDeleteArgs
): Promise<Condidate> => {
  try {
    const deleted = await prisma.condidate.delete(args)
    return deleted
  } catch (error: any) {
    throw new Error(`Error deleting Condidate: ${ error.message }`)
  }
}

// create many Condidates
createMany = async (
  args: Prisma.CondidateCreateManyArgs
): Promise<Prisma.BatchPayload> => {
  try {
    return await prisma.condidate.createMany(args)
  }catch(error:any) {
    throw new Error(`Error updating Condidate: ${ error.message }`)
  }
}

// count Condidates
count = async (
  args: Prisma.SelectSubset<null, Prisma.CondidateFindManyArgs>
): Promise<number> => {
  try {
    return await prisma.condidate.count(args)
  } catch (error: any){
    throw new Error(`Error updating Condidate: ${ error.message }`)
  }
}

// update many Condidates
updateMany = async (
  args: Prisma.CondidateUpdateManyArgs
): Promise<Prisma.BatchPayload> => {
  try {
    return await prisma.condidate.updateMany(args)
  } catch (error: any) {
    throw new Error(`Error updating Condidate: ${ error.message }`)
  }
}

// soft delete a Condidate
  softDelete = async (
    args: Prisma.CondidateUpdateArgs
  ): Promise<any> => {
    try {
      const deleted = await prisma.condidate.update(args)
      return deleted
    } catch (error: any) {
      throw new Error(`Error soft deleting Condidate: ${ error.message }`)
    }
  }
}

