 import { z } from '@hono/zod-openapi'
import { condidateDto } from './Condidate'

const getListCondidateDtoSchema = z.object({
    paginatedResult: z.array(condidateDto), // An array of condidate objects
    totalCount: z.number() // Total count of  condidate 
}).strict() 

// Generate an OpenAPI example for condidate input
export const GetListCondidateDto = getListCondidateDtoSchema.openapi({ 
  example: {

     paginatedResult: [ 
{
    id: '550e8400-e29b-41d4-a716-446655440000', 
    createdAt: new Date('2023-08-24T10:00:00Z'), 
    updatedAt: new Date('2023-08-24T10:00:00Z'), 
    deletedAt: new Date('2023-08-24T10:00:00Z'), 
    name: 'name', 
    familyName: 'familyName', 
    resume: 'resume', 
    email: 'email' 
}
 ], 
 totalCount: 1
 } 
}) 

