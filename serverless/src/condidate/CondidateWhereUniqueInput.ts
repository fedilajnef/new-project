import { z } from '@hono/zod-openapi'

// Define the schema for condidateWhereUniqueInputSchema input using Zod
const CondidateWhereUniqueInputSchema = z.object({ 
  id: z.string({
                required_error: 'id is required',
                invalid_type_error: 'id must be a string'
            })
}).strict(); 

// Generate an OpenAPI example for condidate input
const openApiExample = CondidateWhereUniqueInputSchema.openapi({ 
  example: {
    id: '550e8400-e29b-41d4-a716-446655440000'  
} 
}) 

// Export the generated OpenAPI example as condidate
export { openApiExample as CondidateWhereUniqueInput  } 
