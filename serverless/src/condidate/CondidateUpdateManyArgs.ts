import { z } from '@hono/zod-openapi'
import { CondidateOrderByInput } from './CondidateOrderByInput'
import { CondidateWhereInput } from './CondidateWhereInput'
// Export the generated OpenAPI example as condidate
const condidateUpdateManyArgsSchema = z.object({
  where: z
  .object({
    where: z.object({
      AND: z.union([CondidateWhereInput, z.array(CondidateWhereInput)]).optional(),
      OR: z.array(CondidateWhereInput).optional(),
      NOT: z.union([CondidateWhereInput, z.array(CondidateWhereInput)]).optional()
    }).merge(CondidateWhereInput)
  })
  .optional(),
})

export type UpdateCondidate = z.infer<typeof condidateUpdateManyArgsSchema>
export { condidateUpdateManyArgsSchema as CondidateUpdateManyArgs }
