import * as XLSX from 'xlsx'
import { auth } from '../middlewares/auth'
import { checkPermission } from '../util/checkPermission'
import { processArgs } from '../util/ProcessArgs'
import { flattenObject } from '../util/flattenObject'
import { createRoute } from '@hono/zod-openapi'
import type { Prisma } from '@prisma/client/edge'
import type { CondidateService } from './condidate.service'
import { CondidateCreateInput } from './CondidateCreateInput'
import { CondidateFindManyArgs } from './CondidateFindManyArgs'
import { CondidateWhereUniqueInput } from './CondidateWhereUniqueInput'
import { CondidateUpdateInput } from './CondidateUpdateInput'
import { GetListCondidateDto } from './getListCondidate.dto'
import {condidateDto} from './Condidate'
import { errorHandler } from '../middlewares/errorHandlingMiddleware'

export class CondidateControllerBase {
  // eslint-disable-next-line @typescript-eslint/space-before-function-paren
  constructor (
    private readonly condidateService: CondidateService
  ) { }
  create: any = async (
    c: any
  ) => {
    try {
      await auth(c)
      const payloadRoles: string[] = c.get('payload').roles
      const permission = checkPermission(payloadRoles, 'create:any')
      const rolesString = payloadRoles.join(', ')
      if (!permission) {
        return c.json(
          {
            code: 403,
            message: `Permission denied. Condidate creation is not allowed for roles: ${rolesString}.`
          },
          403
        )
      }
      const bodyParse = await c.req.json()
      // If permission check passes, create ENTITY_NAME
      const response = await this.condidateService.create({ 
        data: {
   name: bodyParse.name,
   familyName: bodyParse.familyName,
   resume: bodyParse.resume,
   email: bodyParse.email}
    })

    return c.json({ response, code: 200 })
  } catch (error: any) {
    return await errorHandler(error,c)
  }
}

routeCreate = createRoute({
  method: 'post',
  path: '/condidate',
  tags: ['condidate'],
  request: {
    body: {
      content: {
        'application/json': {
          schema: CondidateCreateInput
          }
        }
      }
    },
  responses: {
    200: {
      content: {
        'application/json': {
          schema: CondidateCreateInput
        }
      },
      description: 'Create a new Condidate'
    }
  }
})

// Find many condidate entity
findMany: any = async (c: any) => {
  try {
    await auth(c)
    const payloadRoles: string[] = c.get('payload').roles
    const permission = checkPermission(payloadRoles, 'read:any')
    const rolesString = payloadRoles.join(', ')
    if (!permission) {
      return c.json(
        {
          code: 403,
          message: `Permission denied. Condidate finding is not allowed for roles: ${rolesString}.`
        },
        403
      )
    }

    // Parse and process query parameters
    const queryParse = c.req.query()
    const args = processArgs(queryParse)
    // Fetch ENTITY_NAME with selected fields
    const result = await this.condidateService.findMany({
      where: args?.where as Prisma.CondidateWhereInput,

      orderBy: args?.orderBy as Prisma.CondidateOrderByWithRelationInput,

      skip: args.skip !== undefined ? parseInt(args?.skip) : undefined,

      take: args.take !== undefined ? parseInt(args?.take) : undefined,

      select: {
   id: true,
   name: true,
   familyName: true,
   resume: true,
   email: true}
      })

      // Return paginated result and total count
      return c.json({ paginatedResult: result.paginatedResult, totalCount: result.totalCount, code: 200 })
    } catch (error: any) {
      return await errorHandler(error,c)
  }
}

routeFindMany = createRoute({
  method: 'get',
  path: '/condidate',
  tags: ['condidate'],
  request: {
    query: CondidateFindManyArgs
    },
  responses: {
    200: {
      content: {
        'application/json': {
          schema: GetListCondidateDto
        }
      },
      description: 'Retrieve a list of Condidate'
    }
  }
})

// Update condidate entity
update: any = async (c: any) => {
  try {
    await auth(c)
    const payloadRoles: string[] = c.get('payload').roles
    const permission = checkPermission(payloadRoles, 'update:any')
    const rolesString = payloadRoles.join(', ')
    if (!permission) {
      return c.json(
        {
          code: 403,
          message: `Permission denied. Condidate updating is not allowed for roles: ${rolesString}.`
        },
        403
      )
    }

    // Parse request parameters and body
    const paramsParse = c.req.param()
    const bodyParse = await c.req.json()
    const params = CondidateWhereUniqueInput.parse(paramsParse)
    const response = await this.condidateService.update({
      where: { id: params.id },
      data: {
   name: bodyParse.name,
   familyName: bodyParse.familyName,
   resume: bodyParse.resume,
   email: bodyParse.email}
})
    return c.json(response, 200)
  } catch (error: any) {
     return await errorHandler(error,c)
  }
}

routeUpdate = createRoute({
  method: 'patch',
  path: '/condidate/{id}',
  tags: ['condidate'],
  request: {
    params: CondidateWhereUniqueInput,
    body: {
      content: {
        'application/json': {
          schema: CondidateUpdateInput
        }
      }
    }
  },
  responses: {
    200: {
      content: {
        'application/json': {
          schema: condidateDto
        }
      },
      description: 'Update a Condidate'
    }
  }
})

// Delete condidate entity
delete: any = async (c: any) => {
  try {
    await auth(c)
    const payloadRoles: string[] = c.get('payload').roles
    const permission = checkPermission(payloadRoles, 'delete:any')
    const rolesString = payloadRoles.join(', ')
    if (!permission) {
      return c.json(
        {
          code: 403,
          message: `Permission denied. Condidate deleting is not allowed for roles: ${rolesString}.`
         },
         403
      )
    }
    // Parse request parameters
    const paramsParse = c.req.param()
    const params = CondidateWhereUniqueInput.parse(paramsParse)
    // Delete
    const response = await this.condidateService.delete({
      where: { id: params.id }
    })

    return c.json(response, 200)
  } catch (error: any) {
    return await errorHandler(error,c)
  }
}

routeDelete = createRoute({
  method: 'delete',
  path: '/condidate/{id}',
  tags: ['condidate'],
  request: {
    params: CondidateWhereUniqueInput
  },
  responses: {
    200: {
      content: {
        'application/json': {
          schema: condidateDto
        }
      },
      description: 'Delete a Condidate'
    }
  }
})

// Find condidate Data For Excel
findDataForExcel: any = async (c: any) => {
  try {
    await auth(c)
    const payloadRoles: string[] = c.get('payload').roles
    const permission = checkPermission(payloadRoles, 'delete:any')
    const rolesString = payloadRoles.join(', ')
    if (!permission) {
      return c.json(
        {
          code: 403,
          message: `Permission denied. Condidate finding is not allowed for roles: ${rolesString}.`
        },
        403
      )
    }
    // Parse and process query parameters
    const queryParse = c.req.query()
    const args = CondidateFindManyArgs.strict().parse(processArgs(queryParse))
    // Fetch condidatedata
    const result = await this.condidateService.findMany({
      where: args?.where as Prisma.CondidateWhereInput,

      orderBy: args?.orderBy as Prisma.CondidateOrderByWithRelationInput,

      skip: args.skip !== undefined ? parseInt(args?.skip) : undefined,

      take: args.take !== undefined ? parseInt(args?.take) : undefined,

      select: { id : true, 
createdAt : true, 
updatedAt : true, 
deletedAt : true, 
name : true, 
familyName : true, 
resume : true, 
email : true}
    })
    // Flatten the result for Excel
    const flatResult = result.paginatedResult.map((obj) => flattenObject(obj))
    // Convert to Excel format
    const excelFile = XLSX.utils.json_to_sheet(flatResult)
    const Workbook = XLSX.utils.book_new()
    await XLSX.utils.book_append_sheet(Workbook, excelFile, 'data')
    // Convert to base64 and return as a file
    const file = await XLSX.write(Workbook, {
      bookType: 'xlsx',
      bookSST: true,
      type: 'base64'
    })
    return c.json({ file }, 200)
  } catch (error: any) {
    return await errorHandler(error,c)
  }
}

routeFindDataExcel = createRoute({
  method: 'get',
  path: '/condidate/fileExcel',
  tags: ['condidate'],
  responses: {
    200: {
      content: {
        'application/json': {
          schema: CondidateFindManyArgs
        }
      },
      description: 'Retrieve Condidate data in Excel format'
    }
  }
})

// Update condidate entity
findOne: any = async (c: any) => {
  try {
    await auth(c)
    const payloadRoles: string[] = c.get('payload').roles
    const permission = checkPermission(payloadRoles, 'read:own')
    const rolesString = payloadRoles.join(', ')
    if (!permission) {
      return c.json(
        {
          code: 403,
          message: `Permission denied. Condidate finding One is not allowed for roles: ${rolesString}.`
        },
        403
      )
    }
    // Parse request parameters and body
    const paramsParse = c.req.param()
    const params = CondidateWhereUniqueInput.parse(paramsParse)
const Condidate = await this.condidateService.findOne(params.id)
    return c.json(Condidate, 200)
  } catch (error: any) {
    return await errorHandler(error,c)
  }
}

routeFindOne = createRoute({
  method: 'get',
  path: '/condidate/{id}',
  tags: ['condidate'],
  request: {
    params: CondidateWhereUniqueInput
  },
  responses: {
    200: {
      content: {
        'application/json': {
          schema: condidateDto
        }
      },
      description: 'find one Condidate'
    }
  }
})
   
// soft deleting condidate entity
softDelete: any = async (c: any) => {
  try {
      await auth(c)
      const payloadRoles: string[] = c.get('payload').roles
      const permission = checkPermission(payloadRoles, 'update:any')
      const rolesString = payloadRoles.join(', ')
      if (!permission) {
        return c.json(
          {
            code: 403,
            message: `Permission denied. Condidate  soft deleting  is not allowed for roles: ${rolesString}.`        
          },
          403
        )
      }

      // Parse request parameters and body
      const paramsParse = c.req.param()
      const bodyParse = await c.req.json()
      const params = CondidateWhereUniqueInput.parse(paramsParse)
      const response = await this.condidateService.softDelete(
        {
          where: { id: params.id },
          data: { 
            deletedAt: bodyParse.deletedAt
          }
        }
      )
    return c.json(response, 200)
  } catch (error: any) {
    return await errorHandler(error,c)
  }
}

routeSoftDelete = createRoute({
  method: 'patch',
  path: '/condidate/softDelete/{id}',
  tags: ['condidate'],
  request: {
    params: CondidateWhereUniqueInput,
    body: {
      content: {
        'application/json': {
          schema: CondidateUpdateInput
        }
      }
    }
  },
  responses: {
    200: {
      content: {
        'application/json': {
          schema: condidateDto
        }
      },
      description: 'soft deleting a Condidate'
    }
  }
})
}

