/* Initialize Prisma Client with the datasoruces string
*  { db : { url : '@string'} }
*  the url is a string and will generate an error if you passed in other data type
*/
/* eslint-disable @typescript-eslint/explicit-function-return-type */
import { PrismaClient } from '@prisma/client/edge'
function prismaa(url: string) {
    const client = new PrismaClient({
        datasources: {
            db: {
                url
            }
        }
    })
    return client
}

// Initialize Prisma Client
export const prisma = prismaa(`${PRISMA_ACCELERATE_URL}`)
