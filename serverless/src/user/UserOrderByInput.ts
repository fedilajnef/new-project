import { z } from '@hono/zod-openapi'

import { condidateInJobofferDto } from '../condidateInJoboffer/CondidateInJoboffer'
const sortEnum = z.enum(['asc', 'desc'])

const userOrderByInputSchema = z.object({
  password: sortEnum.optional(),
    id: sortEnum.optional(), 
    createdAt: sortEnum.optional(), 
    updatedAt: sortEnum.optional(), 
    deletedAt: sortEnum.optional(), 
    firstName: sortEnum.optional(), 
    lastName: sortEnum.optional(), 
    username: sortEnum.optional(), 
    isValid: sortEnum.optional(), 
    roles: sortEnum.optional(), 
  condidateInJoboffers:  z.array(sortEnum)}).strict();

const UserOrderByInput = userOrderByInputSchema.openapi({
  example: {
  password: 'asc',
    id: 'asc', 
    createdAt: 'asc', 
    updatedAt: 'asc', 
    deletedAt: 'asc', 
    firstName: 'asc', 
    lastName: 'asc', 
    username: 'asc', 
    isValid: 'asc', 
    roles: 'asc', 
  condidateInJoboffers: ['asc']  
}
})

export { UserOrderByInput }
