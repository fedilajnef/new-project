import { z } from '@hono/zod-openapi'
import { UserOrderByInput } from './UserOrderByInput'
import { UserWhereInput } from './UserWhereInput'
// Export the generated OpenAPI example as user
const userFindManyArgsSchema = z.object({
  where: z
  .object({
    where: z.object({
      AND: z.union([UserWhereInput, z.array(UserWhereInput)]).optional(),
      OR: z.array(UserWhereInput).optional(),
      NOT: z.union([UserWhereInput, z.array(UserWhereInput)]).optional()
    }).merge(UserWhereInput)
  })
  .optional(),
  orderBy: z.object({ orderBy: UserOrderByInput.optional() }).optional(),
// eslint-disable-next-line @typescript-eslint/no-unsafe-argument
  skip: z.string().optional().refine((value: any) => value === undefined || !isNaN(value), {
    message: 'Invalid value for skip, must be a number or undefined'
  }).openapi(({ example: '0' })), // Use a number example instead of string

  // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
  take: z.string().optional().refine((value: any) => value === undefined || !isNaN(value), {
    message: 'Invalid value for take, must be a number or undefined'
  }).openapi(({ example: '10' }))
})
export { userFindManyArgsSchema as UserFindManyArgs }
