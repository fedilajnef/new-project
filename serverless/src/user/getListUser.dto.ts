 import { z } from '@hono/zod-openapi'
import { userDto } from './User'

import { condidateInJobofferDto } from '../condidateInJoboffer/CondidateInJoboffer'
const getListUserDtoSchema = z.object({
    paginatedResult: z.array(userDto), // An array of user objects
    totalCount: z.number() // Total count of  user 
}).strict() 

// Generate an OpenAPI example for user input
export const GetListUserDto = getListUserDtoSchema.openapi({ 
  example: {

     paginatedResult: [ 
{
  password: null,
    id: '550e8400-e29b-41d4-a716-446655440000', 
    createdAt: new Date('2023-08-24T10:00:00Z'), 
    updatedAt: new Date('2023-08-24T10:00:00Z'), 
    deletedAt: new Date('2023-08-24T10:00:00Z'), 
    firstName: 'firstName', 
    lastName: 'lastName', 
    username: 'username', 
    isValid: true, 
    roles: ['admin', 'user'], 
  condidateInJoboffers: [{ id: '550e8400-e29b-41d4-a716-446655440000' }] 
}
 ], 
 totalCount: 1
 } 
}) 

