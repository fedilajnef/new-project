import { z } from '@hono/zod-openapi'
import { UserOrderByInput } from './UserOrderByInput'
import { UserWhereInput } from './UserWhereInput'
// Export the generated OpenAPI example as user
const userUpdateManyArgsSchema = z.object({
  where: z
  .object({
    where: z.object({
      AND: z.union([UserWhereInput, z.array(UserWhereInput)]).optional(),
      OR: z.array(UserWhereInput).optional(),
      NOT: z.union([UserWhereInput, z.array(UserWhereInput)]).optional()
    }).merge(UserWhereInput)
  })
  .optional(),
})

export type UpdateUser = z.infer<typeof userUpdateManyArgsSchema>
export { userUpdateManyArgsSchema as UserUpdateManyArgs }
