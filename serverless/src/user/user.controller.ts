import * as XLSX from 'xlsx'
import { auth } from '../middlewares/auth'
import { checkPermission } from '../util/checkPermission'
import { processArgs } from '../util/ProcessArgs'
import { flattenObject } from '../util/flattenObject'
import { createRoute } from '@hono/zod-openapi'
import type { Prisma } from '@prisma/client/edge'
import type { UserService } from './user.service'
import { UserCreateInput } from './UserCreateInput'
import { UserFindManyArgs } from './UserFindManyArgs'
import { UserWhereUniqueInput } from './UserWhereUniqueInput'
import { UserUpdateInput } from './UserUpdateInput'
import { GetListUserDto } from './getListUser.dto'
import {userDto} from './User'
import { errorHandler } from '../middlewares/errorHandlingMiddleware'
import type { StatusCode } from 'hono/utils/http-status'
import { UserUpdateManyArgs } from './UserUpdateManyArgs'
import { BatchPayload } from '../BatchPayload'
import { prisma } from '../config/database'
import { PasswordService } from '../auth/password.service'

export interface Env {
  Linkedin_CLIENT_ID: string
  Linkedin_CLIENT_SECRET: string
  CF_ROUTE: string
  SITE_URL : string
}
const PasswordServiceClass = new PasswordService(8)
export class UserControllerBase {
  // eslint-disable-next-line @typescript-eslint/space-before-function-paren
  constructor(private readonly userService: UserService) 
  {}

// create user new record
create: any = async (c: any) => {
  try {
      await auth(c)
      const payloadRoles: string[] = c.get('payload').roles
      const permission = checkPermission(payloadRoles, 'create:any')
      const rolesString = payloadRoles.join(', ')
      if (!permission) {
        return c.json(
          {
            code: 403,
            message: `Permission denied. User creation is not allowed for roles: ${rolesString}.`
          },
          403
        )
      }
      const bodyParse = await c.req.json()
      const body = UserCreateInput.parse(bodyParse)
      // If permission check passes
      const response = await this.userService.create({
            data: {
   firstName: bodyParse.firstName,
   lastName: bodyParse.lastName,
   username: bodyParse.username,
   isValid: bodyParse.isValid,
   roles: bodyParse.roles}
    })
    return c.json({ response, code: 200 })
  } catch (error: any) { 
    return await errorHandler(error,c)
  }
}

routeCreate = createRoute({
  method: 'post',
  path: '/user',
  tags: ['user'],
  request: {
    body: {
      content: {
        'application/json': {
          schema: UserCreateInput
        }
      }
    }
  },
  responses: {
    200: {
      content: {
        'application/json': {
          schema: UserCreateInput
        }
      },
      description: 'Create a new User'
    }
  }
})

// Find many ENTITY_NAME
findMany: any = async (c: any) => {
  try {
    await auth(c)
    const payloadRoles: string[] = c.get('payload').roles
    const permission = checkPermission(payloadRoles, 'read:any')
    const rolesString = payloadRoles.join(', ')
    if (!permission) {
      return c.json(
        {
          code: 403,
          message: `Permission denied. User finding is not allowed for roles: ${rolesString}.`
        },
        403
      )
    }

    // Parse and process query parameters
    const queryParse = c.req.query()
    const args = processArgs(queryParse)

    // Fetch ENTITY_NAME with selected fields
    const result = await this.userService.findMany({
    where: args?.where as Prisma.UserWhereInput,

    orderBy: args?.orderBy as Prisma.UserOrderByWithRelationInput,

    skip: args.skip !== undefined ? parseInt(args?.skip) : undefined,

    take: args.take !== undefined ? parseInt(args?.take) : undefined,

    select: {
  id : true, 
  createdAt : true, 
  updatedAt : true, 
  deletedAt : true, 
  firstName : true, 
  lastName : true, 
  username : true, 
  isValid : true, 
  roles : true, 
  condidateInJoboffers : true}
    })
    // Return paginated result and total count
    return c.json({ paginatedResult: result.paginatedResult, totalCount: result.totalCount, code: 200 })
  } catch (error: any) {
    return await errorHandler(error,c)
  }
}

routeFindMany = createRoute({
  method: 'get',
  path: '/user',
  tags: ['user'],
  request: {
    query: UserFindManyArgs
    },
  responses: {
    200: {
      content: {
        'application/json': {
          schema: GetListUserDto
        }
      },
      description: 'Retrieve a list of User'
    },
  }
})

// Update ENTITY_NAME
update: any = async (c: any) => {
  try {
    await auth(c)
    const payloadRoles: string[] = c.get('payload').roles
    const permission = checkPermission(payloadRoles, 'update:any')
    const rolesString = payloadRoles.join(', ')
    if (!permission) {
      return c.json(
        {
          code: 403,
          message: `Permission denied. User updating is not allowed for roles: ${rolesString}.`
        },
        403
      )
    }

    // Parse request parameters and body
    const paramsParse = c.req.param()
    const bodyParse = await c.req.json()
    const params = UserWhereUniqueInput.parse(paramsParse)
    const body = UserUpdateInput.parse(bodyParse)

    const response = await this.userService.update({
      where: { id: params.id },
      data: {
   firstName: bodyParse.firstName,
   lastName: bodyParse.lastName,
   username: bodyParse.username,
   isValid: bodyParse.isValid,
   roles: bodyParse.roles},
    })
    return c.json(response, 200)
  } catch (error: any) {
    return await errorHandler(error,c)
  }
}

routeUpdate = createRoute({
  method: 'patch',
  path: '/user/{id}',
  tags: ['user'],
  request: {
    params: UserWhereUniqueInput,
  body: {
  content: {
    'application/json': {
      schema: UserUpdateInput
        }
      }
    }
  },
  responses: {
    200: {
      content: {
        'application/json': {
          schema: userDto
        }
      },
      description: 'Update a User'
    }
  }
})

// Delete 
delete: any = async (c: any) => {
  try {
    await auth(c)
    const payloadRoles: string[] = c.get('payload').roles
    const permission = checkPermission(payloadRoles, 'delete:any')
    const rolesString = payloadRoles.join(', ')
    if (!permission) {
      return c.json(
        {
          code: 403,
          message: `Permission denied. User deleting is not allowed for roles: ${rolesString}.`
        },
        403
      )
    }

    // Parse request parameters
    const paramsParse = c.req.param()
    const params = UserWhereUniqueInput.parse(paramsParse)

    // Delete 
    const response = await this.userService.delete({
    where: { id: params.id }
    })

    return c.json(response, 200)
  } catch (error: any) {
    return await errorHandler(error,c)
  }
}

routeDelete = createRoute({
  method: 'delete',
  path: '/user/{id}',
  tags: ['user'],
  request: {
    params: UserWhereUniqueInput
  },
  responses: {
    200: {
      content: {
        'application/json': {
          schema: userDto,
        }
      },
      description: 'Delete a User'
    }
  }
})

createMany: any = async (c: any) => {
  try {
    await auth(c)
    const payloadRoles: string[] = c.get('payload').roles
    const permission = checkPermission(payloadRoles, 'create:many')
    const rolesString = payloadRoles.join(', ')
    if (!permission) {
      return c.json(
        {
          code: 403,
          message: `Permission denied. User creating Many Users  is not allowed for roles: ${rolesString}.` 
        },
        403
      )
    }

    const bodyParse = await c.req.json()
    const body = UserCreateInput.array().parse(bodyParse)

    // If permission check passes, create the user
    const users = await this.userService.createMany(
      { 
        data: {
   firstName: bodyParse.firstName,
   lastName: bodyParse.lastName,
   username: bodyParse.username,
   isValid: bodyParse.isValid,
   roles: bodyParse.roles}
      }
    )
    return c.json(users, 200 as StatusCode)
  } catch (error:any) {
    return await errorHandler(error, c)
  }
}

routeCreateMany = createRoute({
  method: 'post',
  path: '/createMany',
  tags:['user'],
  request: {
    body: {
      content: {
        'application/json': {
          schema: UserCreateInput.array()
        }
      }
    }
  },
  responses: {
    200: {
      content: {
        'application/json': {
          schema: BatchPayload,
        }
      },
      description: 'Create many users'
    }
  }
})


// Update many user
updateMany: any = async (c: any) => {
    try {
      //checking auth
      await auth(c)
      const payloadRoles: string[] = c.get('payload').roles
      const permission = checkPermission(payloadRoles, 'update:any')
      const rolesString = payloadRoles.join(', ')
      if (!permission) {
        return c.json(
          {
            code: 403,
            message: `Permission denied. User deleting is not allowed for roles: ${rolesString}.`
          },
          403
        )
      }

      // Parse and process query parameters
      const queryParse = c.req.query()
      const args = UserUpdateManyArgs.strict().parse(processArgs(queryParse))
      const bodyParse = await c.req.json()
      const body =  UserUpdateInput.parse(bodyParse)

      // Update many User
      const user = await this.userService.updateMany({
        where: args?.where as Prisma.UserWhereInput,

        data: {
   firstName: bodyParse.firstName,
   lastName: bodyParse.lastName,
   username: bodyParse.username,
   isValid: bodyParse.isValid,
   roles: bodyParse.roles}
      })
      return c.json(user, 200 as StatusCode)
    } catch (error: any) {
      return await errorHandler(error, c)
    }
}

routeUpdateMany = createRoute({
    method: 'patch',
    tags: ['user'],
    path: '/updateMany',
    request: {
      query:UserUpdateManyArgs,
      body: {
        content: {
          'application/json': {
            schema: UserUpdateInput
          }
        }
      }
    },
    responses: {
      200: {
        content: {
          'application/json': {
            schema: BatchPayload
          }
        },
        description: 'Update many Users'
      }
    }
})

// Find Data For Excel
findDataForExcel: any = async (c: any) => {
  try {
    await auth(c)
    const payloadRoles: string[] = c.get('payload').roles
    const permission = checkPermission(payloadRoles, 'read:any')
    const rolesString = payloadRoles.join(', ')
    if (!permission) {
      return c.json(
        {
          code: 403,
          message: `Permission denied. User finding is not allowed for roles: ${rolesString}.`
        },
        403
      )
    }
    // Parse and process query parameters
    const queryParse = c.req.query()
    const args = UserFindManyArgs.strict().parse(processArgs(queryParse))

    // Fetch data

    const result = await this.userService.findMany({
    where: args?.where as Prisma.UserWhereInput,
    orderBy: args?.orderBy as Prisma.UserOrderByWithRelationInput,
    skip: args.skip !== undefined ? parseInt(args?.skip) : undefined,

    take: args.take !== undefined ? parseInt(args?.take) : undefined,

    select: { id : true, 
createdAt : true, 
updatedAt : true, 
deletedAt : true, 
firstName : true, 
lastName : true, 
username : true, 
isValid : true, 
roles : true, 
condidateInJoboffers : true}
    })

    // Flatten the result for Excel
    const flatResult = result.paginatedResult.map((obj) => flattenObject(obj))

    // Convert to Excel format
    const excelFile = XLSX.utils.json_to_sheet(flatResult)
    const Workbook = XLSX.utils.book_new()
    // eslint-disable-next-line @typescript-eslint/await-thenable, @typescript-eslint/no-confusing-void-expression
    await XLSX.utils.book_append_sheet(Workbook, excelFile, 'data')

    // Convert to base64 and return as a file
    const file = await XLSX.write(Workbook, {
      bookType: 'xlsx',
      bookSST: true,
      type: 'base64'
    })

    return c.json({ file }, 200)
  } catch (error: any) {
    return await errorHandler(error,c)
  }
}

routeFindDataExcel = createRoute({
  method: 'get',
  path: '/user/fileExcel',
  tags: ['user'],
  responses: {
    200: {
      content: {
        'application/json': {
          schema: UserFindManyArgs
        }
      },
      description: 'Retrieve User data in Excel format'
    }
  }
})

// Update User
findOne: any = async (c: any) => {
  try {
    await auth(c)
    const payloadRoles: string[] = c.get('payload').roles
    const permission = checkPermission(payloadRoles, 'read:own')
    const rolesString = payloadRoles.join(', ')
    if (!permission) {
      return c.json(
        {
          code: 403,
          message: `Permission denied. User finding One is not allowed for roles: ${rolesString}.`
        },
        403
      )
    }

    // Parse request parameters and body
    const paramsParse = c.req.param()
    const params = UserWhereUniqueInput.parse(paramsParse)
const User = await this.userService.findOneById(params.id)
    return c.json(User, 200)
  } catch (error: any) {
    return await errorHandler(error,c)
  }
}

routeFindOne = createRoute({
  method: 'get',
  path: '/user/{id}',
  tags: ['user'],
  request: {
    params: UserWhereUniqueInput
  },
  responses: {
    200: {
      content: {
        'application/json': {
          schema: userDto
        }
      },
      description: 'find one User'
    }
  }
})

// soft deleting
softDelete: any = async (c: any) => {
    try {
      await auth(c)
      const payloadRoles: string[] = c.get('payload').roles
      const permission = checkPermission(payloadRoles, 'update:any')
      const rolesString = payloadRoles.join(', ')
      if (!permission) {
        return c.json(
          {
            code: 403,
            message: `Permission denied. User  soft deleting  is not allowed for roles: ${rolesString}.`
          },
          403
        )
      }

    // Parse request parameters and body
    const paramsParse = c.req.param()
    const bodyParse = await c.req.json()
    const params = UserWhereUniqueInput.parse(paramsParse)
    const response = await this.userService.softDelete(
      {
        where: { id: params.id },
        data: { 
          deletedAt: bodyParse.deletedAt
        }
      }
    )
    return c.json(response, 200)
  } catch (error: any) {
    return await errorHandler(error,c)
  }
}

routeSoftDelete = createRoute({
  method: 'patch',
  path: '/user/softDelete/{id}',
  tags: ['user'],
  request: {
    params: UserWhereUniqueInput,
    body: {
      content: {
        'application/json': {
          schema: UserUpdateInput
        }
      }
    }
  },
  responses: {
    200: {
      content: {
        'application/json': {
          schema: userDto
        }
      },
      description: 'soft deleting a User'
    }
  }
})

  /**  oauth with linkedin callback function */
  callback: any = async (c: any, next: any) => {
    try {
      const code = await c.req.query('code')
      const state = await c.req.query('state')

      // Check the state to prevent CSRF attacks
      if (state !== 'random_state_string') {
        return c.json({ message: 'Invalid state parameter' })
      }

      // Exchange the authorization code for an access token
      const redirectUri = `${c.env.CF_ROUTE}/auth/callback`
      const params = new URLSearchParams({
        grant_type: 'authorization_code',
        client_id: `${c.env.Linkedin_CLIENT_ID}`,
        client_secret: `${c.env.Linkedin_CLIENT_SECRET}`,
        code
      })

      const query = params.toString()
      const fullUrl = `https://www.linkedin.com/oauth/v2/accessToken?${query}&redirect_uri=${decodeURIComponent(redirectUri)}`

      const response = await fetch(fullUrl, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      })

      const data: any = await response.json()
      const accessToken = data.access_token
      const refreshToken = data.refresh_token
      // We are Using the access token to fetch user information from LinkedIn
      const linkedinProfileUrl = 'https://api.linkedin.com/v2/userinfo?projection=(sub,name,given_name,family_name,picture,locale,email_verified,email)'
      const profileResponse = await fetch(linkedinProfileUrl, {
        method: 'GET',
        headers: {
          // eslint-disable-next-line quote-props
          'Authorization': `Bearer ${accessToken}`,
          'Content-Type': 'application/json'
        }
      })

      if (profileResponse.status === 200) {
        const linkedinUser: any = await profileResponse.json()
        const firstName = linkedinUser.family_name
        const lastName = linkedinUser.given_name
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const userId = linkedinUser.sub
        const email = linkedinUser.email
        const verified = linkedinUser.email_verified
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const picture = linkedinUser.picture
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const name = linkedinUser.name
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const local = linkedinUser.locale
        const data = {
          username: email,
          firstName: firstName,
          lastName: lastName,
          password: await PasswordServiceClass.hash(accessToken),
          isValid: verified,
          roles: ['user']
        }
        // adding or updating linkedin user into supabase
        const user = await prisma.user.upsert({
          where: { username: data.username },
          update: {
            ...data
          },
          create: data,
          select: {
            id: true // selecting the id to combin it with the returned data
          }
        })

        const redirectUrl = `${c.env.SITE_URL}/auth/sign-in`

        // Append the query parameters to the redirect URL
        // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
        const fullRedirectUrl = `${redirectUrl}?id=${decodeURIComponent(user.id)}&username=${data.username}&firstName=${data.firstName}&lastName=${data.lastName}&roles=${[data.roles]}&isValid=${data.isValid}&accessToken=${accessToken}&refreshToken=${refreshToken}`

        return c.redirect(fullRedirectUrl)
    } else {
      console.error('Error fetching LinkedIn profile:', profileResponse)
    }
  } catch (error: any) {
    return await errorHandler(error,c)
  }
}

routeCallBack = createRoute({
  method: 'get',
  path: '/auth/callback',
  tags: ['user'],
  responses: {
    200: {
      content: {
        'application/x-www-form-urlencoded': {
          schema: {}
        }
      },
      description: 'oauth with linkedin callback url'
    }
  }
})

linkdinOauth: any = async (c: any) => {
  try{
    const linkedinAuthUrl = 'https://www.linkedin.com/oauth/v2/authorization'
    const queryParams = new URLSearchParams({
      response_type: 'code',
      client_id: `${c.env.Linkedin_CLIENT_ID}`,
      scope: 'email openid profile r_organization_social w_member_social w_organization_social r_basicprofile r_1st_connections_size rw_organization_admin r_organization_admin', // LinkedIn scopes for basic profile and email
      state: 'random_state_string', // You should generate a random string for state
      redirect_uri: decodeURIComponent(`${c.env.CF_ROUTE}/auth/callback`)
    })

    const authUrl = `${linkedinAuthUrl}?${queryParams.toString()}`
    return c.json({ url: authUrl })
  }catch(error:any){
    return await errorHandler(error,c)
  }
}

routeLinkdinOauth = createRoute({
  method: 'get',
  path: '/auth/linkedin',
  tags: ['user'],
  responses: {
    200: {
      content: {
        'application/json': {
          schema: {
            url: 'https://www.linkedin.com/oauth/v2/authorization?response_type=""&client_id=""&scope=""&state=""&redirect_uri=""'
          }
        }
      },
      description: 'oauth with linkedin'
    }
  }
})
}
