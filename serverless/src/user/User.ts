import { z } from '@hono/zod-openapi'

import { condidateInJobofferDto } from '../condidateInJoboffer/CondidateInJoboffer'
// Define the schema for User input using Zod
export const userSchema = z.object({ 
  password: z.optional(z.union([z.string(), z.null()])),
  id: z.string({
                required_error: 'id is required',
                invalid_type_error: 'id must be a string'
            }),
  createdAt: z.optional(z.union([z.date(), z.string()])), 
  updatedAt: z.optional(z.union([z.date(), z.string(), z.null()])), 
  deletedAt: z.optional(z.union([z.date(), z.string(), z.null()])), 
  firstName: z.optional(z.union([
  z.string(),
 z.null()
])), 
  lastName: z.optional(z.union([
  z.string(),
 z.null()
])), 
  username: z.string().optional(), 
  isValid: z.optional(z.union([z.boolean(), z.null()])), 
  roles: z.optional(z.union([z.array(z.string()), z.null()])), 
  condidateInJoboffers: z.optional(z.union([z.array(condidateInJobofferDto), z.null()]))}) 

// Generate an OpenAPI example for User input
const openApiExample = userSchema.openapi({ 
  example: {
  password: null,
    id: '550e8400-e29b-41d4-a716-446655440000', 
    createdAt: new Date('2023-08-24T10:00:00Z'), 
    updatedAt: new Date('2023-08-24T10:00:00Z'), 
    deletedAt: new Date('2023-08-24T10:00:00Z'), 
    firstName: 'firstName', 
    lastName: 'lastName', 
    username: 'username', 
    isValid: true, 
    roles: ['admin', 'user'], 
  condidateInJoboffers: [
 {
 id: '550e8400-e29b-41d4-a716-446655440000' 
} 
]  
}
}).strict() 

// Define a type for User input using the generated example
export type User = z.infer<typeof openApiExample> 

// Export the generated OpenAPI example as User
export { openApiExample as userDto }
