import type { Prisma,User } from '@prisma/client/edge'
import type { PaginatedInterface } from '../util/PaginatedInterface'
import { prisma } from '../config/database'
import { PasswordService } from '../auth/password.service'

const passwordService  = new PasswordService(8)

export class UserService {

findMany = async(
  args: Prisma.UserFindManyArgs
): Promise<PaginatedInterface<User>> => {
  try {
    const [data, totalCount] = await Promise.all([
      prisma.user.findMany(args),
      prisma.user.count({ where: { deletedAt: null } })
    ])
    return { paginatedResult: data, totalCount }
  } catch (error: any) {
    throw new Error(`Error finding Users: ${ error.message }`)
  }
}

findOne = async (
  username: string
): Promise<User | null > => {
  try {
    const user = await prisma.user.findUnique({
       where: { username:username }
    })
    return user
  } catch (error: any) {
    throw new Error(`Error fecthing User by email: ${ error.message }`)
  }
}

// Function to get a user by username (id)
findOneById = async (
  id: string
): Promise<User | null> => {
  try {
    const user = await prisma.user.findUnique({
      where: { id }
    })
    return user
  } catch (error:any) {
    throw new Error(`Error fetching user by id: ${error.message}`)
  }
}

// Function to update a User by ID
update = async (
  args: Prisma.UserUpdateArgs
): Promise<User> => {
  try {
    // Hash the password if it is provided in the update data
    // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
    if (args.data.password) {
      args.data.password = await passwordService.hash(args.data.password)
    }
    const updated = await prisma.user.update(args)
    return updated
  } catch (error: any) {
    throw new Error(`Error updating User: ${ error.message }`)
  }
}

// Function to create a new User
create = async (
  args: Prisma.UserCreateArgs
): Promise<User> => {
  try {
    // Hash the password before creating the user
    // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
    if (args.data.password) {
      args.data.password = await passwordService.hash(args.data.password)
    }
    const newUser = await prisma.user.create(args)
    return newUser
  } catch (error: any) {
      throw new Error(`Error creating User: ${ error.message }`)
  }
}

// Function to delete a old User
delete = async (
  args: Prisma.UserDeleteArgs
): Promise<User> => {
  try {
    const deleted = await prisma.user.delete(args)
    return deleted
  } catch (error: any) {
    throw new Error(`Error deleting User: ${ error.message }`)
  }
}

// Function to create many User
createMany = async (
  args: Prisma.UserCreateManyArgs
): Promise<Prisma.BatchPayload> => {
  try {
    return await prisma.user.createMany(args)
  }catch(error:any) {
    throw new Error(`Error updating User: ${ error.message }`)
  }
}

// Function to count User
count = async (
  args: Prisma.SelectSubset<null, Prisma.UserFindManyArgs>
): Promise<number> => {
  try {
    return await prisma.user.count(args)
  } catch (error: any){
    throw new Error(`Error updating User: ${ error.message }`)
  }
}

// Function to update many User
updateMany = async (
  args: Prisma.UserUpdateManyArgs
): Promise<Prisma.BatchPayload> => {
  try {
    return await prisma.user.updateMany(args)
  } catch (error: any) {
    throw new Error(`Error updating User: ${ error.message }`)
  }
}

// soft delete
  softDelete = async (
    args: Prisma.UserUpdateArgs
  ): Promise<any> => {
    try {
      const deleted = await prisma.user.update(args)
      return deleted
    } catch (error: any) {
      throw new Error(`Error soft deleting User: ${ error.message }`)
    }
  }
}
