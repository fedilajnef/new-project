import { z } from '@hono/zod-openapi'

// Define the schema for userWhereUniqueInputSchema input using Zod
const UserWhereUniqueInputSchema = z.object({ 
  id: z.string({
                required_error: 'id is required',
                invalid_type_error: 'id must be a string'
            })
}).strict(); 

// Generate an OpenAPI example for user input
const openApiExample = UserWhereUniqueInputSchema.openapi({ 
  example: {
    id: '550e8400-e29b-41d4-a716-446655440000'  
} 
}) 

// Export the generated OpenAPI example as user
export { openApiExample as UserWhereUniqueInput  } 
