import { OpenAPIHono } from '@hono/zod-openapi'
import { html } from 'hono/html'
import { cors } from 'hono/cors'
import { UserService } from './user/user.service'
import { AuthController } from './auth/auth.contoller'
import { AuthService} from './auth/auth.service'
import { UserControllerBase } from './user/user.controller'
import { TokenService } from './auth/token.service'
import { AppConfigControllerBase } from './appConfig/appConfig.controller'
import { AppConfigService } from './appConfig/appConfig.service'
import { logger } from 'hono/logger'
import { customLogger, infoLogger} from './middlewares/logger'
import { CondidateControllerBase } from './condidate/condidate.controller'
import { CondidateService } from './condidate/condidate.service'
import { CondidateInJobofferControllerBase } from './condidateInJoboffer/condidateInJoboffer.controller'
import { CondidateInJobofferService } from './condidateInJoboffer/condidateInJoboffer.service'
import { JobofferControllerBase } from './joboffer/joboffer.controller'
import { JobofferService } from './joboffer/joboffer.service'
const condidateController = new CondidateControllerBase(new CondidateService())
const condidateInJobofferController = new CondidateInJobofferControllerBase(new CondidateInJobofferService())
const jobofferController = new JobofferControllerBase(new JobofferService())
const userController = new UserControllerBase(new UserService())
const appConfigController = new AppConfigControllerBase(new AppConfigService())
const authController = new AuthController(new AuthService(new UserService(), new TokenService()),new UserService(),new TokenService())
const app = new OpenAPIHono()

app.get('/', (c: any) => c.json(200))
 
app.use('*', cors())

// log api url and method
app.use('*', async (c: any, next: any) => {
  // custom Logger
  logger(customLogger)
  infoLogger(`[${c.req.method}]${c.req.url}`)
  await next()
})

// condidate
app.openapi(condidateController.routeUpdate, condidateController.update)
app.openapi(condidateController.routeCreate, condidateController.create)
app.openapi(condidateController.routeFindMany, condidateController.findMany)
app.openapi(condidateController.routeDelete, condidateController.delete)
app.openapi(condidateController.routeFindDataExcel, condidateController.findDataForExcel)
app.openapi(condidateController.routeFindOne, condidateController.findOne)
app.openapi(condidateController.routeSoftDelete, condidateController.softDelete)
// condidateInJoboffer
app.openapi(condidateInJobofferController.routeUpdate, condidateInJobofferController.update)
app.openapi(condidateInJobofferController.routeCreate, condidateInJobofferController.create)
app.openapi(condidateInJobofferController.routeFindMany, condidateInJobofferController.findMany)
app.openapi(condidateInJobofferController.routeDelete, condidateInJobofferController.delete)
app.openapi(condidateInJobofferController.routeFindDataExcel, condidateInJobofferController.findDataForExcel)
app.openapi(condidateInJobofferController.routeFindOne, condidateInJobofferController.findOne)
app.openapi(condidateInJobofferController.routeSoftDelete, condidateInJobofferController.softDelete)
// joboffer
app.openapi(jobofferController.routeUpdate, jobofferController.update)
app.openapi(jobofferController.routeCreate, jobofferController.create)
app.openapi(jobofferController.routeFindMany, jobofferController.findMany)
app.openapi(jobofferController.routeDelete, jobofferController.delete)
app.openapi(jobofferController.routeFindDataExcel, jobofferController.findDataForExcel)
app.openapi(jobofferController.routeFindOne, jobofferController.findOne)
app.openapi(jobofferController.routeSoftDelete, jobofferController.softDelete)
// appConfig
app.openapi(appConfigController.routeUpdate, appConfigController.update) 
app.openapi(appConfigController.routeCreate, appConfigController.create) 
app.openapi(appConfigController.routeFindMany, appConfigController.findMany) 
app.openapi(appConfigController.routeDelete, appConfigController.delete) 
app.openapi(appConfigController.routeFindDataExcel, appConfigController.findDataForExcel) 
app.openapi(appConfigController.routeFindOne, appConfigController.findOne) 
app.openapi(appConfigController.routeSoftDelete, appConfigController.softDelete) 
// User
app.openapi(userController.routeUpdate, userController.update) 
app.openapi(userController.routeCreate, userController.create) 
app.openapi(userController.routeFindMany, userController.findMany) 
app.openapi(userController.routeDelete, userController.delete) 
app.openapi(userController.routeCreateMany, userController.createMany) 
app.openapi(userController.routeUpdateMany, userController.updateMany) 
app.openapi(userController.routeFindDataExcel, userController.findDataForExcel) 
app.openapi(userController.routeFindOne, userController.findOne) 
app.openapi(userController.routeSoftDelete, userController.softDelete) 
app.openapi(userController.routeCallBack, userController.callback)
app.openapi(userController.routeLinkdinOauth, userController.linkdinOauth)
// auth
app.openapi(authController.routeSignUpUser, authController.signUp) 
app.openapi(authController.routeLoginUser, authController.login) 
app.openapi(authController.routeResetPasswordWithEmail, authController.resetPasswordWithEmail) 
app.openapi(authController.routeGetUserByToken, authController.getUserByToken) 
app.openapi(authController.routeInviteUser, authController.inviteUser) 
app.openapi(authController.routeInviteManyUser, authController.inviteManyUser) 
app.openapi(authController.routeResetPassword, authController.resetPassword) 
app.get('/swagger', (c:any) => {
return c.html(
   html`<!DOCTYPE html>
           <html lang="en">
             <head>               <meta charset="utf - 8"/>
               <meta name="viewport" content="width=device-width, initial-scale=1"/>
               <meta name="description" content="SwaggerUI"/>
               <title> SwaggerUI </title>
               <link rel="stylesheet" href="https://unpkg.com/swagger-ui-dist@4.5.0/swagger-ui.css"/>
             </head>
           <body>
           <div id="swagger-ui"></div>
           <script src="https://unpkg.com/swagger-ui-dist@4.5.0/swagger-ui-bundle.js" crossorigin></script>
           <script>
                   window.onload = () => {
                    // Fetch JSON data from the URL
                     fetch("${process.env.CF_ROUTE}/doc",{mode:"no-cors"})
                          .then(response => response.json())
                          .then(data => {
                    // Modify the JSON data by adding the "components" object
                           data.components ={
                                "securitySchemes": {
                                      "bearer": {
                                          "bearerFormat": "JWT",
                                          "scheme": "bearer",
                                          "type": "http"
                                       }
                                  }
                           };
                         // Create Swagger UI with the modified data
                         window.ui = SwaggerUIBundle({
                                       spec: data, // Use the modified JSON data
                                       dom_id: '#swagger-ui',
                                     });
                              })
                             .catch(error => {
                                   console.error("Error fetching data:", error.message);
                             });
                    };
           </script>
           </body>
           </html>
`)
})

// The OpenAPI documentation will be available at /doc
app.doc('/doc', {
    openapi: '3.0.0',
    info: {
      version: '1.0.0',
      title: 'Serverless API'
    },
    security: [{ bearer: [] }]
})
export default app
