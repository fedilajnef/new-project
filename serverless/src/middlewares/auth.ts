import jwt from '@tsndr/cloudflare-worker-jwt'
import { tokenTypes } from '../../src/config/tokens'

const authenticate = async (jwtToken: string, secret: string) => {
  let authorized = false
  let payload
  try {
    authorized = await jwt.verify(jwtToken, secret)
    const decoded = jwt.decode(jwtToken)
    payload = decoded.payload
    authorized = authorized && payload.type === tokenTypes.ACCESS
  } catch (e) { }
  return { authorized, payload }
};

export const auth = async (c: any) => {
  const credentials = c.req.headers.get('Authorization')
  console.log(credentials, 'credentialscredentials')
  if (!credentials) {
    throw new Error('Please authenticate')
  }

  const parts = credentials.split(/\s+/)
  console.log(parts, 'partspartsparts')
  if (parts.length !== 2) {
    throw new Error('Please authenticate')
  }

  const jwtToken = parts[1];
  const { authorized, payload } = await authenticate(jwtToken, 'anis')
  console.log(payload, 'payloadpayload')
  if (!authorized || !payload) {
    throw new Error('Please authenticate')
  } else {
    c.set('payload', payload)
  }
};