import { customLogger } from "./logger"

enum HttpStatus {
    Unauthorized = 401,
    BadRequest = 400,
    NoFound = 404,
    InternalServerError = 500
}

export function errorHandler(err: any, c: any): Promise<any> {
    if (typeof err.message === 'string' && err.message.includes('Please authenticate')) {
        customLogger(err.message, c)
        return c.json({ message: err.message, status: HttpStatus.Unauthorized }, HttpStatus.Unauthorized);
    } else if (typeof err.message === 'string' && (err.message.includes('Record to delete does not exist.') || err.message.includes('not found'))) {
        customLogger(err.message, c)
        return c.json({ message: err.message, status: HttpStatus.NoFound }, HttpStatus.NoFound)
    } else if (typeof err.message === 'string' && (err.message.includes('Error creating') || err.message.includes('Error updating') || err.message.includes('Error deleting'))) {
        customLogger(err.message, c)
        return c.json({ message: err.message, status: HttpStatus.BadRequest }, HttpStatus.BadRequest)
    } else {
        customLogger(err.message, c)
        return c.json({ message: err.message, status: HttpStatus.InternalServerError }, HttpStatus.InternalServerError)
    }
}
