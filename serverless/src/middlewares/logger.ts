export const customLogger = (message: any, ...rest: string[]) => {
    const reset = '\x1b[0m' // Reset color
    const red = '\x1b[31m' // Red color
    const yellow = '\x1b[33m' // Yellow color
    const cyan = '\x1b[36m' // Cyan color

    if (typeof message === 'string') {
        if (message.includes('404') || message.includes('401') || message.includes('400') || message.includes('500')) {
            errorLogger(message, ...rest)
        } else if (message.includes('warning')) {
            warningLogger(message, ...rest)
        } else {
            infoLogger(message, ...rest)
        }
    } else {
        // Handle the case when message is not a string
        console.error(`${red}[ERROR]${reset} Invalid log message format. Expected string, but got:`, message, ...rest)
    }
}

export const errorLogger = (message: string, ...rest: string[]) => {
    const reset = '\x1b[0m' // Reset color
    const red = '\x1b[31m' // Red color

    console.error(`${red}[ERROR]${reset} ${message}`, ...rest)
}

export const warningLogger = (message: string, ...rest: string[]) => {
    const reset = '\x1b[0m' // Reset color
    const yellow = '\x1b[33m' // Yellow color

    console.warn(`${yellow}[WARNING]${reset} ${message}`, ...rest)
}

export const infoLogger = (message: string, ...rest: string[]) => {
    const reset = '\x1b[0m' // Reset color
    const cyan = '\x1b[36m'
    console.log(`${cyan}[INFO]${reset} ${message}`, ...rest)
}
