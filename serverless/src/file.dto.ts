import { z } from '@hono/zod-openapi'

const FileDtoSchema = z.object({
  file: z.string() // Assuming 'file' represents a string; adjust the type accordingly
})
const openAPISchema = FileDtoSchema.openapi({
  example: {
    file: 'example_file'
  }
})
export { openAPISchema as FileDto }