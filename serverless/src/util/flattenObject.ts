export const flattenObject = (obj: any, parentKey: string = '', result: Record<string, any> = {}): Record<string, any> => {
  for (const key in obj) {
    // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions, no-prototype-builtins
    if (obj.hasOwnProperty(key)) {
      // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
      const newKey = parentKey ? `${parentKey}_${key}` : key

      if (typeof obj[key] === 'object' && obj[key] !== null && !Array.isArray(obj[key])) {
        flattenObject(obj[key], newKey, result)
      } else {
        result[newKey] = obj[key]
      }
    }
  }

  return result
}
