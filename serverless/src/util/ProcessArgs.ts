// eslint-disable-next-line @typescript-eslint/explicit-function-return-type, @typescript-eslint/space-before-function-paren
export function processArgs(args: any) {
  const where: Record<string, any> = {}
  const orderBy: Record<string, string> = {}

  // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
  for (const key of Object.keys(args)) {
    if (key.startsWith('where[')) {
      const field = key.substring(6, key.length - 1).replace(/\]\[/g, '.')
      const value = args[key]
      // Use a recursive function to build the nested structure
      setNestedValue(where, field, value)
    } else if (key.startsWith('orderBy[')) {
      const field = key.substring(8, key.length - 1)
      orderBy[field] = args[key] as string
    }
  }

  const structuredData: any = {}

  if (Object.keys(where).length > 0) {
    structuredData.where = where
  }

  if (Object.keys(orderBy).length > 0) {
    structuredData.orderBy = { orderBy }
  }

  if (typeof args.skip !== 'undefined') {
    structuredData.skip = args.skip
  }

  if (typeof args.take !== 'undefined') {
    structuredData.take = args.take
  }

  if (typeof args.where === 'undefined') {
    structuredData.where = { ...where, deletedAt: null }
  }

  return structuredData
}

// Recursive function to set nested values
// eslint-disable-next-line @typescript-eslint/explicit-function-return-type, @typescript-eslint/space-before-function-paren
function setNestedValue(obj: Record<string, any>, path: string, value: any) {
  const keys = path.split('.')
  let currentObj = obj

  for (let i = 0; i < keys.length; i++) {
    const key = keys[i]
    // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
    if (!currentObj[key]) {
      currentObj[key] = {}
    }

    if (i === keys.length - 1) {
      currentObj[key] = value
    } else {
      currentObj = currentObj[key]
    }
  }
}
