import grants from '../grants.json'
// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export function checkPermission(roles: string[], action: any) {
  for (const role of roles) {
    for (let index = 0; index < grants.length; index++) {
      const grant = grants[index]
      if (role === grant.role && grant.action === action) {
        return true // User has the required permission in at least one role
      }
    }
  }

  return false // User does not have the required permission in any role
}
