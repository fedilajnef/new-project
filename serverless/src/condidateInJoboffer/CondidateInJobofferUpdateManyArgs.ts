import { z } from '@hono/zod-openapi'
import { CondidateInJobofferOrderByInput } from './CondidateInJobofferOrderByInput'
import { CondidateInJobofferWhereInput } from './CondidateInJobofferWhereInput'
// Export the generated OpenAPI example as condidateInJoboffer
const condidateInJobofferUpdateManyArgsSchema = z.object({
  where: z
  .object({
    where: z.object({
      AND: z.union([CondidateInJobofferWhereInput, z.array(CondidateInJobofferWhereInput)]).optional(),
      OR: z.array(CondidateInJobofferWhereInput).optional(),
      NOT: z.union([CondidateInJobofferWhereInput, z.array(CondidateInJobofferWhereInput)]).optional()
    }).merge(CondidateInJobofferWhereInput)
  })
  .optional(),
})

export type UpdateCondidateInJoboffer = z.infer<typeof condidateInJobofferUpdateManyArgsSchema>
export { condidateInJobofferUpdateManyArgsSchema as CondidateInJobofferUpdateManyArgs }
