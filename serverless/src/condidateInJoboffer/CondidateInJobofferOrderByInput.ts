import { z } from '@hono/zod-openapi'

const sortEnum = z.enum(['asc', 'desc'])

const condidateInJobofferOrderByInputSchema = z.object({
    id: sortEnum.optional(), 
    createdAt: sortEnum.optional(), 
    updatedAt: sortEnum.optional(), 
    deletedAt: sortEnum.optional(), 
  jobIdId: sortEnum.optional(),
    questions: sortEnum.optional(), 
    responses: sortEnum.optional(), 
  condidateIdId: sortEnum.optional()}).strict();

const CondidateInJobofferOrderByInput = condidateInJobofferOrderByInputSchema.openapi({
  example: {
    id: 'asc', 
    createdAt: 'asc', 
    updatedAt: 'asc', 
    deletedAt: 'asc', 
  jobIdId: 'asc',
    questions: 'asc', 
    responses: 'asc', 
  condidateIdId: 'asc'  
}
})

export { CondidateInJobofferOrderByInput }
