import { z } from '@hono/zod-openapi'
import { CondidateInJobofferOrderByInput } from './CondidateInJobofferOrderByInput'
import { CondidateInJobofferWhereInput } from './CondidateInJobofferWhereInput'
// Export the generated OpenAPI example as condidateInJoboffer
const condidateInJobofferFindManyArgsSchema = z.object({
  where: z
  .object({
    where: z.object({
      AND: z.union([CondidateInJobofferWhereInput, z.array(CondidateInJobofferWhereInput)]).optional(),
      OR: z.array(CondidateInJobofferWhereInput).optional(),
      NOT: z.union([CondidateInJobofferWhereInput, z.array(CondidateInJobofferWhereInput)]).optional()
    }).merge(CondidateInJobofferWhereInput)
  })
  .optional(),
  orderBy: z.object({ orderBy: CondidateInJobofferOrderByInput.optional() }).optional(),
// eslint-disable-next-line @typescript-eslint/no-unsafe-argument
  skip: z.string().optional().refine((value: any) => value === undefined || !isNaN(value), {
    message: 'Invalid value for skip, must be a number or undefined'
  }).openapi(({ example: '0' })), // Use a number example instead of string

  // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
  take: z.string().optional().refine((value: any) => value === undefined || !isNaN(value), {
    message: 'Invalid value for take, must be a number or undefined'
  }).openapi(({ example: '10' }))
})
export { condidateInJobofferFindManyArgsSchema as CondidateInJobofferFindManyArgs }
