import * as XLSX from 'xlsx'
import { auth } from '../middlewares/auth'
import { checkPermission } from '../util/checkPermission'
import { processArgs } from '../util/ProcessArgs'
import { flattenObject } from '../util/flattenObject'
import { createRoute } from '@hono/zod-openapi'
import type { Prisma } from '@prisma/client/edge'
import type { CondidateInJobofferService } from './condidateInJoboffer.service'
import { CondidateInJobofferCreateInput } from './CondidateInJobofferCreateInput'
import { CondidateInJobofferFindManyArgs } from './CondidateInJobofferFindManyArgs'
import { CondidateInJobofferWhereUniqueInput } from './CondidateInJobofferWhereUniqueInput'
import { CondidateInJobofferUpdateInput } from './CondidateInJobofferUpdateInput'
import { GetListCondidateInJobofferDto } from './getListCondidateInJoboffer.dto'
import {condidateInJobofferDto} from './CondidateInJoboffer'
import { errorHandler } from '../middlewares/errorHandlingMiddleware'

export class CondidateInJobofferControllerBase {
  // eslint-disable-next-line @typescript-eslint/space-before-function-paren
  constructor (
    private readonly condidateinjobofferService: CondidateInJobofferService
  ) { }
  create: any = async (
    c: any
  ) => {
    try {
      await auth(c)
      const payloadRoles: string[] = c.get('payload').roles
      const permission = checkPermission(payloadRoles, 'create:any')
      const rolesString = payloadRoles.join(', ')
      if (!permission) {
        return c.json(
          {
            code: 403,
            message: `Permission denied. CondidateInJoboffer creation is not allowed for roles: ${rolesString}.`
          },
          403
        )
      }
      const bodyParse = await c.req.json()
      // If permission check passes, create ENTITY_NAME
      const response = await this.condidateinjobofferService.create({ 
        data: {
   jobIdId: bodyParse.jobIdId,
   questions: bodyParse.questions,
   responses: bodyParse.responses,
   condidateIdId: bodyParse.condidateIdId}
    })

    return c.json({ response, code: 200 })
  } catch (error: any) {
    return await errorHandler(error,c)
  }
}

routeCreate = createRoute({
  method: 'post',
  path: '/condidateinjoboffer',
  tags: ['condidateinjoboffer'],
  request: {
    body: {
      content: {
        'application/json': {
          schema: CondidateInJobofferCreateInput
          }
        }
      }
    },
  responses: {
    200: {
      content: {
        'application/json': {
          schema: CondidateInJobofferCreateInput
        }
      },
      description: 'Create a new CondidateInJoboffer'
    }
  }
})

// Find many condidateinjoboffer entity
findMany: any = async (c: any) => {
  try {
    await auth(c)
    const payloadRoles: string[] = c.get('payload').roles
    const permission = checkPermission(payloadRoles, 'read:any')
    const rolesString = payloadRoles.join(', ')
    if (!permission) {
      return c.json(
        {
          code: 403,
          message: `Permission denied. CondidateInJoboffer finding is not allowed for roles: ${rolesString}.`
        },
        403
      )
    }

    // Parse and process query parameters
    const queryParse = c.req.query()
    const args = processArgs(queryParse)
    // Fetch ENTITY_NAME with selected fields
    const result = await this.condidateinjobofferService.findMany({
      where: args?.where as Prisma.CondidateInJobofferWhereInput,

      orderBy: args?.orderBy as Prisma.CondidateInJobofferOrderByWithRelationInput,

      skip: args.skip !== undefined ? parseInt(args?.skip) : undefined,

      take: args.take !== undefined ? parseInt(args?.take) : undefined,

      select: {
   id: true,
   jobId: {
                select:{
                  id:true

                }
              },
   questions: true,
   responses: true,
   condidateId: {
                select:{
                  id:true

                }
              }}
      })

      // Return paginated result and total count
      return c.json({ paginatedResult: result.paginatedResult, totalCount: result.totalCount, code: 200 })
    } catch (error: any) {
      return await errorHandler(error,c)
  }
}

routeFindMany = createRoute({
  method: 'get',
  path: '/condidateinjoboffer',
  tags: ['condidateinjoboffer'],
  request: {
    query: CondidateInJobofferFindManyArgs
    },
  responses: {
    200: {
      content: {
        'application/json': {
          schema: GetListCondidateInJobofferDto
        }
      },
      description: 'Retrieve a list of CondidateInJoboffer'
    }
  }
})

// Update condidateinjoboffer entity
update: any = async (c: any) => {
  try {
    await auth(c)
    const payloadRoles: string[] = c.get('payload').roles
    const permission = checkPermission(payloadRoles, 'update:any')
    const rolesString = payloadRoles.join(', ')
    if (!permission) {
      return c.json(
        {
          code: 403,
          message: `Permission denied. CondidateInJoboffer updating is not allowed for roles: ${rolesString}.`
        },
        403
      )
    }

    // Parse request parameters and body
    const paramsParse = c.req.param()
    const bodyParse = await c.req.json()
    const params = CondidateInJobofferWhereUniqueInput.parse(paramsParse)
    const response = await this.condidateinjobofferService.update({
      where: { id: params.id },
      data: {
   jobIdId: bodyParse.jobIdId,
   questions: bodyParse.questions,
   responses: bodyParse.responses,
   condidateIdId: bodyParse.condidateIdId}
})
    return c.json(response, 200)
  } catch (error: any) {
     return await errorHandler(error,c)
  }
}

routeUpdate = createRoute({
  method: 'patch',
  path: '/condidateinjoboffer/{id}',
  tags: ['condidateinjoboffer'],
  request: {
    params: CondidateInJobofferWhereUniqueInput,
    body: {
      content: {
        'application/json': {
          schema: CondidateInJobofferUpdateInput
        }
      }
    }
  },
  responses: {
    200: {
      content: {
        'application/json': {
          schema: condidateInJobofferDto
        }
      },
      description: 'Update a CondidateInJoboffer'
    }
  }
})

// Delete condidateinjoboffer entity
delete: any = async (c: any) => {
  try {
    await auth(c)
    const payloadRoles: string[] = c.get('payload').roles
    const permission = checkPermission(payloadRoles, 'delete:any')
    const rolesString = payloadRoles.join(', ')
    if (!permission) {
      return c.json(
        {
          code: 403,
          message: `Permission denied. CondidateInJoboffer deleting is not allowed for roles: ${rolesString}.`
         },
         403
      )
    }
    // Parse request parameters
    const paramsParse = c.req.param()
    const params = CondidateInJobofferWhereUniqueInput.parse(paramsParse)
    // Delete
    const response = await this.condidateinjobofferService.delete({
      where: { id: params.id }
    })

    return c.json(response, 200)
  } catch (error: any) {
    return await errorHandler(error,c)
  }
}

routeDelete = createRoute({
  method: 'delete',
  path: '/condidateinjoboffer/{id}',
  tags: ['condidateinjoboffer'],
  request: {
    params: CondidateInJobofferWhereUniqueInput
  },
  responses: {
    200: {
      content: {
        'application/json': {
          schema: condidateInJobofferDto
        }
      },
      description: 'Delete a CondidateInJoboffer'
    }
  }
})

// Find condidateinjoboffer Data For Excel
findDataForExcel: any = async (c: any) => {
  try {
    await auth(c)
    const payloadRoles: string[] = c.get('payload').roles
    const permission = checkPermission(payloadRoles, 'delete:any')
    const rolesString = payloadRoles.join(', ')
    if (!permission) {
      return c.json(
        {
          code: 403,
          message: `Permission denied. CondidateInJoboffer finding is not allowed for roles: ${rolesString}.`
        },
        403
      )
    }
    // Parse and process query parameters
    const queryParse = c.req.query()
    const args = CondidateInJobofferFindManyArgs.strict().parse(processArgs(queryParse))
    // Fetch condidateinjobofferdata
    const result = await this.condidateinjobofferService.findMany({
      where: args?.where as Prisma.CondidateInJobofferWhereInput,

      orderBy: args?.orderBy as Prisma.CondidateInJobofferOrderByWithRelationInput,

      skip: args.skip !== undefined ? parseInt(args?.skip) : undefined,

      take: args.take !== undefined ? parseInt(args?.take) : undefined,

      select: { id : true, 
createdAt : true, 
updatedAt : true, 
deletedAt : true, 
jobId : true, 
questions : true, 
responses : true, 
condidateId : true}
    })
    // Flatten the result for Excel
    const flatResult = result.paginatedResult.map((obj) => flattenObject(obj))
    // Convert to Excel format
    const excelFile = XLSX.utils.json_to_sheet(flatResult)
    const Workbook = XLSX.utils.book_new()
    await XLSX.utils.book_append_sheet(Workbook, excelFile, 'data')
    // Convert to base64 and return as a file
    const file = await XLSX.write(Workbook, {
      bookType: 'xlsx',
      bookSST: true,
      type: 'base64'
    })
    return c.json({ file }, 200)
  } catch (error: any) {
    return await errorHandler(error,c)
  }
}

routeFindDataExcel = createRoute({
  method: 'get',
  path: '/condidateinjoboffer/fileExcel',
  tags: ['condidateinjoboffer'],
  responses: {
    200: {
      content: {
        'application/json': {
          schema: CondidateInJobofferFindManyArgs
        }
      },
      description: 'Retrieve CondidateInJoboffer data in Excel format'
    }
  }
})

// Update condidateinjoboffer entity
findOne: any = async (c: any) => {
  try {
    await auth(c)
    const payloadRoles: string[] = c.get('payload').roles
    const permission = checkPermission(payloadRoles, 'read:own')
    const rolesString = payloadRoles.join(', ')
    if (!permission) {
      return c.json(
        {
          code: 403,
          message: `Permission denied. CondidateInJoboffer finding One is not allowed for roles: ${rolesString}.`
        },
        403
      )
    }
    // Parse request parameters and body
    const paramsParse = c.req.param()
    const params = CondidateInJobofferWhereUniqueInput.parse(paramsParse)
const CondidateInJoboffer = await this.condidateinjobofferService.findOne(params.id)
    return c.json(CondidateInJoboffer, 200)
  } catch (error: any) {
    return await errorHandler(error,c)
  }
}

routeFindOne = createRoute({
  method: 'get',
  path: '/condidateinjoboffer/{id}',
  tags: ['condidateinjoboffer'],
  request: {
    params: CondidateInJobofferWhereUniqueInput
  },
  responses: {
    200: {
      content: {
        'application/json': {
          schema: condidateInJobofferDto
        }
      },
      description: 'find one CondidateInJoboffer'
    }
  }
})
   
// soft deleting condidateinjoboffer entity
softDelete: any = async (c: any) => {
  try {
      await auth(c)
      const payloadRoles: string[] = c.get('payload').roles
      const permission = checkPermission(payloadRoles, 'update:any')
      const rolesString = payloadRoles.join(', ')
      if (!permission) {
        return c.json(
          {
            code: 403,
            message: `Permission denied. CondidateInJoboffer  soft deleting  is not allowed for roles: ${rolesString}.`        
          },
          403
        )
      }

      // Parse request parameters and body
      const paramsParse = c.req.param()
      const bodyParse = await c.req.json()
      const params = CondidateInJobofferWhereUniqueInput.parse(paramsParse)
      const response = await this.condidateinjobofferService.softDelete(
        {
          where: { id: params.id },
          data: { 
            deletedAt: bodyParse.deletedAt
          }
        }
      )
    return c.json(response, 200)
  } catch (error: any) {
    return await errorHandler(error,c)
  }
}

routeSoftDelete = createRoute({
  method: 'patch',
  path: '/condidateinjoboffer/softDelete/{id}',
  tags: ['condidateinjoboffer'],
  request: {
    params: CondidateInJobofferWhereUniqueInput,
    body: {
      content: {
        'application/json': {
          schema: CondidateInJobofferUpdateInput
        }
      }
    }
  },
  responses: {
    200: {
      content: {
        'application/json': {
          schema: condidateInJobofferDto
        }
      },
      description: 'soft deleting a CondidateInJoboffer'
    }
  }
})
}

