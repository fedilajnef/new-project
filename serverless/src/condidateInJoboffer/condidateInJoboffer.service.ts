import type { Prisma,CondidateInJoboffer } from '@prisma/client/edge'
import type { PaginatedInterface } from '../util/PaginatedInterface'
import { prisma } from '../config/database'


export class CondidateInJobofferService {

findMany = async(
  args: Prisma.CondidateInJobofferFindManyArgs
): Promise<PaginatedInterface<CondidateInJoboffer>> => {
  try {
    const [data, totalCount] = await Promise.all([
      prisma.condidateInJoboffer.findMany(args),
      prisma.condidateInJoboffer.count({ where: { deletedAt: null } })
    ])
    return { paginatedResult: data, totalCount }
  } catch (error: any) {
    throw new Error(`Error updating CondidateInJoboffer: ${ error.message }`)
  }
}

// find a single CondidateInJoboffer
findOne = async (
  id: string | undefined
): Promise<CondidateInJoboffer | null > => {
  try {
    const response = await prisma.condidateInJoboffer.findUnique({
      where: { id: id }
    })
    return response
  } catch (error: any) {
    throw new Error(`Error updating CondidateInJoboffer: ${ error.message }`)
  }
}

// update a CondidateInJoboffer by ID
update = async (
  args: Prisma.CondidateInJobofferUpdateArgs
): Promise<CondidateInJoboffer> => {
  try {
    const updated = await prisma.condidateInJoboffer.update(args)
    return updated
  } catch (error: any) {
    throw new Error(`Error updating CondidateInJoboffer: ${ error.message }`)
  }
}

// create a new CondidateInJoboffer
create = async (
  args: Prisma.CondidateInJobofferCreateArgs
): Promise<CondidateInJoboffer> => {
  try {
    const newRecord = await prisma.condidateInJoboffer.create(args)
    return newRecord
  } catch (error: any) {
      throw new Error(`Error creating CondidateInJoboffer: ${ error.message }`)
  }
}

// delete existing CondidateInJoboffer
delete = async (
  args: Prisma.CondidateInJobofferDeleteArgs
): Promise<CondidateInJoboffer> => {
  try {
    const deleted = await prisma.condidateInJoboffer.delete(args)
    return deleted
  } catch (error: any) {
    throw new Error(`Error deleting CondidateInJoboffer: ${ error.message }`)
  }
}

// create many CondidateInJoboffers
createMany = async (
  args: Prisma.CondidateInJobofferCreateManyArgs
): Promise<Prisma.BatchPayload> => {
  try {
    return await prisma.condidateInJoboffer.createMany(args)
  }catch(error:any) {
    throw new Error(`Error updating CondidateInJoboffer: ${ error.message }`)
  }
}

// count CondidateInJoboffers
count = async (
  args: Prisma.SelectSubset<null, Prisma.CondidateInJobofferFindManyArgs>
): Promise<number> => {
  try {
    return await prisma.condidateInJoboffer.count(args)
  } catch (error: any){
    throw new Error(`Error updating CondidateInJoboffer: ${ error.message }`)
  }
}

// update many CondidateInJoboffers
updateMany = async (
  args: Prisma.CondidateInJobofferUpdateManyArgs
): Promise<Prisma.BatchPayload> => {
  try {
    return await prisma.condidateInJoboffer.updateMany(args)
  } catch (error: any) {
    throw new Error(`Error updating CondidateInJoboffer: ${ error.message }`)
  }
}

// soft delete a CondidateInJoboffer
  softDelete = async (
    args: Prisma.CondidateInJobofferUpdateArgs
  ): Promise<any> => {
    try {
      const deleted = await prisma.condidateInJoboffer.update(args)
      return deleted
    } catch (error: any) {
      throw new Error(`Error soft deleting CondidateInJoboffer: ${ error.message }`)
    }
  }
}

