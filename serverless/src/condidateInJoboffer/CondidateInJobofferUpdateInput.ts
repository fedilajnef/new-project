import { z } from '@hono/zod-openapi'

// Define the schema for condidateInJobofferUpdateInputSchema input using Zod
export const condidateInJobofferUpdateInputSchema = z.object({ 
  id: z.optional(z.string()),
  createdAt: z.optional(z.union([z.date(), z.string()])), 
  updatedAt: z.optional(z.union([z.date(), z.string(), z.null()])), 
  deletedAt: z.optional(z.union([z.date(), z.string(), z.null()])), 
  jobIdId: z.optional(z.union([z.string(), z.null()])),
  questions: z.optional(z.union([z.object({}), z.null()])), 
  responses: z.optional(z.union([z.object({}), z.null()])), 
  condidateIdId: z.optional(z.union([z.string(), z.null()]))}).strict() 

// Generate an OpenAPI example for condidateInJoboffer input
const openApiExample = condidateInJobofferUpdateInputSchema.openapi({ 
  example: {
    id: '550e8400-e29b-41d4-a716-446655440000', 
    createdAt: new Date('2023-08-24T10:00:00Z'), 
    updatedAt: new Date('2023-08-24T10:00:00Z'), 
    deletedAt: new Date('2023-08-24T10:00:00Z'), 
  jobIdId: '550e8400-e29b-41d4-a716-446655440000',
    questions: {}, 
    responses: {}, 
  condidateIdId: '550e8400-e29b-41d4-a716-446655440000'  
}
}).strict() 

// Define a type for condidateInJoboffer input using the generated example
export type UpdateCondidateInJoboffer = z.infer <typeof condidateInJobofferUpdateInputSchema>

// Export the generated OpenAPI example as condidateInJoboffer
export { openApiExample as CondidateInJobofferUpdateInput } 
