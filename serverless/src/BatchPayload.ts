import { z } from '@hono/zod-openapi'

const BatchPayloadSchema = z.object({
  count: z.number().optional()
})

const openAPISchema = BatchPayloadSchema.openapi({
  example: {
    count: 42
  }
})

export { openAPISchema as BatchPayload }
