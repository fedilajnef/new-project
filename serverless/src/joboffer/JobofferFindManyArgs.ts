import { z } from '@hono/zod-openapi'
import { JobofferOrderByInput } from './JobofferOrderByInput'
import { JobofferWhereInput } from './JobofferWhereInput'
// Export the generated OpenAPI example as joboffer
const jobofferFindManyArgsSchema = z.object({
  where: z
  .object({
    where: z.object({
      AND: z.union([JobofferWhereInput, z.array(JobofferWhereInput)]).optional(),
      OR: z.array(JobofferWhereInput).optional(),
      NOT: z.union([JobofferWhereInput, z.array(JobofferWhereInput)]).optional()
    }).merge(JobofferWhereInput)
  })
  .optional(),
  orderBy: z.object({ orderBy: JobofferOrderByInput.optional() }).optional(),
// eslint-disable-next-line @typescript-eslint/no-unsafe-argument
  skip: z.string().optional().refine((value: any) => value === undefined || !isNaN(value), {
    message: 'Invalid value for skip, must be a number or undefined'
  }).openapi(({ example: '0' })), // Use a number example instead of string

  // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
  take: z.string().optional().refine((value: any) => value === undefined || !isNaN(value), {
    message: 'Invalid value for take, must be a number or undefined'
  }).openapi(({ example: '10' }))
})
export { jobofferFindManyArgsSchema as JobofferFindManyArgs }
