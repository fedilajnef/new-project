import type { Prisma,Joboffer } from '@prisma/client/edge'
import type { PaginatedInterface } from '../util/PaginatedInterface'
import { prisma } from '../config/database'


export class JobofferService {

findMany = async(
  args: Prisma.JobofferFindManyArgs
): Promise<PaginatedInterface<Joboffer>> => {
  try {
    const [data, totalCount] = await Promise.all([
      prisma.joboffer.findMany(args),
      prisma.joboffer.count({ where: { deletedAt: null } })
    ])
    return { paginatedResult: data, totalCount }
  } catch (error: any) {
    throw new Error(`Error updating Joboffer: ${ error.message }`)
  }
}

// find a single Joboffer
findOne = async (
  id: string | undefined
): Promise<Joboffer | null > => {
  try {
    const response = await prisma.joboffer.findUnique({
      where: { id: id }
    })
    return response
  } catch (error: any) {
    throw new Error(`Error updating Joboffer: ${ error.message }`)
  }
}

// update a Joboffer by ID
update = async (
  args: Prisma.JobofferUpdateArgs
): Promise<Joboffer> => {
  try {
    const updated = await prisma.joboffer.update(args)
    return updated
  } catch (error: any) {
    throw new Error(`Error updating Joboffer: ${ error.message }`)
  }
}

// create a new Joboffer
create = async (
  args: Prisma.JobofferCreateArgs
): Promise<Joboffer> => {
  try {
    const newRecord = await prisma.joboffer.create(args)
    return newRecord
  } catch (error: any) {
      throw new Error(`Error creating Joboffer: ${ error.message }`)
  }
}

// delete existing Joboffer
delete = async (
  args: Prisma.JobofferDeleteArgs
): Promise<Joboffer> => {
  try {
    const deleted = await prisma.joboffer.delete(args)
    return deleted
  } catch (error: any) {
    throw new Error(`Error deleting Joboffer: ${ error.message }`)
  }
}

// create many Joboffers
createMany = async (
  args: Prisma.JobofferCreateManyArgs
): Promise<Prisma.BatchPayload> => {
  try {
    return await prisma.joboffer.createMany(args)
  }catch(error:any) {
    throw new Error(`Error updating Joboffer: ${ error.message }`)
  }
}

// count Joboffers
count = async (
  args: Prisma.SelectSubset<null, Prisma.JobofferFindManyArgs>
): Promise<number> => {
  try {
    return await prisma.joboffer.count(args)
  } catch (error: any){
    throw new Error(`Error updating Joboffer: ${ error.message }`)
  }
}

// update many Joboffers
updateMany = async (
  args: Prisma.JobofferUpdateManyArgs
): Promise<Prisma.BatchPayload> => {
  try {
    return await prisma.joboffer.updateMany(args)
  } catch (error: any) {
    throw new Error(`Error updating Joboffer: ${ error.message }`)
  }
}

// soft delete a Joboffer
  softDelete = async (
    args: Prisma.JobofferUpdateArgs
  ): Promise<any> => {
    try {
      const deleted = await prisma.joboffer.update(args)
      return deleted
    } catch (error: any) {
      throw new Error(`Error soft deleting Joboffer: ${ error.message }`)
    }
  }
}

