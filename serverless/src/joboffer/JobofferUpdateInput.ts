import { z } from '@hono/zod-openapi'

import { condidateInJobofferDto } from '../condidateInJoboffer/CondidateInJoboffer'
// Define the schema for jobofferUpdateInputSchema input using Zod
export const jobofferUpdateInputSchema = z.object({ 
  id: z.optional(z.string()),
  createdAt: z.optional(z.union([z.date(), z.string()])), 
  updatedAt: z.optional(z.union([z.date(), z.string(), z.null()])), 
  deletedAt: z.optional(z.union([z.date(), z.string(), z.null()])), 
  jobDescription: z.optional(z.union([z.object({}), z.null()])), 
  image: z.optional(z.union([
  z.string(),
 z.null()
])), 
  status: z.optional(z.union([z.string(), z.null()])),
  dateFin: z.optional(z.union([z.array(), z.null()])), 
  condidateInJoboffers: z.optional(z.union([z.array(condidateInJobofferDto), z.null()]))}).strict() 

// Generate an OpenAPI example for joboffer input
const openApiExample = jobofferUpdateInputSchema.openapi({ 
  example: {
    id: '550e8400-e29b-41d4-a716-446655440000', 
    createdAt: new Date('2023-08-24T10:00:00Z'), 
    updatedAt: new Date('2023-08-24T10:00:00Z'), 
    deletedAt: new Date('2023-08-24T10:00:00Z'), 
    jobDescription: {}, 
    image: 'image', 
    status: "", 
    dateFin: [
 {
 id: '550e8400-e29b-41d4-a716-446655440000' 
} 
], 
  condidateInJoboffers: [{ id: '550e8400-e29b-41d4-a716-446655440000' }]  
}
}).strict() 

// Define a type for joboffer input using the generated example
export type UpdateJoboffer = z.infer <typeof jobofferUpdateInputSchema>

// Export the generated OpenAPI example as joboffer
export { openApiExample as JobofferUpdateInput } 
