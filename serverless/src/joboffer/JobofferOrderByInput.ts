import { z } from '@hono/zod-openapi'

import { condidateInJobofferDto } from '../condidateInJoboffer/CondidateInJoboffer'
const sortEnum = z.enum(['asc', 'desc'])

const jobofferOrderByInputSchema = z.object({
    id: sortEnum.optional(), 
    createdAt: sortEnum.optional(), 
    updatedAt: sortEnum.optional(), 
    deletedAt: sortEnum.optional(), 
    jobDescription: sortEnum.optional(), 
    image: sortEnum.optional(), 
    status: sortEnum.optional(), 
    dateFin: sortEnum.optional(), 
  condidateInJoboffers:  z.array(sortEnum)}).strict();

const JobofferOrderByInput = jobofferOrderByInputSchema.openapi({
  example: {
    id: 'asc', 
    createdAt: 'asc', 
    updatedAt: 'asc', 
    deletedAt: 'asc', 
    jobDescription: 'asc', 
    image: 'asc', 
    status: 'asc', 
    dateFin: 'asc', 
  condidateInJoboffers: ['asc']  
}
})

export { JobofferOrderByInput }
