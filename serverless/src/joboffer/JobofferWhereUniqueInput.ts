import { z } from '@hono/zod-openapi'

// Define the schema for jobofferWhereUniqueInputSchema input using Zod
const JobofferWhereUniqueInputSchema = z.object({ 
  id: z.string({
                required_error: 'id is required',
                invalid_type_error: 'id must be a string'
            })
}).strict(); 

// Generate an OpenAPI example for joboffer input
const openApiExample = JobofferWhereUniqueInputSchema.openapi({ 
  example: {
    id: '550e8400-e29b-41d4-a716-446655440000'  
} 
}) 

// Export the generated OpenAPI example as joboffer
export { openApiExample as JobofferWhereUniqueInput  } 
