import { z } from '@hono/zod-openapi'
import { JobofferOrderByInput } from './JobofferOrderByInput'
import { JobofferWhereInput } from './JobofferWhereInput'
// Export the generated OpenAPI example as joboffer
const jobofferUpdateManyArgsSchema = z.object({
  where: z
  .object({
    where: z.object({
      AND: z.union([JobofferWhereInput, z.array(JobofferWhereInput)]).optional(),
      OR: z.array(JobofferWhereInput).optional(),
      NOT: z.union([JobofferWhereInput, z.array(JobofferWhereInput)]).optional()
    }).merge(JobofferWhereInput)
  })
  .optional(),
})

export type UpdateJoboffer = z.infer<typeof jobofferUpdateManyArgsSchema>
export { jobofferUpdateManyArgsSchema as JobofferUpdateManyArgs }
