 import { z } from '@hono/zod-openapi'
import { jobofferDto } from './Joboffer'

import { condidateInJobofferDto } from '../condidateInJoboffer/CondidateInJoboffer'
const getListJobofferDtoSchema = z.object({
    paginatedResult: z.array(jobofferDto), // An array of joboffer objects
    totalCount: z.number() // Total count of  joboffer 
}).strict() 

// Generate an OpenAPI example for joboffer input
export const GetListJobofferDto = getListJobofferDtoSchema.openapi({ 
  example: {

     paginatedResult: [ 
{
    id: '550e8400-e29b-41d4-a716-446655440000', 
    createdAt: new Date('2023-08-24T10:00:00Z'), 
    updatedAt: new Date('2023-08-24T10:00:00Z'), 
    deletedAt: new Date('2023-08-24T10:00:00Z'), 
    jobDescription: {}, 
    image: 'image', 
    status: "", 
    dateFin: [
 {
 id: '550e8400-e29b-41d4-a716-446655440000' 
} 
], 
  condidateInJoboffers: [{ id: '550e8400-e29b-41d4-a716-446655440000' }] 
}
 ], 
 totalCount: 1
 } 
}) 

