import * as XLSX from "xlsx";
import { auth } from "../middlewares/auth";
import { checkPermission } from "../util/checkPermission";
import { processArgs } from "../util/ProcessArgs";
import { flattenObject } from "../util/flattenObject";
import { createRoute } from "@hono/zod-openapi";
import type { Prisma } from "@prisma/client/edge";
import type { JobofferService } from "./joboffer.service";
import { JobofferCreateInput } from "./JobofferCreateInput";
import { JobofferFindManyArgs } from "./JobofferFindManyArgs";
import { JobofferWhereUniqueInput } from "./JobofferWhereUniqueInput";
import { JobofferUpdateInput } from "./JobofferUpdateInput";
import { GetListJobofferDto } from "./getListJoboffer.dto";
import { jobofferDto } from "./Joboffer";
import { errorHandler } from "../middlewares/errorHandlingMiddleware";

export class JobofferControllerBase {
  // eslint-disable-next-line @typescript-eslint/space-before-function-paren
  constructor(private readonly jobofferService: JobofferService) {}
  
  create: any = async (c: any) => {
    try {
      await auth(c);
      const payloadRoles: string[] = c.get("payload").roles;
      const permission = checkPermission(payloadRoles, "create:any");
      const rolesString = payloadRoles.join(", ");
      if (!permission) {
        return c.json(
          {
            code: 403,
            message: `Permission denied. Joboffer creation is not allowed for roles: ${rolesString}.`,
          },
          403
        );
      }
      const bodyParse = await c.req.json();
      // If permission check passes, create ENTITY_NAME
      const response = await this.jobofferService.create({
        data: {
          jobDescription: bodyParse.jobDescription,
          image: bodyParse.image,
          status: bodyParse.status,
          dateFin: bodyParse.dateFin,
        },
      });

      return c.json({ response, code: 200 });
    } catch (error: any) {
      return await errorHandler(error, c);
    }
  };

  routeCreate = createRoute({
    method: "post",
    path: "/joboffer",
    tags: ["joboffer"],
    request: {
      body: {
        content: {
          "application/json": {
            schema: JobofferCreateInput,
          },
        },
      },
    },
    responses: {
      200: {
        content: {
          "application/json": {
            schema: JobofferCreateInput,
          },
        },
        description: "Create a new Joboffer",
      },
    },
  });

  // Find many joboffer entity
  findMany: any = async (c: any) => {
    try {
      await auth(c);
      const payloadRoles: string[] = c.get("payload").roles;
      const permission = checkPermission(payloadRoles, "read:any");
      const rolesString = payloadRoles.join(", ");
      if (!permission) {
        return c.json(
          {
            code: 403,
            message: `Permission denied. Joboffer finding is not allowed for roles: ${rolesString}.`,
          },
          403
        );
      }

      // Parse and process query parameters
      const queryParse = c.req.query();
      const args = processArgs(queryParse);
      // Fetch ENTITY_NAME with selected fields
      const result = await this.jobofferService.findMany({
        where: args?.where as Prisma.JobofferWhereInput,

        orderBy: args?.orderBy as Prisma.JobofferOrderByWithRelationInput,

        skip: args.skip !== undefined ? parseInt(args?.skip) : undefined,

        take: args.take !== undefined ? parseInt(args?.take) : undefined,

        select: {
          id: true,
          jobDescription: true,
          image: true,
          status: true,
          dateFin: true,
        },
      });

      // Return paginated result and total count
      return c.json({
        paginatedResult: result.paginatedResult,
        totalCount: result.totalCount,
        code: 200,
      });
    } catch (error: any) {
      return await errorHandler(error, c);
    }
  };

  routeFindMany = createRoute({
    method: "get",
    path: "/joboffer",
    tags: ["joboffer"],
    request: {
      query: JobofferFindManyArgs,
    },
    responses: {
      200: {
        content: {
          "application/json": {
            schema: GetListJobofferDto,
          },
        },
        description: "Retrieve a list of Joboffer",
      },
    },
  });

  // Update joboffer entity
  update: any = async (c: any) => {
    try {
      await auth(c);
      const payloadRoles: string[] = c.get("payload").roles;
      const permission = checkPermission(payloadRoles, "update:any");
      const rolesString = payloadRoles.join(", ");
      if (!permission) {
        return c.json(
          {
            code: 403,
            message: `Permission denied. Joboffer updating is not allowed for roles: ${rolesString}.`,
          },
          403
        );
      }

      // Parse request parameters and body
      const paramsParse = c.req.param();
      const bodyParse = await c.req.json();
      const params = JobofferWhereUniqueInput.parse(paramsParse);
      const response = await this.jobofferService.update({
        where: { id: params.id },
        data: {
          jobDescription: bodyParse.jobDescription,
          image: bodyParse.image,
          status: bodyParse.status,
          dateFin: bodyParse.dateFin,
        },
      });
      return c.json(response, 200);
    } catch (error: any) {
      return await errorHandler(error, c);
    }
  };

  routeUpdate = createRoute({
    method: "patch",
    path: "/joboffer/{id}",
    tags: ["joboffer"],
    request: {
      params: JobofferWhereUniqueInput,
      body: {
        content: {
          "application/json": {
            schema: JobofferUpdateInput,
          },
        },
      },
    },
    responses: {
      200: {
        content: {
          "application/json": {
            schema: jobofferDto,
          },
        },
        description: "Update a Joboffer",
      },
    },
  });

  // Delete joboffer entity
  delete: any = async (c: any) => {
    try {
      await auth(c);
      const payloadRoles: string[] = c.get("payload").roles;
      const permission = checkPermission(payloadRoles, "delete:any");
      const rolesString = payloadRoles.join(", ");
      if (!permission) {
        return c.json(
          {
            code: 403,
            message: `Permission denied. Joboffer deleting is not allowed for roles: ${rolesString}.`,
          },
          403
        );
      }
      // Parse request parameters
      const paramsParse = c.req.param();
      const params = JobofferWhereUniqueInput.parse(paramsParse);
      // Delete
      const response = await this.jobofferService.delete({
        where: { id: params.id },
      });

      return c.json(response, 200);
    } catch (error: any) {
      return await errorHandler(error, c);
    }
  };

  routeDelete = createRoute({
    method: "delete",
    path: "/joboffer/{id}",
    tags: ["joboffer"],
    request: {
      params: JobofferWhereUniqueInput,
    },
    responses: {
      200: {
        content: {
          "application/json": {
            schema: jobofferDto,
          },
        },
        description: "Delete a Joboffer",
      },
    },
  });

  // Find joboffer Data For Excel
  findDataForExcel: any = async (c: any) => {
    try {
      await auth(c);
      const payloadRoles: string[] = c.get("payload").roles;
      const permission = checkPermission(payloadRoles, "delete:any");
      const rolesString = payloadRoles.join(", ");
      if (!permission) {
        return c.json(
          {
            code: 403,
            message: `Permission denied. Joboffer finding is not allowed for roles: ${rolesString}.`,
          },
          403
        );
      }
      // Parse and process query parameters
      const queryParse = c.req.query();
      const args = JobofferFindManyArgs.strict().parse(processArgs(queryParse));
      // Fetch jobofferdata
      const result = await this.jobofferService.findMany({
        where: args?.where as Prisma.JobofferWhereInput,

        orderBy: args?.orderBy as Prisma.JobofferOrderByWithRelationInput,

        skip: args.skip !== undefined ? parseInt(args?.skip) : undefined,

        take: args.take !== undefined ? parseInt(args?.take) : undefined,

        select: {
          id: true,
          createdAt: true,
          updatedAt: true,
          deletedAt: true,
          jobDescription: true,
          image: true,
          status: true,
          dateFin: true,
          condidateInJoboffers: true,
        },
      });
      // Flatten the result for Excel
      const flatResult = result.paginatedResult.map((obj) =>
        flattenObject(obj)
      );
      // Convert to Excel format
      const excelFile = XLSX.utils.json_to_sheet(flatResult);
      const Workbook = XLSX.utils.book_new();
      await XLSX.utils.book_append_sheet(Workbook, excelFile, "data");
      // Convert to base64 and return as a file
      const file = await XLSX.write(Workbook, {
        bookType: "xlsx",
        bookSST: true,
        type: "base64",
      });
      return c.json({ file }, 200);
    } catch (error: any) {
      return await errorHandler(error, c);
    }
  };

  routeFindDataExcel = createRoute({
    method: "get",
    path: "/joboffer/fileExcel",
    tags: ["joboffer"],
    responses: {
      200: {
        content: {
          "application/json": {
            schema: JobofferFindManyArgs,
          },
        },
        description: "Retrieve Joboffer data in Excel format",
      },
    },
  });

  // Update joboffer entity
  findOne: any = async (c: any) => {
    try {
      await auth(c);
      const payloadRoles: string[] = c.get("payload").roles;
      const permission = checkPermission(payloadRoles, "read:own");
      const rolesString = payloadRoles.join(", ");
      if (!permission) {
        return c.json(
          {
            code: 403,
            message: `Permission denied. Joboffer finding One is not allowed for roles: ${rolesString}.`,
          },
          403
        );
      }
      // Parse request parameters and body
      const paramsParse = c.req.param();
      const params = JobofferWhereUniqueInput.parse(paramsParse);
      const Joboffer = await this.jobofferService.findOne(params.id);
      return c.json(Joboffer, 200);
    } catch (error: any) {
      return await errorHandler(error, c);
    }
  };

  routeFindOne = createRoute({
    method: "get",
    path: "/joboffer/{id}",
    tags: ["joboffer"],
    request: {
      params: JobofferWhereUniqueInput,
    },
    responses: {
      200: {
        content: {
          "application/json": {
            schema: jobofferDto,
          },
        },
        description: "find one Joboffer",
      },
    },
  });

  // soft deleting joboffer entity
  softDelete: any = async (c: any) => {
    try {
      await auth(c);
      const payloadRoles: string[] = c.get("payload").roles;
      const permission = checkPermission(payloadRoles, "update:any");
      const rolesString = payloadRoles.join(", ");
      if (!permission) {
        return c.json(
          {
            code: 403,
            message: `Permission denied. Joboffer  soft deleting  is not allowed for roles: ${rolesString}.`,
          },
          403
        );
      }

      // Parse request parameters and body
      const paramsParse = c.req.param();
      const bodyParse = await c.req.json();
      const params = JobofferWhereUniqueInput.parse(paramsParse);
      const response = await this.jobofferService.softDelete({
        where: { id: params.id },
        data: {
          deletedAt: bodyParse.deletedAt,
        },
      });
      return c.json(response, 200);
    } catch (error: any) {
      return await errorHandler(error, c);
    }
  };

  routeSoftDelete = createRoute({
    method: "patch",
    path: "/joboffer/softDelete/{id}",
    tags: ["joboffer"],
    request: {
      params: JobofferWhereUniqueInput,
      body: {
        content: {
          "application/json": {
            schema: JobofferUpdateInput,
          },
        },
      },
    },
    responses: {
      200: {
        content: {
          "application/json": {
            schema: jobofferDto,
          },
        },
        description: "soft deleting a Joboffer",
      },
    },
  });
}
