import { z } from '@hono/zod-openapi'

import { condidateInJobofferDto } from '../condidateInJoboffer/CondidateInJoboffer'
// Define the schema for Joboffer input using Zod
export const jobofferSchema = z.object({ 
  id: z.string({
                required_error: 'id is required',
                invalid_type_error: 'id must be a string'
            }),
  createdAt: z.optional(z.union([z.date(), z.string()])), 
  updatedAt: z.optional(z.union([z.date(), z.string(), z.null()])), 
  deletedAt: z.optional(z.union([z.date(), z.string(), z.null()])), 
  jobDescription: z.optional(z.union([z.object({}), z.null()])), 
  image: z.optional(z.union([
  z.string(),
 z.null()
])), 
  status: z.optional(z.union([z.string(), z.null()])),  dateFin: z.optional(z.union([z.array(z.string()), z.null()])), 
  condidateInJoboffers: z.optional(z.union([z.array(condidateInJobofferDto), z.null()]))}) 

// Generate an OpenAPI example for Joboffer input
const openApiExample = jobofferSchema.openapi({ 
  example: {
    id: '550e8400-e29b-41d4-a716-446655440000', 
    createdAt: new Date('2023-08-24T10:00:00Z'), 
    updatedAt: new Date('2023-08-24T10:00:00Z'), 
    deletedAt: new Date('2023-08-24T10:00:00Z'), 
    jobDescription: {}, 
    image: 'image', 
    status: "", 
    dateFin: [
 {
 id: '550e8400-e29b-41d4-a716-446655440000' 
} 
], 
  condidateInJoboffers: [
 {
 id: '550e8400-e29b-41d4-a716-446655440000' 
} 
]  
}
}).strict() 

// Define a type for Joboffer input using the generated example
export type Joboffer = z.infer<typeof openApiExample> 

// Export the generated OpenAPI example as Joboffer
export { openApiExample as jobofferDto }
