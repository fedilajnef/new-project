<<<<<<< HEAD

# Project Setup Guide

This guide will help you set up and manage your project using Prisma Accelerate and other essential tools.

## Prerequisites

Before you start, ensure you have the following:

- **Node.js**: Version 16.20.2
- **npm**: Version 8.19.4

## Getting Started

1. **Create a Database with Prisma Accelerate**: Begin by setting up your database using Prisma Accelerate.

2. **Run the Input Start Script**: Launch the input-start-script.sh. This Bash script will prompt you for a database URL input and set it as an environment variable, ensuring a valid value is provided.

    ```bash
    ./input-start-script.sh
    ```

3. **Add a Pre-commit Hook**: Execute the pre-commit-script.sh to add a pre-commit hook to your Git repository. This hook enforces the presence of specific lines in schema.prisma and index.ts files, and it rejects commits that do not meet these conditions.

    ```bash
    ./pre-commit-script.sh
    ```

4. **Configure a Pre-push Hook**: Use the pre-push-script.sh to configure a Git pre-push hook. This hook replaces Prisma and API URLs in designated files using provided or default values.

    ```bash
    ./pre-push-script.sh $URL_DATABASE $CF_ROUTE
    ```

5. **Project Initialization**: Run the start-project.sh script to replace an API URL in an index.ts file and optionally update a Prisma URL in a schema.prisma file. It also supports configuring a BREVO_KEY variable in a wrangler.toml file.

    ```bash
    ./start-project.sh $CF_ROUTE $URL_DATABASE
    ```

6. **Docker Compose**: Finally, start your project with Docker Compose using the "dev" profile.

    ```bash
    docker-compose --profile dev up -d
    ```

## Additional CLI Commands

You can use the following npm run commands to manage your project:

- **npm run seed**: Runs a TypeScript script (seed.ts) to seed your database with initial data.

- **npm run prisma:generate**: Generates Prisma Client code using npx prisma generate, optimized for use with Accelerate.

- **npm run db-push:hard**: Copies the project's .env file to the Prisma directory and pushes database schema changes using yarn prisma db push. It includes the --accept-data-loss flag, allowing for potentially destructive changes.

- **npm run db:migrate-save**: Executes prisma migrate dev, a command for creating and managing database migrations.

- **npm run seed:push**: Runs the Prisma-generated seed script (seed-generated-output/seed) to populate the database with data.

- **npm run build**: Executes the build.js script to build your project.

- **npm run deploy:prod**: Deploys your project to a production environment using Wrangler.

- **npm run start**: Initiates a local development server using Wrangler with the entry point from src/index.ts.

This setup allows you to efficiently manage your project, set up the database, and perform essential tasks with ease.

---

This revised README provides clear steps and descriptions for setting up and managing your project, making it more user-friendly and informative.
=======
>>>>>>> ec9e850a36d110429d58a82082bd6f75ef4525c0
