#!/bin/bash

source "../.env"
# Set the paths for your .env and wrangler.toml files
ENV_FILE=".env"
WRANGLER_TOML="wrangler.toml"
# setting envs into .env file
echo "DATABASE_URL=$DATABASE_URL" >> $ENV_FILE
echo "BREVO_KEY=$BREVO_KEY" >> $ENV_FILE
# Specify the variables to copy from .env
VARIABLES=("BREVO_KEY" "SITE_URL")

# Remove existing [vars] section
sed -i '/^\[vars\]$/,/^\[/ { /^\[vars\]$/! { /^\[/!d } }' "$WRANGLER_TOML"

# Append selected variables from .env to wrangler.toml under [vars]
for VAR in "${VARIABLES[@]}"; do
  VALUE=$(grep "$VAR" "$ENV_FILE" | cut -d'=' -f2)
  # Remove surrounding double quotes if they exist in the value
  VALUE=$(sed 's/^"\(.*\)"$/\1/' <<< "$VALUE")
  echo "$VAR = \"$VALUE\"" >> "$WRANGLER_TOML"
done
echo "PRISMA_ACCELERATE_URL=$PRISMA_ACCELERATE_URL" >> $ENV_FILE
echo "PRISMA_ACCELERATE_URL=\"$PRISMA_ACCELERATE_URL\"" >> $WRANGLER_TOML
echo "Linkedin_CLIENT_ID=\"$Linkedin_CLIENT_ID\"" >> $WRANGLER_TOML
echo "Linkedin_CLIENT_SECRET=\"$Linkedin_CLIENT_SECRET\"" >> $WRANGLER_TOML
echo "CF_ROUTE=\"$CF_ROUTE\"" >> $WRANGLER_TOML
echo "Script completed successfully."
