import { PrismaClient } from "@prisma/client";
import { customSeed } from "./customSeed";
import { hash } from "bcrypt";
import { prisma } from '../src/config/database'



const salt = 8;

seed(salt).catch((error) => {
  console.error(error);

});
// }

async function seed(bcryptSalt: any) {
  console.info("Seeding database...");

  const client = new PrismaClient();
  const data = {
    username: "admin@gmail.com",
    firstName: "admin",
    lastName: "admin",
    password: await hash("admin", bcryptSalt),
    isValid: true,
    roles: ["user"],
  };
  await prisma.user.upsert({
    where: { username: data.username },
    update: {},
    create: data,
  });

  const createReqUserFunction =
    "create or replace function requesting_user_id() " +
    "returns text " +
    "language sql stable " +
    "as $$ " +
    "select nullif(current_setting('request.jwt.claims', true)::json->>'sub', '')::text; " +
    "$$;";
  await client.$queryRawUnsafe(createReqUserFunction);
  // create function that initialize isValid in User table depends on password
  const queryFunctionIsValidUser =
    'create or replace function public.handle_valid_user()\n      returns trigger\n      language plpgsql\n      security definer set search_path = public\n      as $$ \n      begin\n        IF (new.password is not null and length(new.password) > 0) THEN\n          update public."User" set "isValid" = true where (id = new.id::text and ("isValid" is null or "isValid" = false ));\n        ELSE\n          update public."User" set "isValid" = false where (id = new.id::text and "isValid" is null);\n        END IF; \n        return new;\n      end;\n      $$;';
  await client.$queryRawUnsafe(queryFunctionIsValidUser);

  const queryTriggerValidUser =
    '\n      create or replace trigger handle_valid_user\n      after insert or update on public."User"\n      for each row execute procedure public.handle_valid_user()';
  await client.$queryRawUnsafe(queryTriggerValidUser);

  client.$disconnect();

  console.info("Seeding database with custom seed...");
  customSeed();

  console.info("Seeded database successfully");
}
