import { PrismaClient } from '@prisma/client';
import { prisma } from '../src/config/database'

export async function customSeed() {
  const client = new PrismaClient();
  const username = 'admin@gmail.com'

  //replace this sample code to populate your database
  //with data that is required for your application to start
  await prisma.user.update({
    where: { username: username },
    data: {
      username
    }
  })

  client.$disconnect()
}
