import { camelCase } from "camel-case";
import { capitalizeFirstLetter } from "../formatDataString";


export const tsGenerateSeedDataFileWithRelations = (entityName: string, relationsFields: Object[]) => {
    // entitiesRelationsFieldsMap => [{fieldsFK: [userId], relationTable: User[]?, referencesPK: [id]}]

    // get import of other entites list (json)
    let entitiesListImports: string[] = []
    let pkFkUsedFieldsDeclaration: string[] = []
    let addPkFk: string[] = []

    relationsFields.forEach((field: any) => {
        const relationTableName = field["relationTableAndType"].replace("?", "").replace("[]", "")
        const fieldsFKsList = field["fieldsFK"].slice(1, -1).split(',')
        const referencesPKList = field["referencesPK"].slice(1, -1).split(',')
        if ((fieldsFKsList.length !== referencesPKList.length) || fieldsFKsList.length > 1) throw new Error(`[ERROR] multiple fields relation is not handled :(. on entity ${entityName}, \n field: ${field}`)

        if (relationTableName != entityName) {
            entitiesListImports.push(`import { ${camelCase(relationTableName)}List } from '../${relationTableName}/${relationTableName}-ts';`)
            pkFkUsedFieldsDeclaration.push(`\tlet used${capitalizeFirstLetter(fieldsFKsList[0])}s: any[] = []`)
            addPkFk.push(`
            let ${fieldsFKsList[0]} = undefined
            if (used${capitalizeFirstLetter(fieldsFKsList[0])}s.length < ${camelCase(relationTableName)}List.length) {
                 let attempts = 0;
                 const maxAttempts = 100;
                while (used${capitalizeFirstLetter(fieldsFKsList[0])}s.includes(${fieldsFKsList[0]}) || ${fieldsFKsList[0]} === undefined) {
                    ${fieldsFKsList[0]} = ${camelCase(relationTableName)}List[Math.floor(Math.random() * (${camelCase(relationTableName)}List.length))][Math.floor(Math.random() * ${camelCase(relationTableName)}List.length)].id
                    if (!used${capitalizeFirstLetter(fieldsFKsList[0])}s.includes(${fieldsFKsList[0]}) && ${fieldsFKsList[0]} !== undefined) {
                            relationFieldsToAdd["${fieldsFKsList[0]}"] = ${fieldsFKsList[0]};
                            used${capitalizeFirstLetter(fieldsFKsList[0])}s.push(${fieldsFKsList[0]});
                            break;
                        }

                        attempts++;

                        if (attempts >= maxAttempts) {
                            console.log("Exceeded maximum attempts, breaking out of the loop.");
                            break;
                        }
                }
                
            }
            

            `)
        }

    })

    entitiesListImports = [...new Set(entitiesListImports)]; // remove duplicated imports

    return `
${entitiesListImports.join('\n')}
import PrismaEdge from "@prisma/client/edge"
import ${entityName} from './${entityName}.json'
const { PrismaClient } = PrismaEdge
import { prisma } from "../../../../src/config/database";


export let ${camelCase(entityName)}List = [${entityName}.${entityName}]

export const add${entityName}SeedData = async() => {
    try {

          let used${entityName}Ids: any[] = [];

        ${pkFkUsedFieldsDeclaration.join('\n')}
        
        for (let i = 0; i < ${entityName.toLowerCase()}List.length; i++) {
            let relationFieldsToAdd: any = {}
             ${entityName.toLowerCase()}List[i] = ${entityName.toLowerCase()}List[i].map((fields: any) => {
            ${addPkFk.join('\n')}
        
            return Object.keys(relationFieldsToAdd).length == 0 ?  fields : {...fields, ...relationFieldsToAdd}
             });
         // Flatten the array before upserting
         const flattened${entityName}List = ${entityName.toLowerCase()}List[i].flat();

        
            await Promise.all(
                flattened${entityName}List.map((${entityName.toLowerCase()}: any) =>
                    prisma.${entityName.toLowerCase()}.upsert({
                        where: {
                            id: ${entityName.toLowerCase()}.id,
                        },
                        update: ${entityName.toLowerCase()},
                        create: ${entityName.toLowerCase()},
                    })
                )
            );
        }

    } catch (error: any) {
        console.log("Error pushing to: ${entityName}")
        console.log("Error:", error)
    }

}`
}

export const tsGenerateSeedDataFile = (entityName: string) => {
    return `
import PrismaEdge from "@prisma/client/edge"
import ${entityName} from './${entityName}.json'
const { PrismaClient } = PrismaEdge
import { prisma } from "../../../../src/config/database";

export let ${camelCase(entityName)}List = [${entityName}.${entityName}]

export const add${entityName}SeedData = async() => {
    try {
        for (let i = 0; i < ${camelCase(entityName)}List.length; i++) {
            for (let j = 0; j < ${camelCase(entityName)}List[i].length; j++) {
                await prisma.${camelCase(entityName)}.upsert({
                    where: {
                        id: ${camelCase(entityName)}List[i][j].id,
                    },
                    update: ${camelCase(entityName)}List[i][j],
                    create: ${camelCase(entityName)}List[i][j],
                })
            }
        }
    } catch (error: any) {
        console.log("Error pushing to: ${entityName}")
        console.log("Error:", error)
    }

}`
}

export const tsGenerateSeedRunnerFile = (entityNamesList: string[]) => {
    let functionsList: string[] = []
    let importsList: string[] = []
    entityNamesList.forEach((entity: string) => {
        importsList.push(`import { add${entity}SeedData } from "./data/${entity}/${entity}-ts"\n`)
        functionsList.push(`\tconsole.log('seeding ${entity} Data....');\n`)
        functionsList.push(`\tawait add${entity}SeedData();\n`)
    })

    return `
${importsList.map(importString => importString).join('')}
export const pushSeed = async() => {
    ${functionsList.map(functionString => functionString).join('')}
}
pushSeed()
        `

}