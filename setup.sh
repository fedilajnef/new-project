   #!/bin/bash
      cp .env.example .env
      source ".env"
      cd serverless
      echo -n "" > ./src/config/database.ts
      # resetting .env origianl file 
      cp .env.example .env
      cp wrangler.backup.toml wrangler.toml
      npm install 
      echo "generating seed Data..." 
      npx tsx ./prisma/seedGenerator/src/genSeed
      cd ..
      echo "SITE_URL=$(gp url 3001)" >> .env
      echo "SITE_URL=$(gp url 3001)" >> ./serverless/.env
      echo "CF_ROUTE=$(gp url 8000)" >> .env
      echo "GITPOD_WORKSPACE_URL=$(gp url 3001)" >> .env
      echo "setting env vars..."
      cd serverless
      sudo chmod 777 ./envSetter.sh
      ./envSetter.sh
      cd ..
      sudo apt-get install -y gettext
      export PRISMA_ACCELERATE_URL=$PRISMA_ACCELERATE_URL
      envsubst < ./serverless/src/config/database.template.ts >> ./serverless/src/config/database.ts
      echo "migration stage..."
      cd ./serverless
      npm run prisma:generate
      npm run db:migrate-save
      npm run seed
      echo "Final stage..."
      cd ..
      echo "CF_ROUTE=$(gp url 8000)" >> ./serverless/.env
